<?php
header("Access-Control-Allow-Origin: *");      
// header("Access-Control-Allow-Headers:
//  {$_SERVER['HTTP_ACCESS_CONTROL_REQUEST_HEADERS']}");
header('Content-Type: application/json; charset=UTF-8');
include_once "../mysql_connect.inc.php";
include_once "../session_stat.php";
// if ($_SERVER['REQUEST_METHOD'] == "POST") {
//////////////////Customer info//////////////////
    if(isset($_POST)){
        if($_POST['Feature'] == "show_all"){
            show_all('show_all',$_POST,$conn);
        }
        else if($_POST['Feature'] == "show_customer_info"){
            $info_array = array();
            // print_r($_POST);die;
            $Customer_ID = $_POST["Customer_ID"];
            if ($Customer_ID){
                $sql = "SELECT * FROM `customer_list` WHERE `Customer_ID` = '".$Customer_ID."'";
                $result = mysqli_query($conn,$sql);
                while($row = $result->fetch_assoc())
                    foreach($row as $key => $value){
                        if($key == 'Customer_Remark')
                            $value = str_replace(chr(13).chr(10), "<br />",$value);
                        $info_array[$key] = $value;
                    }
        
                echo json_encode($info_array,JSON_UNESCAPED_UNICODE);
            } else {
                echo json_encode(array(
                    'errorMsg' => '資料未輸入完全！'
                ),JSON_UNESCAPED_UNICODE);
            }
        }
        else if($_POST['Feature'] == "Update_Info"){
            if($_POST['submit'] == '確定'){
                $Customer_Remark = str_replace("<br />", '',nl2br($_POST["Customer_Remark"]));
                $Status = $_POST["Customer_Remark"] ? "1":"0";
                $sql = "UPDATE `customer_list` SET `Customer_Name`='".$_POST['Customer_Name']."',`Customer_SSID`='".$_POST['Customer_SSID']."',`Customer_Sex`='".$_POST['Customer_Sex']."',`Customer_BDay`='".$_POST['Customer_BDay']."',`Customer_Phone`='".$_POST['Customer_Phone']."',`Customer_Email`='".$_POST['Customer_Email']."',`Customer_Address`='".$_POST['Customer_Address']."',`Customer_Status`='".$Status."',`Customer_Remark`='".$Customer_Remark."' WHERE `Customer_ID`='".$_POST['Customer_ID']."'";
                if(!mysqli_query($conn,$sql)){
                    echo "This SQL: " . $sql . "<br>";
                    die;
                }
            }
            else if($_POST['submit'] == '刪除'){
                $sql = "UPDATE `customer` SET `Enable`=0 WHERE `Customer_ID`='".$_POST['Customer_ID']."'";
                if(!mysqli_query($conn,$sql)){
                    echo "This SQL: " . $sql . "<br>";
                    die;
                }

            }
            show_all('Update_Info',$_POST,$conn);
        }
    }
    function show_all($From,$Update_Info,$conn){
        // echo $From."<pre>";
        // print_r($Update_Info);
        // echo "</pre>";
        $flag=0;
        $sql = "SELECT * FROM `customer_list`";
        $Fuzzy_Search = "";
        $Date = "";
        if(!empty($Update_Info['Fuzzy_Search'])){
            $Fuzzy_Search = "(`Customer_Name` LIKE '%".$Update_Info['Fuzzy_Search']."%' OR `Customer_Phone` LIKE '%".$Update_Info['Fuzzy_Search']."%' OR `Customer_Email` LIKE '%".$Update_Info['Fuzzy_Search']."%' OR `Customer_SSID` LIKE '%".$Update_Info['Fuzzy_Search']."%' OR `Customer_Address` LIKE '%".$Update_Info['Fuzzy_Search']."%') ";
        }

        if($Fuzzy_Search != '')
            $sql = $sql . " WHERE " . $Fuzzy_Search;
        // echo $sql;die;
        $info_array = array();
        $result = mysqli_query($conn,$sql);
        $count = 0;
        while($row = $result->fetch_assoc()){
            foreach($row as $key => $value){
                $info_array[$count][$key] = $value;
            }
            $count++;
        }
        echo json_encode($info_array,JSON_UNESCAPED_UNICODE);
    }
?>