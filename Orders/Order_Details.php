<?php
    include_once "../session_stat.php";
    include_once "../mysql_connect.inc.php";
    if ($_SERVER['REQUEST_METHOD'] == "POST") {
        if(isset($_POST['Action'])){
            // $Order_ID = explode("_",$_POST['Action']);
            // $Action = $Order_ID[1];
            // $Order_ID = $Order_ID[0];
            $Order_ID = $_POST['Action'];
            $sql = "SELECT * FROM `order_list` WHERE `Order_ID`='".$Order_ID."'";
            $resule = mysqli_query($conn,$sql);
            $Order_Info = $resule->fetch_assoc();
            $sql = "SELECT `BA_Name` FROM `branch_allocation` WHERE `BA_ID`='".$Order_Info['Rental_Area']."'";
            $Order_Info['Rental_Area'] = mysqli_query($conn,$sql)->fetch_assoc()['BA_Name'];
            $sql = "SELECT `BA_Name` FROM `branch_allocation` WHERE `BA_ID`='".$Order_Info['Return_Area']."'";
            $Order_Info['Return_Area'] = mysqli_query($conn,$sql)->fetch_assoc()['BA_Name'];
            $sql = "SELECT * FROM `customer_list` WHERE `Customer_ID`='".$Order_Info['Customer_ID']."'";
            $Customer_Info = mysqli_query($conn,$sql)->fetch_assoc();
            $sql = "SELECT * FROM `customer_list` WHERE `Customer_ID`='".$Order_Info['Guarantor_ID']."'";
            $Guarantor_Info = mysqli_query($conn,$sql)->fetch_assoc();
            $sql = "SELECT * FROM `car_list`,`car_type` WHERE `car_list`.`Car_Type` = `car_type`.`Car_Type` AND `car_list`.`License_Plate` = '".$Order_Info['License_Plate']."'";
            $Vehicle_Info = mysqli_query($conn,$sql)->fetch_assoc();
            // if(empty($Order_Info['License_Plate']))
            //     $Vehicle_Info['License_Plate'] = "未安排";
            $SC_Flag = true;
            date_default_timezone_set('Asia/Taipei');
            if(intval(date_diff(date_create(date('Y/m/d H:i:s', time())),date_create($Order_Info['Estimated_Rent_DateTime']))->format('%r%a'))<=0)
                $SC_Flag = false;
        }
        else{
            echo '<meta http-equiv=REFRESH CONTENT=0;url=index.php>';
            die;
        }
    }
    else{
        echo '<meta http-equiv=REFRESH CONTENT=0;url=index.php>';
        die;
    }

    $sex_array = array('女', '男', '不明', '戰鬥直升機', '戰鬥直升機', '戰鬥直升機', '戰鬥直升機', '戰鬥直升機', '戰鬥直升機', '戰鬥直升機');
    $Status = array('未取車(未付訂)','未取車(已付訂)','未取車(已付清)','已出車(未付訂)','已出車(已付訂)','已出車(已付清)','已還車','保留中','訂單取消');
?>
<html>
    <head>
        <script type="text/javascript" src="https://cdnjs.cloudflare.com/ajax/libs/moment.js/2.22.2/moment.min.js"></script>
        <script type="text/javascript" src="../js/functions.js"></script>
        
        <script type="text/javascript" src="https://code.jquery.com/jquery-3.3.1.slim.min.js"></script>
        <script type="text/javascript" src="https://cdnjs.cloudflare.com/ajax/libs/twitter-bootstrap/4.3.1/js/bootstrap.bundle.min.js"></script>
        <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/twitter-bootstrap/4.3.1/css/bootstrap.min.css">

        <link rel="stylesheet" href="//code.jquery.com/ui/1.12.1/themes/base/jquery-ui.css">
        <script src="https://code.jquery.com/jquery-1.12.4.js"></script>
        <script src="https://code.jquery.com/ui/1.12.1/jquery-ui.js"></script>

        <script type="text/javascript" src="../js/lightpick.js"></script>
        <link rel="stylesheet" type="text/css" href="../js/lightpick.css">
        <style>
            .tabcontent {
                display: none;
                padding: 6px 12px;
                border: 1px solid #ccc;
                margin-bottom: 50px;
            }
            .active{
            }
            .disabled{
            }
            .tablinks{
                width: 250px;
                height: 80px;
                font-size: 30px;
                color: WHITE;
                background-color: #0091FF;
                border-radius:15px;
            }
            td{
                padding:10px
            }
            #room_status_table tr:nth-child(odd){
                background-color: #f2f2f2;
            }
            #room_status_table th {
                padding:10px;
                text-align: left;
                background-color: #6236FF;
                color: WHITE;
            }
            #payment_status_table tr:nth-child(odd){
                background-color: #f2f2f2;
            }
            #payment_status_table th {
                padding:10px;
                text-align: left;
                background-color: #6236FF;
                color: WHITE;
            }
            .modified_select:-moz-focusring {
                color: transparent;
                text-shadow: 0 0 0 #000;
            }
            .modified_select{
                border: solid 3px #DADADA;
                -webkit-appearance: none;
                -moz-appearance: none;
                background: url("../images/dropdown-arrow-icon.png") no-repeat scroll right center transparent;
                background-size: 18.51px 16.03px;
                background-origin: content-box;
                padding-left: 10px;
                padding-right: 10px;
                background-repeat: no-repeat;
                border-radius:7.5px;
            }
            .ui-dialog-titlebar{
                display: none
            }
            .ui-widget.ui-widget-content{
                border-radius: 20px;
                /* border-width: 20px; */
                /* background-color: #DADADA;
                border: 1px solid #DADADA; */
            }
            .ui-widget-content {
                /* border-radius: 20px; */
                /* border-width: 20px; */
            }
            .ui-widget-overlay{
                /* background-color: transparent; */
            }
            #modify_btn{
                display: none;
            }
            .info_edit_column{
                display: none
            }
            .Customer_Info_Input{
                display:none;
            }
            .extra_expense_tr{
                display: none;
            }
            #extra_expense_table{
                table-layout: fixed;
                overflow: hidden;
                font-size: 25px;
                border-collapse: collapse;
                width: 100%;
            }
            #extra_expense_table td{
                table-layout: fixed;
                overflow: hidden;
                white-space: nowrap;
                text-overflow: ellipsis;
            }
            #extra_expense_table tr:nth-child(odd){
                background-color: #f2f2f2;
            }
            #extra_expense_table th {
                padding:10px;
                text-align: center;
                background-color: #6236FF;
                color: WHITE;
            }
            input, select{
                font-size: 26px;
                border-radius: 7.5px;
            }
            .modified_select:-moz-focusring {
                color: transparent;
                text-shadow: 0 0 0 #000;
            }
            .modified_select{
                border: solid 3px #DADADA;
                -webkit-appearance: none;
                -moz-appearance: none;
                background: url("../images/dropdown-arrow-icon.png") no-repeat scroll right center transparent;
                background-size: 18.51px 16.03px;
                background-origin: content-box;
                padding-left: 10px;
                padding-right: 10px;
                background-repeat: no-repeat;
                border-radius:15px;
            }
            .ui-widget.ui-widget-content{
                border-radius: 20px;
                border-width: 20px;
                background-color: #DADADA;
                border: 1px solid #DADADA;
            }
            .ui-widget-content {
                border-radius: 20px;
                border-width: 20px;
            }
            .ui-widget-overlay{
                background-color: transparent;
            }
            .ui-dialog-titlebar{
                display: none
            }
        </style>
    </head>
    <body onload="includeHTML();">
        <div class='navbar-div' include-html="../navbar.php"></div>
        <div class='for_hyper left' include-html="../hyper.php"></div>
        <div class='right'>
                    <center>
                        <table width='95%' border='0'>
                            <tr>
                                <td style='width:150px;text-align:center'>
                                    <button style='width:130px;height:50px;font-size:24px' onclick='goback()'>返回</button>
                                </td>
                                <td>
                                    <button class='tablinks active disabled' onclick='openTab(event, "info")'>訂單資訊</button>
                                    <!-- <button class='tablinks' onclick='openTab(event, "payment")'>收款結帳</button> -->
                                </td>
                            </tr>
                            <tr>
                                <td>
                                </td>
                                <td>
                                    <div id='info' class='tabcontent' style='display: block;'>
                                        <table style='font-size:25px' width='100%'>
                                            <tr>
                                                <td width='50%'>
                                                    訂單編號：
                                                    <span style='color:#0091FF' id='Order_ID'>
                                                        <?php echo $Order_ID;?>
                                                    </span>
                                                </td>
                                                <td width='50%' style='text-align:right'>
                                                    <!-- <input type='button' style='width:130px;height:50px;font-size:20px;margin:12.5px;border-radius:15px;display:inline' id='edit' value='編輯訂單' onClick='modify_infos()'> -->
                                                    <form style='display:inline' method='POST' action='Print_Order.php'>
                                                        <button type='submit' style='width:180px;height:50px;font-size:20px;margin:12.5px;border-radius:15px;background-color:#F79B00;color:WHITE' name='Order_ID' value='<?php echo $Order_ID;?>' onclick='print_order()'>列印訂單</button>
                                                    </form>
                                                    <!-- <input type='button' style='width:180px;height:50px;font-size:20px;margin:12.5px;border-radius:15px;background-color:#F79B00;color:WHITE' id='add_expense' value='新增消費'> -->
                                                    <input type='hidden' id='Overall_Status' value=''>
                                                    <input type='hidden' id='Customer_ID' name='Customer_ID' value='<?php echo $Order_Info['Customer_ID'];?>'>
                                                    <input type='hidden' id='Guarantor_ID' name='Guarantor_ID' value='<?php echo $Order_Info['Guarantor_ID'];?>'>
                                                </td>
                                            </tr>
                                            <tr>
                                                <td>
                                                    訂車日期：<?php echo date_format(date_create($Order_Info['Created_DT']),"Y-m-d H:i");?>
                                                </td>
                                                <td>
                                                    客戶姓名：<?php echo $Customer_Info['Customer_Name'];?>
                                                </td>
                                            </tr>
                                            <tr>
                                                <td>
                                                    租車日期：<?php echo date_format(date_create($Order_Info['Estimated_Rent_DateTime']),"Y-m-d H:i");?>
                                                </td>
                                                <td>
                                                    客戶性別：<?php echo $sex_array[$Customer_Info['Customer_Sex']];?>
                                                </td>
                                            </tr>
                                            <tr>
                                                <td>
                                                    還車日期：<?php echo date_format(date_create($Order_Info['Estimated_Return_DateTime']),"Y-m-d H:i");?>
                                                </td>
                                                <td>
                                                    客戶生日：<?php echo $Customer_Info['Customer_BDay'];?>
                                                </td>
                                            </tr>
                                            <tr>
                                                <td>
                                                    實際出車：<?php if($Order_Info['Actual_Rent_DateTime'] != "0000-00-00 00:00:00") echo date_format(date_create($Order_Info['Actual_Rent_DateTime']),"Y-m-d H:i");?>
                                                </td>
                                                <td>
                                                    客戶身分證/護照：<?php echo $Customer_Info['Customer_SSID'];?>
                                                </td>
                                            </tr>
                                            <tr>
                                                <td>
                                                    實際還車：<?php if($Order_Info['Actual_Return_DateTime'] != "0000-00-00 00:00:00") echo date_format(date_create($Order_Info['Actual_Return_DateTime']),"Y-m-d H:i");?>
                                                </td>
                                                <td>
                                                    連絡電話：<?php echo $Customer_Info['Customer_Phone'];?>
                                                </td>
                                            </tr>
                                            <tr>
                                                <td>
                                                    租車天數：<?php echo $Order_Info['Day_Count'];?>
                                                </td>
                                                <td>
                                                    聯絡地址：<?php echo $Customer_Info['Customer_Address'];?>
                                                </td>
                                            </tr>
                                            <tr>
                                                <td>
                                                    租車地點：<?php echo $Order_Info['Rental_Area'];?>
                                                </td>
                                                <td>
                                                    擔保人：<?php 
                                                        if(!empty($Guarantor_Info))
                                                            echo $Guarantor_Info['Customer_Name'] . " <button type='button' class='function_btn' style='color:black' value='" . $Guarantor_Info['Customer_ID'] . "' onclick='Show_Guarantor(this.value)'>點擊查看</button>";
                                                        else
                                                            echo "無";
                                                        ?>
                                                </td>
                                            </tr>
                                            <tr>
                                                <td>
                                                    還車地點：<?php echo $Order_Info['Return_Area'];?>
                                                </td>
                                                <td>
                                                    緊急連絡人：<?php echo $Order_Info['Emergency_Name'];?>
                                                </td>
                                            </tr>
                                            <tr>
                                                <td>
                                                    車輛資訊：<?php echo $Vehicle_Info['Car_Type'] . " " . $Vehicle_Info['Car_Capacity'] . "人座" . " / " . $Vehicle_Info['License_Plate'];?>
                                                </td>
                                                <td>
                                                    連絡人電話：<?php echo $Order_Info['Emergency_Phone'];?>
                                                </td>
                                            </tr>
                                            <tr>
                                                <td>
                                                    總價：<?php echo $Order_Info['Total_Price'];?>
                                                </td>
                                                <td>
                                                </td>
                                            </tr>
                                            <tr>
                                                <td>
                                                    訂單狀態：<span id='Status'><?php echo $Status[$Order_Info['Status']];?></span>
                                                </td>
                                                <td>
                                                    負責人：<?php 
                                                        $sql = "SELECT `employee_list`.`Employee_Name` FROM `order_list`,`employee_list` WHERE `order_list`.`Employee_ID`=`employee_list`.`Employee_ID` AND `order_list`.`Order_ID`='".$Order_ID."'";
                                                        $result = mysqli_query($conn,$sql);
                                                        $result = $result->fetch_assoc();
                                                        echo $result['Employee_Name'];?>
                                                </td>
                                            </tr>
                                            <tr>
                                                <td></td>
                                                <td></td>
                                            </tr>
                                            <tr>
                                                <td colspan='2' style='text-align:right'>
                                                    <div id='default_btn'>
                                                        <input type='button' value='確定' style='width:130px;height:50px;font-size:20px;margin-left:40px;border-radius:15px;background-color:#0091FF;color:white' onClick='goback()'>
                                                        <?php
                                                            if($Order_Info['Status']<3){
                                                                echo "<input type='button' value='辦理取車' id='SC_btn' class='disabled' style='width:130px;height:50px;font-size:20px;margin-left:40px;border-radius:15px;background-color:#0091FF;color:white' onClick='change_status(\"$Order_ID\")'"; if($SC_Flag) echo " disabled"; echo ">";
                                                            }
                                                            else if($Order_Info['Status']<6){
                                                                echo "<input type='button' value='辦理還車' id='SC_btn' class='disabled' style='width:130px;height:50px;font-size:20px;margin-left:40px;border-radius:15px;background-color:#0091FF;color:white'>";
                                                            }
                                                        ?>
                                                        <input type='button' value='取消訂單' id='cancel_booking_btn' style='width:130px;height:50px;font-size:20px;margin-left:40px;border-radius:15px;background-color:#0091FF;color:white' onClick='cancel_booking("<?php echo $Order_ID;?>")'>
                                                    </div>
                                                    <div id='modify_btn'>
                                                        <input type='button' value='完成' id='完成' style='width:130px;height:50px;font-size:20px;margin-left:40px;border-radius:15px;background-color:#0091FF;color:white' onClick='complete_modify()'>
                                                        <input type='button' value='取消' style='width:130px;height:50px;font-size:20px;margin-left:40px;border-radius:15px;background-color:#0091FF;color:white' onClick='cancel_modify()'>
                                                    </div>
                                                </td>
                                            </tr>
                                        </table>
                                    </div>

                                    <!-- <div id='payment' class='tabcontent'>
                                        <table style='font-size:25px' width='100%'>
                                                <tr>
                                                    <td width='30%' style='font-size:50px'>總額：<span id='Total_Price_Display'></span>
                                                    <td width='40%'>
                                                        <span style='font-size:25px;margin-right:20px'>已收金額：<span id='already_paid'>0</span></span>
                                                        <span style='font-size:25px'>待收金額：<span id='Unpaid'>106072</span></span>
                                                    </td>
                                                    <td width='30%' style='text-align:right'> <input type='button' style='width:180px;height:50px;font-size:20px;background-color:#F79B00;color:WHITE;border-radius:15px' value='新增收款' onClick='show_payment_dialog()'>
                                                    </td>
                                                </tr>
                                                <tr>
                                                    <td height='10px'></td>
                                                </tr>
                                                <tr>
                                                    <td>使用專案： 現場房價</td>
                                                </tr>
                                                <tr>
                                                    <td>收款明細：</td>
                                                </tr>
                                                <tr>
                                                    <td colspan='3'>
                                                        <table id='payment_status_table' border='1' style='font-size:25px;border-collapse:collapse;' width='100%'>
                                                            <tr class='title'>
                                                                <th>類型</th>
                                                                <th>金額</th>
                                                                <th>方式</th>
                                                                <th>收款時間</th>
                                                                <th>備註</th>
                                                            </tr>
                                                            <tr>
                                                                <td>退款</td>
                                                                <td>0</td>
                                                                <td>其它</td>
                                                                <td>2020-07-15 00:00:00</td>
                                                                <td></td>
                                                            </tr>
                                                        </table>
                                                    </td>
                                                </tr>
                                                <tr>
                                                    <td colspan='2' height='30px'></td>
                                                </tr>
                                                <tr>
                                                    <td colspan='3' style='text-align:right'>
                                                        <input type='button' value='確定' style='width:130px;height:50px;font-size:20px;margin-left:40px;border-radius:15px;background-color:#0091FF;color:white' onClick='goback()'>
                                                    </td>
                                                </tr>
                                            </table>
                                    </div> -->
                                </td>
                            </tr>
                        </table>
                    </center>
        </div>
        <!-- 此程式由嵐叔獨力完成，若有相關需求，歡迎聯絡Facebook「嵐叔＆貓貓的日常」粉絲專頁 -->
    </body>
</html>
<!----------Dialog---------->
    <div id='Guarantor_Info' name='dialog_section' style='background-color:#DADADA'><br>
        <center>
            <input type="hidden" autofocus="true" />
            <table width='100%' style='table-layout: fixed;'>
                <tr>
                    <td style='text-align:right;font-size:26px;width:30%;height:59px'>保證人姓名：</td>
                    <td style='font-size:26px;' colspan='2'><span class='display_text' id='Customer_Name_Text'></span></td>
                </tr>
                <tr>
                    <td style='text-align:right;font-size:26px;;height:59px'>身分證號：</td>
                    <td style='font-size:26px;' colspan='2'><span class='display_text' id='Customer_SSID_Text'></span></td>
                </tr>
                <tr>
                    <td style='text-align:right;font-size:26px;;height:59px'>保證人性別：</td>
                    <td style='font-size:26px;' colspan='2'><span class='display_text' id='Customer_Sex_Text'></span></td>
                </tr>
                <tr>
                    <td style='text-align:right;font-size:26px;;height:59px'>保證人生日：</td>
                    <td style='font-size:26px;' colspan='2'><span class='display_text' id='Customer_BDay_Text'></span></td>
                </tr>
                <tr>
                    <td style='text-align:right;font-size:26px;;height:59px'>連絡電話：</td>
                    <td style='font-size:26px;' colspan='2'><span class='display_text' id='Customer_Phone_Text'></span></td>
                </tr>
                <tr>
                    <td style='text-align:right;font-size:26px;;height:59px'>E-Mail：</td>
                    <td style='font-size:26px;' colspan='2'><span class='display_text' id='Customer_Email_Text'></span></td>
                </tr>
                <tr>
                    <td style='text-align:right;font-size:26px;;height:59px'>保證人住址：</td>
                    <td style='font-size:26px;' colspan='2'><span class='display_text' id='Customer_Address_Text'></span></td>
                </tr>
                <tr>
                    <td style='text-align:right;font-size:26px;;height:59px'>保證人狀態：</td>
                    <td style='font-size:26px;' colspan='2'><span class='display_text' id='Customer_Status_Text'></span></td>
                </tr>
                <tr>
                    <td style='text-align:right;font-size:26px;vertical-align:top'>備註：</td>
                    <td style='font-size:26px;' colspan='2'><span class='display_text' id='Customer_Remark_Text'></span></td>
                </tr>
            </table>
            <input type='button' class='function_btn' style='background-color:#0091FF;font-size:24px;' value='確定' onClick='Close_Dialog()'></div>
        </center>
    </div>
<!-------------------------->
<script>
    
    var gender = ['女','男','不明', '戰鬥直升機'];
    var Customer_Status = ['正常','黑名單'];
    var Status = ['未取車(未付訂)','未取車(已付訂)','未取車(已付清)','已出車(未付訂)','已出車(已付訂)','已出車(已付清)','已還車','保留中','訂單取消'];

    $(document).ready(function(){
        $("#Guarantor_Info").dialog({
            height: 680,
            width: 700,
            autoOpen: false,
            resizable: false,
            draggable: false,
            modal: true
            // dialogClass: 'ui-dialog-osx',
        });
    });

    function Show_Guarantor(Guarantor_ID){
        $.ajax({
            type: "POST",
            url: "service.php",
            dataType: "json",
            data: {
                Feature: "show_guarantor_info",
                Guarantor_ID: Guarantor_ID
            },
            success: function(data) {
                $("#result").html('');
                var json_array = data;
                $("#Customer_Name_Text").html(json_array.Customer_Name);
                $("#Customer_Sex_Text").html(gender[json_array.Customer_Sex]);
                $("#Customer_SSID_Text").html(json_array.Customer_SSID);
                $("#Customer_BDay_Text").html(json_array.Customer_BDay);
                $("#Customer_Phone_Text").html(json_array.Customer_Phone);
                $("#Customer_Email_Text").html(json_array.Customer_Email);
                $("#Customer_Address_Text").html(json_array.Customer_Address);
                $("#Customer_Status_Text").html(Customer_Status[json_array.Customer_Status]);
                $("#Customer_Remark_Text").html(json_array.Customer_Remark.replace(/\r?\n|\r/g, "<br>"));
                // console.log(data);
                $( "#dialog" ).dialog( "open" );
            },
            error: function(jqXHR) {
                $("#result").html('<font color="#ff0000">發生錯誤：GGGGGGGGGGGGGGGG' + jqXHR.responseText + '</font>');
                // console.log(jqXHR.responseText);
            }
        })
        $( "#Guarantor_Info" ).dialog( "open" );
    }

    $("body").on("click",".ui-widget-overlay",function() {
        $('#Guarantor_Info').dialog( "close" );
    });

    function Close_Dialog(){
        $('#Guarantor_Info').dialog( "close" );
    }

    function change_status(OID){
        console.log("OHO");
        $.ajax({
            type: "POST",
            url: "service.php",
            dataType: "json",
            data: {
                Feature: "change_status",
                OID: OID
            },
            success: function(data) {
                $("#Status").html(Status[data.Status]);
                // $("#cancel_booking_btn").prop('disabled',true);
                // alert("訂單已取消");
            },
            error: function(jqXHR) {
                // $("#result").html('<font color="#ff0000">發生錯誤：GGGGGGGGGGGGGGGG' + jqXHR.responseText + '</font>');
                alert("操作有誤！請連絡開發者以快速解決問題！");
                console.log(jqXHR.responseText);
            }
        })
        console.log("OHO????");

    }

    function cancel_booking(OID){
        $.ajax({
            type: "POST",
            url: "service.php",
            dataType: "json",
            data: {
                Feature: "cancel_booking",
                OID: OID
            },
            success: function(data) {
                $("#Status").html(Status[8]);
                $("#cancel_booking_btn").prop('disabled',true);
                alert("訂單已取消");
            },
            error: function(jqXHR) {
                // $("#result").html('<font color="#ff0000">發生錯誤：GGGGGGGGGGGGGGGG' + jqXHR.responseText + '</font>');
                alert("操作有誤！請連絡開發者以快速解決問題！");
                console.log(jqXHR.responseText);
            }
        })
        // console.log(OID);
    }

    function goback(){
        window.history.back();
    }
    
</script>