<?php
    include_once "../session_stat.php";
    include_once "../mysql_connect.inc.php";
    if ($_SERVER['REQUEST_METHOD'] == "POST") {
        if(isset($_POST['Order_ID'])){
            $Order_ID = $_POST['Order_ID'];
            $sql = "SELECT * FROM `order_list` WHERE `Order_ID`='".$Order_ID."'";
            $Order_Info = mysqli_query($conn,$sql)->fetch_assoc();
            $sql = "SELECT * FROM `customer_list` WHERE `Customer_ID`='".$Order_Info['Customer_ID']."'";
            $Customer_Info = mysqli_query($conn,$sql)->fetch_assoc();
            $sql = "SELECT * FROM `customer_list` WHERE `Customer_ID`='".$Order_Info['Guarantor_ID']."'";
            $Guarantor_Info = mysqli_query($conn,$sql)->fetch_assoc();
            $sql = "SELECT * FROM `car_list`,`car_type` WHERE `car_list`.`Car_Type` = `car_type`.`Car_Type` AND `car_list`.`License_Plate` = '".$Order_Info['License_Plate']."'";
            $Vehicle_Info = mysqli_query($conn,$sql)->fetch_assoc();
        }
        else{
            echo '<meta http-equiv=REFRESH CONTENT=0;url=index.php>';
            die;
        }
    }
    else{
        echo '<meta http-equiv=REFRESH CONTENT=0;url=index.php>';
        die;
    }
?>

<!-- saved from url=(0061)https://www.kuruma.com.tw/adm/api/printOrder/20201012093406RX -->
<html>
    <!-- <script type="text/javascript">window["_gaUserPrefs"] = { ioo : function() { return true; } }</script> -->
    <!-- <script src="chrome-extension://jhffgcfmcckgmioipfnmbannkpncfipo/util.js"></script><script src="chrome-extension://jhffgcfmcckgmioipfnmbannkpncfipo/pagejs.js"></script> -->
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <style type="text/css">
            @CHARSET "UTF-8";
            *{
                font-size: 12px !important;
            }
            html, body{
                margin: 0;
                font-family: Arial, Helvetica, sans-serif;
                font-size: 12px;
                line-height: 22px;
            }
            #container{
                margin: 0 auto;
                width: 595px; /* A4 */
                /*height: 842px;*/ /* A4 */
            }
            .no_print{
                display: none;
            }
            .table{
                border-collapse: collapse;
                width: 100%;
            }
            .table th{
                border: solid 1px #000;
                padding: 4px;
                text-align: left;
            }
            .table td{
                border: solid 1px #000;
                padding: 4px;
            }
            p{
                margin: 0;
            }
            .w50{
                width: 50px;
            }
            pre {
                width:95%;  
                white-space: pre-wrap; /* css-3 */
                white-space: -moz-pre-wrap; /* Mozilla, since 1999 */
                white-space: -pre-wrap; /* Opera 4-6 */
                white-space: -o-pre-wrap; /* Opera 7 */
                word-wrap: break-word; /* Internet Explorer 5.5+ */
            }
            @page { size: portrait; }
        </style>
    </head>

    <body style="background-image: none; background-color: #fff;">
        <div style="padding: 10px;">
            <div style="font-weight: bold;margin-bottom: 5px;">日期:&nbsp;<?php echo $Order_Info['Created_DT'];?></div>
            <div style="font-size: 20px; text-align: center;">出租人(簡稱甲方):華誼優質租賃有限公司</div>
            <div style="font-size: 20px; text-align: right;">(附表一)</div>
                
            <table width="100%" cellpadding="0" cellspacing="0" class="table">
                <tbody>
                        <tr>
                        <td width="50%" valign="top"><table width="100%" cellpadding="0" cellspacing="0" class="table">
                            <tbody>
                            <tr>
                                <td colspan="2">
                                <strong><?php echo $Order_Info['Order_ID'];?></strong><center><strong>承租人資料(簡稱:乙方)</strong></center></td>
                            </tr>
                            <tr>
                                <td width="45%">姓名：<?php echo $Customer_Info['Customer_Name'];?></td>
                                <td width="55%">護照/身分證：<?php echo $Customer_Info['Customer_SSID'];?></td>
                            </tr>
                            <tr>
                                <td>出生日期：<?php echo $Customer_Info['Customer_BDay'];?></td>
                                <td>&nbsp;</td>
                            </tr>
                            <tr>
                                <td>聯絡電話：<?php echo $Customer_Info['Customer_Phone'];?></td>
                                <td>&nbsp;</td>
                            </tr>
                            <tr>
                                <td colspan="2">聯絡地址：<?php echo $Customer_Info['Customer_Address'];?></td>
                            </tr>
                            <tr>
                                <td colspan="2">通訊地址：</td>
                            </tr>
                            <tr>
                                <td><span style="color: #FF0000">緊急聯絡人：</span><?php echo $Order_Info['Emergency_Name'];?></td>
                                <td><span style="color: #FF0000">緊急聯絡人電話：</span><?php echo $Order_Info['Emergency_Phone'];?></td>
                            </tr>
                            <tr>
                                <td colspan="2" align="center"><strong>共同承租人(擔保人)</strong></td>
                            </tr>
                            <tr>
                                <td>姓名：<?php echo $Guarantor_Info['Customer_Name'];?></td>
                                <td>護照/身分證：<?php echo $Guarantor_Info['Customer_SSID'];?></td>
                            </tr>
                            <tr>
                                <td>出生日期：<?php echo $Guarantor_Info['Customer_BDay'];?></td>
                                <td>出生地/籍貫：</td>
                            </tr>
                            <tr>
                                <td>住宅電話：<?php echo $Guarantor_Info['Customer_Phone'];?></td>
                                <td>行動電話：<?php echo $Guarantor_Info['Customer_Phone'];?></td>
                            </tr>
                            <tr>
                                <td colspan="2">聯絡地址：<?php echo $Guarantor_Info['Customer_Address'];?></td>
                            </tr>
                            <tr>
                                <td></td>
                                <td></td>
                            </tr>
                            <tr>
                                <td colspan="2" align="center"><strong>還車檢查</strong></td>
                            </tr>
                            <tr>
                                <td>出車里程：</td>
                                <td>還車里程：</td>
                            </tr>
                            <tr>
                                <td colspan="2"><span style="font-size: 20px">□</span>導航 <span style="font-size: 20px">□</span>行車記錄器 <span style="font-size: 20px">□</span>安全座椅<span style="font-size: 20px"> □</span>音源線 <span style="font-size: 20px">□</span>多合一 <span style="font-size: 20px">□</span>其他</td>
                            </tr>
                            <tr>
                                <td colspan="2"><span style="font-size: 20px">□</span>還車未達油量基準＄  <span style="font-size: 20px"> □</span>證件歸還</td>
                            </tr>
                            <tr>
                                <td colspan="2"><span style="font-size: 20px">□</span>歸還設備與出車不相符             <span style="font-size: 20px"> □</span>車況與出車不相符，須整備維修。</td>
                            </tr>
                            <tr>
                                <td><span style="font-size: 20px">□</span>維修費用 NT＄　</td>
                                <td><span style="font-size: 20px">□</span>自負額     NT＄　</td>
                            </tr>
                            <tr>
                                <td colspan="2"><span style="font-size: 20px">□</span>營業損失 NT＄　</td>
                            </tr>
                            <tr>
                                <td colspan="2"><img width="370" height="190" src="car_map.png" alt="車輛平面圖"></td>
                            </tr>
                            </tbody>
                        </table></td>
                        <td width="50%"><table width="100%" cellpadding="0" cellspacing="0" class="table">
                            <tbody>
                            <tr>
                                <td colspan="2" align="center"><strong>出租車輛資訊及費用</strong></td>
                            </tr>
                            <tr>
                                <td width="50%"><span style="color: #FF0000">表定出車：</span><?php echo date_format(date_create($Order_Info['Estimated_Rent_DateTime']),"Y-m-d H:i");?></td>
                                <td width="50%"><span style="color: #FF0000">表定還車：</span><?php echo date_format(date_create($Order_Info['Estimated_Return_DateTime']),"Y-m-d H:i");?></td>
                            </tr>
                            <tr>
                                <td width="50%"><span style="color: #FF0000">實際出車：</span><?php if($Order_Info['Actual_Rent_DateTime'] != "0000-00-00 00:00:00")echo date_format(date_create($Order_Info['Actual_Rent_DateTime']),"Y-m-d H:i");?></td>
                                <td width="50%"><span style="color: #FF0000">實際還車：</span><?php if($Order_Info['Actual_Return_DateTime'] != "0000-00-00 00:00:00") echo date_format(date_create($Order_Info['Actual_Return_DateTime']),"Y-m-d H:i");?></td>
                            </tr>
                            <tr>
                                <td>取車地點：<?php echo $Order_Info['Rental_Area'];?></td>
                                <td>還車地點：<?php echo $Order_Info['Return_Area'];?></td>
                            </tr>
                            <tr>
                                <td colspan="2">出租廠牌/車型/顏色：<?php echo $Vehicle_Info['Car_Type'] . "/" . $Vehicle_Info['Car_Capacity'] . "座";?></td>
                            </tr>
                            <tr>
                                <td colspan="2">牌照號碼：<?php echo $Vehicle_Info['License_Plate'];?></td>
                            </tr>
                            <tr>
                                <td colspan="2">輪胎胎紋胎壓是否符合標準 <span style="font-size: 24px">□</span>符合<span style="font-size: 24px">□</span> 不符合</td>
                            </tr>
                                            <tr>
                                <!--<td colspan="2"><span style="font-size: 20px">□</span>導航 <span style="font-size: 20px">□</span>行車記錄器 <span style="font-size: 20px">□</span>安全座椅<span style="font-size: 20px"> □</span>音源線   <span style="font-size: 20px">□</span>多合一 <span style="font-size: 20px">□</span>其他</td>-->
                                <td colspan="2">&nbsp;</td>
                            </tr>
                            <tr>
                                <td>配件租金：0</td>
                                <td>押金：</td>
                            </tr>
                            <tr>
                                <td colspan="2"><span style="color: #FF0000">租金合計：<?php echo number_format($Order_Info['Total_Price'], 0, '', ',');?></span></td>
                            </tr>
                                            <tr>
                                <td>訂金：0</td>
                                <td>尾款：0</td>
                            </tr>
                            
                            <tr>
                                <td>e Tag：</td>
                                <td>停車費：</td>
                            </tr>
                            <tr>
                                <td>清潔費：</td>
                                <td>發票號碼</td>
                            </tr>
                            <tr>
                                <td colspan="2"> <span style="font-size: 20px">□</span>信用卡   <span style="font-size: 20px">□</span>保證人   <span style="font-size: 20px"> □</span>其它  (              )</td>
                            </tr>
                            <tr>
                                <td colspan="2">出車油量：F  <span style="font-size: 20px !important">□□□□</span>         Ｅ  <span style="font-size: 20px !important">□□□□</span></td>
                            </tr>
                                            <tr>
                                <td colspan="2" align="center"><strong>其他費用</strong></td>
                            </tr>
                            <tr>
                                <td>超駛公里：</td>
                                <td>逾時時數：</td>
                            </tr>
                            <tr>
                                <td><span style="color: #FF0000">超駛租金：</span></td>
                                <td><span style="color: #FF0000">逾時租金：</span></td>
                            </tr>
                            <tr>
                                <td>保證金：</td>
                                <td>統一編號：</td>
                            </tr>
                            <tr>
                                <td colspan="2" align="left">擔保品：<span style="font-size: 20px">□</span>汽車或機車乙輛車號_______________________
                                <span style="font-size: 20px">□</span>其他：（　　　　　　　　　　　　　）</td>
                            </tr>
                            <tr>
                                <td height="80" colspan="2" valign="top">
                                    外觀請承租人自行拍照錄影另行紀錄<br>
                                    外觀確認：___________<br>
                                    信用卡號碼：_________________ 有效日期：____ 末3碼：____<br>
                                    承租人簽名：_________________ 華誼服務專線：03-378-2223                
                                </td>
                            </tr>
                            <tr>
                                <td colspan="2"><span style="color: #FF0000">本人已於簽署前經合理期間審閱確認後同意本合約各項條款，並了解保險未含零配件之損竊，若有損竊發生承租人願負擔損害賠償之責。若以信用卡付款時以下簽名即表示授權出租人就有關租金費，以承租人名義處理信用卡收款單以供付款。</span></td>
                            </tr>
                            </tbody>
                        </table></td>
                        </tr>
                        <tr>
                        <td colspan="2"><table width="100%" cellpadding="0" cellspacing="0" class="table">
                            <tbody>
                            <tr>
                                <td>出車專員簽名:</td>
                                <td>收車專員簽名: 　</td>
                                <td>承租人還車確認簽名:</td>
                            </tr>
                            </tbody>
                        </table></td>
                        </tr>
                </tbody>
            </table>

                
            <p style="page-break-after:always"></p> 
            <div style="padding: 10px;width:100%;">
                <table cellpadding="0" cellspacing="0">
                    <tbody>
                        <tr>
                            <td>
                                <pre style="font-size:9pt; line-height: 12px;">小客車租賃定型化契約交通部104.05.08交路字第10400089287號公告修正，並自即日生效

第一條  契約當事人：出租人（以下簡稱甲方）承租人（以下簡稱乙方）玆為出租小客車乙事，雙方同意訂立本契約書。

第二條  本車輛及隨車配件，詳如附表一。 本車輛出車前車況圖，詳如附表一。

第三條  租賃期間及租金詳如附表一。

第四條  除強制汽車責任險外，乙方欲加其他保險，請於租車時付清保險費，保險不得中途加保或退保，上述保險不包含因自然災害(如洪水、颱風、地震等)造成之損傷及車內配備遺失或損壞之賠償責任。

第五條  付款方式及擔保方式詳如附表一，甲方於乙方交還車輛時，經檢查確無損壞或遺失配件後，應即無息返還前項保證金或擔保品。

第六條  本車輛每日平均行駛最高里程為四百公里，逾最高里程者，每一公里加收三元累計，但每日加收金額不得逾一日租金之半數。乙方應依約定時間交還車輛，還車時間逾一小時者，每滿一小時按每日租金十分之一計算收費，逾期六小時以上者，以一日之租金計算收費。乙方於約定使用時間屆滿前交還車輛，且提前還車時間滿一日以上者，得請求甲方退還每滿一日部分之租金，遇連續假日則不適用。前項因車輛本身機件故障或不可歸責於乙方之事由，致乙方不能依約定時間交還車輛者，不在此限。乙方有前項情形得為通知者，乙方應即為通知甲方。

第七條  本車輛使用之燃料由乙方自備，乙方並應購用合法銷售之燃料。乙方違反前項約定致車輛故障者，應負損害賠償責任，本車限用油料：(□98無鉛汽油  □95無鉛汽油  □92無鉛汽油 □高級柴油)。

第八條  本車輛不得超載，並不得載送下列物品：⑴違禁品。⑵危險品。⑶不潔或易於污損車輛之物品。⑷不適宜隨車之動物類。⑸其他：                   乙方應在約定範圍內使用車輛並自行駕駛，不得擅交無駕照之他人駕駛、從事汽車運輸業行為或充作教練車等用途。違反前二項約定，甲方得終止租賃契約，並即時收回車輛，如另有損害，並得向乙方請求賠償。

第九條  租賃期間乙方應隨車攜帶駕駛執照、強制汽車責任保險證、汽車出租單及行車執照以備查驗

乙方於租賃期間，因違規所生之處罰案件，有關罰鍰部分，由乙方負責繳清，如由甲方代為繳納者，乙方應負責償還。租賃期間所生之停車費，過路通行費等費用，概由乙方自行負擔；有關牌照、行車執照或車輛被扣部分，自被扣之日起至通知領回日止之費用，由乙方及連帶保證人負擔。(超速六十公里以上需吊牌三個月)。

第十條  乙方應盡善良管理人注意義務保管及維護本車輛，禁止出賣、設質、抵押、讓與擔保、典當車輛等行為。

第十一條  本車輛發生擦撞或毀損，除有不能向警察機關報案之情形外，乙方應立即報案並通知甲方後送 □原廠 □雙方同意            廠修理，如因可歸責於乙方之事由所生之拖車費、修理費、折舊費及第十二條後段規定車輛修理期間之租金，應由乙方負擔。

第十二條  因可歸責於乙方之事由致本車輛毀損達無法修復程度者，應照當時市價賠償；毀損但可修復者，修理期間在十日以內者，並應償付該期間百分之七十之租金；在十一日以上十五日以內，並應償付該期間百分之六十之租金；在十六日以上者，並應償付該期間百分之五十之租金。但期間之計算，最長以二十日為限。

第十三條  本車輛遺失或被盜者，除有不能向警察機關報案之情形外，乙方應立即報案並通知甲方。

第十四條  因可歸責於乙方之事由致本車輛遺失或被盜者，乙方應照當時市價賠償，如本車輛有投保竊盜損失保險者，乙方僅支付市價與保險賠償金額之差額。乙方未賠償前失竊車輛經尋獲者，其賠償金額準用第十一條及第十二條規定處理；乙方已賠償後失竊車輛經尋獲者，如本車輛未投保竊盜損失保險者，甲方應即將該車輛過戶予乙方。

第十五條  甲方應擔保租賃期間內本車輛合於約定使用狀態，如有違反，雙方得依物之瑕疵擔保或債務不履行等相關法律規定辦理。

第十六條  乙方還車地點(依附表一所示)。前項還車地點，在甲方交車地點以外之其他處所者，甲方另收取成本費新臺幣                      元整。

第十七條  因本契約發生訴訟時，甲乙雙方同意以甲方地方法院為第一審管轄法院，但不得排除消費者保護法第四十七條及民事訴訟法第四百三十六條之九規定之小額訴訟管轄法院之適用。

第十八條  乙方欲續租本車輛者，應在甲方營業時間內事先聯繫並取得甲方之同意，始為有效。乙方未依約繳租金或未徵得甲方同意續租而逾期未繳租金時，該出租車輛無論在何時、何地被甲方發現，甲方得不管其車上有無人或物，乙方同意甲方免予報警，可逕行取回甲方所有之車輛，若因乙方未歸還甲方所有之車輛，經由甲方委託協尋公司取回之費用，應由乙方負擔，絕無異議。如車內物品有遺失或遭竊，概由乙方負責與甲方無關。

第十九條  本契約如有未訂事宜，依相關法令及誠信原則公平解決之。若有折舊費用之約定，不得超過修理費用百分之二十。

第二十條  甲方對乙方個人資料之蒐集、處理及利用，應依個人資料保護法規定，並負有保密義務，非經乙方書面同意，甲方不得對外揭露或為契約目的範圍外之利用。契約關係消滅後，亦同。

第二十一條  本契約一式二份，由甲、乙雙方各執一份為憑。

特別條款  承租人，應遵守道路交通管理處罰條例第35條:飲酒後不開車及同條例43條:不在道路上蛇行超速或以危險方式駕車，若擅自違規被舉發致車輛或牌照被扣，承租人願負法律責任及賠償一切損失，出車前車牌前後二面應懸掛完整。

本契約條款已於中華民國     年     月     日經承租人審閱完成。出租人(甲方)並應於簽約前，將內容逐條向承租人(乙方)說明，雙方謹定書面契約，以憑信守。

承租人確認簽章：
                                </pre>
                            </td>
                        </tr>
                    </tbody>
                </table>
            </div>


        </div>
    </body>
</html>
<script>
  if (window.print) {
      window.print();
  }

  window.onafterprint = function (){
        window.history.back();
  }
</script>