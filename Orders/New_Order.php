<?php
    include_once "../session_stat.php";
?>
<!DOCTYPE html>
<html>
    <head>
        <title>新增租車訂單</title>
        <meta content="text/html; charset=utf-8" http-equiv="Content-Type">
        <link rel="stylesheet" type="text/css" href="../js/lightpick.css">
        <script type="text/javascript" src="../js/functions.js"></script>
        <script type="text/javascript" src="https://cdnjs.cloudflare.com/ajax/libs/moment.js/2.22.2/moment.min.js"></script>
        <script type="text/javascript" src="../js/lightpick.js"></script>
        
        <script type="text/javascript" src="https://code.jquery.com/jquery-3.3.1.slim.min.js"></script>
        <script type="text/javascript" src="https://cdnjs.cloudflare.com/ajax/libs/twitter-bootstrap/4.3.1/js/bootstrap.bundle.min.js"></script>
        <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/twitter-bootstrap/4.3.1/css/bootstrap.min.css">

        <style>
            *{
                font-size:18px
            }
            textarea{
                border-radius:8px;
                margin:10px;
            }
            input,button,select{
                border-radius:8px;
                height: 35px;
                margin: 2.5px 0px !important;
            }
            input[type=checkbox], input[type=radio] {
                vertical-align: middle;
                /* zoom: 2; */
            }
            .section_option{
                height: 50px;
                width: 30%;
                font-size: 2vw;
                color: WHITE;
                background-color: #0091FF;
                border-radius: 15px;
            }
            .active{
                background-color: #F79B00;
            }
            .Input_Info{
                display: none;
                width: 95%;
            }
            .II_Active{
                display: inline-block;
            }
            .option_btn{
                width: 30%;
                height: 50px;
                background-color: #e5e9ef;
                font-size: 26px;
                transition: 0.3s;
                display: inline;
                margin: 2.5px 10px;
                padding: 0px 5px;
                text-overflow: ellipsis;
                white-space: nowrap;
                overflow: hidden;
            }
            .selected_btn{
                background-color: #1893E6;
                color: white;
            }
            .option_btn:hover{
                height: unset;
                min-height: 50px;
                text-overflow: unset;
                white-space: unset;
                overflow: unset;
                background-color: #1893E6;
                color: white;
            }
        </style>
    </head>
    
    <body onload="includeHTML();ValidateBothEmail();GetRentalPlaces();">
        <div class='navbar-div' include-html="../navbar.php"></div>
        <div class='for_hyper left' include-html="../hyper.php"></div>
        <div class='right'>
            <center><div>
                <p id='result'></p>
                <div style='width:95%;border: 1px solid GREY;border-radius:15px;margin-bottom: 20px;'>
                    <form id='reservation'>
                        <input type='hidden' id='Feature' name='Feature' value='Create_Order'>
                        <input type='hidden' id='Employee_ID' name='Employee_ID' value='1'>
                        <input type='hidden' class='Customer_Info' id='Customer_ID' name='Customer_ID' value=''>
                        <input type='hidden' class='Guarantor_Info' id='Guarantor_ID' name='Guarantor_ID' value=''>
                        <div style='width:46%;display:inline-block;vertical-align:top;table-layout: fixed;'><br>
                            <table border='0' cellspacing='0'><tr>
                                <tr>
                                    <td style='text-align:right;width:20%'>選擇日期：</td>
                                    <td colspan='2'>
                                        <div style='float:left;text-align:center;'>
                                            <input type='text' style='width:150px;margin-bottom:2.5px;' id='Start_Date' name='Start_Date' value='' placeholder='YY/MM/DD' required><br>
                                            <input type='time' name='Start_Date_Time' id='Start_Date_Time' style='margin-top:2.5px;' value='10:00' onchange='calculate_all_info()'>
                                        </div>
                                        <div style='float:left;margin-top:44px;'> ～ </div>
                                        <div style='float:left;text-align:center;'>
                                            <input type='text' style='width:150px;margin-bottom:2.5px;' id='End_Date' name='End_Date' value='' placeholder='YY/MM/DD' required><br>
                                            <input type='time' name='End_Date_Time' id='End_Date_Time' style='margin-top:2.5px;' value='12:00' onchange='calculate_all_info()'>
                                        </div>
                                    </td>
                                </tr>
                                <tr>
                                    <td style='text-align:right'>租車天數：</td>
                                    <td colspan='2'><input type='text' name='Day_Count' id='Day_Count' value='' readonly>天</td>
                                </tr>
                                <tr>
                                    <td style='text-align:right'>租車地點：</td>
                                    <td colspan='2'>
                                        <select id='Rent_Place' name='Rent_Place' style='width:150px' class='modified_select' onchange='Get_Car_List();calculate_all_info();' required>
                                            <option value=''>選擇地點</option>
                                        </select>
                                    </td>
                                </tr>
                                <tr>
                                    <td style='text-align:right;vertical-align:top;padding-top:20px'>
                                        車型：
                                    </td>
                                    <td style='padding-top:20px'>
                                        <div id='selection_area'>尚未租車條件或沒有車輛</div>
                                        <input type="hidden" name='selected_option' id='selected_option' value='' required>
                                        <p id='needed_text'></p>
                                        <input type='hidden' id='needed' value='0'>
                                    </td>
                                </tr>
                                <tr>
                                    <td style='text-align:right'>
                                        總金額：
                                    </td>
                                    <td>
                                        <input type='text' name='Total_Price' id='Total_Price' readonly required>
                                    </td>
                                </tr>
                                <tr>
                                    <td style='text-align:right'>
                                        備註：
                                    </td>
                                    <td>
                                        <textarea rows='3' cols='40' class='Customer_Info' name='Remark' style='resize: none;'></textarea>
                                    </td>
                                </tr>
                                <!-- <tr>
                                    <td style='text-align:right'>價格專案：</td>
                                    <td colspan='2'><select id='Discount_ID' name='Discount_ID' style='width:200px' class='modified_select' required><option value=''>選擇價格專案</option></select>&nbsp;&nbsp;<label><input type='checkbox' name='hold_room' value='1' onChange='hold_room_fnc(this)'>保留房</label></td>
                                </tr> -->
                            </table>
                        </div>
                        <div style='width:53%;display:inline-block;vertical-align:top;table-layout: fixed;'><br>
                            <button type='button' id='Lessee_Btn' class='section_option active' style='float:left' onclick='change_input_info(this,"Lessee")'>承租人</button>
                            <button type='button' id='Guarantor_Btn' class='section_option' style='float:right' onclick='change_input_info(this,"Guarantor")'>擔保人</button>
                            <table id='Lessee' class='Input_Info II_Active' border='0' cellspacing='0'>
                                <tr><td style='text-align:right;width: 35%;'>租車人資訊：</td><td style='text-align: center;width: 60%;'><input type='button' style='background-color:RED;color:white' onclick='clear_value()' value='清除顧客資訊'></td></tr>
                                <tr><td style='text-align:right'>租車人姓名：</td><td><input type='text' class='Customer_Info' name='Customer_Name' id='Customer_Name'  value='' required></td></tr>
                                <tr><td style='text-align:right'>租車人電話：</td><td><input type='text' class='Customer_Info' name='Customer_Phone' id='Customer_Phone' onkeyup='//Start_Datefo(this)' value='' required></td></tr>
                                <tr><td style='text-align:right'>租車人信箱：</td><td><input type='text' class='Customer_Info' name='Customer_Email' id='Customer_Email' onkeyup='ValidateBothEmail();//Start_Datefo(this);' value='' required></td></tr>
                                <tr><td style='text-align:right'>租車人住址：</td><td><input type='text' class='Customer_Info' name='Customer_Address' id='Customer_Address' value='' required></td></tr>
                                <tr><td style='text-align:right'>租車人生日：</td><td><input type='text' class='Customer_Info' name='Customer_BDay' id='Customer_BDay' value='' required></td></tr>
                                <tr><td style='text-align:right'>擔保人需求：</td><td>
                                    <label><input type='radio' class='Customer_Info' name='Guarantor_Requirment' id='Guarantor_Requirment_false' value='false' onchange='activate_Guarantor(this.value)' required>否</label>
                                    &emsp;&emsp;
                                    <label><input type='radio' class='Customer_Info' name='Guarantor_Requirment' id='Guarantor_Requirment_true' value='true' onchange='activate_Guarantor(this.value)' checked required>是</label>
                                </td></tr>
                                <tr><td style='text-align:right'>租車人性別：</td><td>
                                    <select name='Customer_Sex' style='width:100px;' class='Customer_Info modified_select' id='Customer_Sex' required>
                                        <option value='0'>女</option>
                                        <option value='1'>男</option>
                                        <option value='2'>不明</option>
                                        <option value='9'>戰鬥直升機</option>
                                    </select>
                                </td></tr>
                                <tr><td  style='text-align:right'>身分證/護照/居留證號碼：</td><td><input type='text' class='Customer_Info' name='Customer_SSID' id='Customer_SSID' onkeyup='check_customer(this.value);' value='' required></td></tr>
                                <tr><td  style='text-align:right'>緊急連絡人：</td><td><input type='text' class='Customer_Info' name='Emergency_Name' id='Emergency_Name' value='' required></td></tr>
                                <tr><td  style='text-align:right'>聯絡人電話：</td><td><input type='text' class='Customer_Info' name='Emergency_Phone' id='Emergency_Phone' value='' required></td></tr>
                                <tr><td  style='text-align:right'></td><td></td></tr>
                                <!-- <tr><td  style='text-align:right'>車輛安排：</td><td>
                                    <label><input type='radio' name='Arrang_Type' id='Arrang_Type' value='manual' required>手動安排</label>
                                    &emsp;&emsp;
                                    <label><input type='radio' name='Arrang_Type' id='Arrang_Type' value='auto' checked required>自動安排</label>
                                </td></tr> -->
                            </table>

                            <table id='Guarantor' class='Input_Info' border='0' cellspacing='0'>
                                <tr><td style='text-align:right;width: 35%;'>擔保人資訊：</td><td style='text-align: center;width: 60%;'><input type='button' style='background-color:RED;color:white' onclick='clear_Guarantor_value()' value='清除擔保人資訊'></td></tr>
                                <tr><td style='text-align:right'>擔保人姓名：</td><td><input type='text' class='Guarantor_Info' name='Guarantor_Name' id='Guarantor_Name'  value='' required></td></tr>
                                <tr><td style='text-align:right'>擔保人電話：</td><td><input type='text' class='Guarantor_Info' name='Guarantor_Phone' id='Guarantor_Phone' onkeyup='//Start_Datefo(this)' value='' required></td></tr>
                                <tr><td style='text-align:right'>擔保人信箱：</td><td><input type='text' class='Guarantor_Info' name='Guarantor_Email' id='Guarantor_Email' onkeyup='/*ValidateEmail(this.value);*/ValidateBothEmail();//Start_Datefo(this);' value='' required></td></tr>
                                <tr><td style='text-align:right'>擔保人住址：</td><td><input type='text' class='Guarantor_Info' name='Guarantor_Address' id='Guarantor_Address' value='' required></td></tr>
                                <tr><td style='text-align:right'>擔保人生日：</td><td><input type='text' class='Guarantor_Info' name='Guarantor_BDay' id='Guarantor_BDay' value='' required></td></tr>
                                <tr><td style='text-align:right'>擔保人性別：</td><td>
                                    <select name='Guarantor_Sex' style='width:100px;' class='Guarantor_Info modified_select' id='Guarantor_Sex' required>
                                        <option value='0'>女</option>
                                        <option value='1'>男</option>
                                        <option value='2'>不明</option>
                                        <option value='9'>戰鬥直升機</option>
                                    </select>
                                </td></tr>
                                <tr><td  style='text-align:right'>身分證/護照/居留證號碼：</td><td><input type='text' class='Guarantor_Info' name='Guarantor_SSID' id='Guarantor_SSID' onkeyup='check_guarantor(this.value);' value='' required></td></tr>
                            </table>
                            <center>
                                <button type='submit' class='submit_button'>新增訂單</button>
                            </center>
                        </div>
                    </form>
                </div>
            </div></center>
        </div>    
        <!-- 此程式由嵐叔獨力完成，若有相關需求，歡迎聯絡Facebook「嵐叔＆貓貓的日常」粉絲專頁 -->
    </body>
</html>
<script src="https://ajax.googleapis.com/ajax/libs/jquery/3.4.1/jquery.min.js"></script>
<script>
    var car_list = null;

    var picker = new Lightpick({
        field: document.getElementById('Start_Date'),
        secondField: document.getElementById('End_Date'),
        singleDate: false,
        minDate: moment().startOf('day'),
        repick: true,
        onSelect: function(start, end){
            var str = '';
            var count = 0;
            str += start ? start.format('YYYY/MM/DD') + ' 到 ' : '';
            str += end ? end.format('YYYY/MM/DD') : '...';
            if((end - start)>=0)
                count = Math.floor((end - start)/86400000);
            document.getElementById('Day_Count').value = count;
            Get_Car_List();
            calculate_all_info();
            //Day count is disabled at line 719
        }
    });
    
    var BD_picker = new Lightpick({
        field: document.getElementById('Customer_BDay'),
        singleDate: true,
        repick: true,
        onSelect: function(start, end){}
    });
    
    $(document).ready(function() {
        $("#reservation").submit(function(){
            if(formcheck()){
                event.preventDefault();
                $.ajax({
                    type: "POST",
                    url: "service.php",
                    data: $("#reservation").serialize(),
                    success: function(data) {
                        alert("訂單新增成功！");
                        location.href = '../Orders/';
                    },
                    error: function(jqXHR) {
                        alert("發生錯誤！請盡速連絡最帥氣最英俊最氣質最優雅最時尚最戰術最聰明最努力最認真的阿嵐");
                        console.log("error: " + jqXHR.responseText);
                    }
                })
            }
        });

    });

    ///////////////////////////////////////////////////////////////////////////////////////

    function GetRentalPlaces(){
        $.ajax({
            type: "POST",
            url: "service.php",
            dataType: "json",
            data: {
                Feature: 'Get_Rental_Places'
            },
            success: function(data) {
                // console.log(data);return;
                $("#Rent_Place").empty();
                $("#Rent_Place").append("<option value=''>請選擇租車地點</option>");
                for(i=0;i<data.length;i++){
                    $("#Rent_Place").append("<option value='"+data[i].BA_ID+"'>"+data[i].BA_Name+"</option>");
                }
            },
            error: function(jqXHR) {
                console.log(jqXHR.responseText);
            }
        })
    }

    function Get_Car_List(){
        if($("#Start_Date").val() != "" && $("#End_Date").val() != "" && $("#Rent_Place").val() != ""){
            $.ajax({
                type: "POST",
                url: "service.php",
                dataType: "json",
                data: {
                    Feature: 'Get_Available_Viechle',
                    Start_Date: $("#Start_Date").val() + " " + $("#Start_Date_Time").val() + ":00",
                    End_Date: $("#End_Date").val() + " " + $("#Start_Date_Time").val() + ":00",
                    Rent_Place: $("#Rent_Place").val()
                },
                success: function(data) {
                    car_list = data;
                    console.log(car_list);
                    let display_html = "";
                    for(let capacity in car_list){
                        display_html += "<div><h5>" + capacity + "人座</h5><div>";
                        for(let car_type in car_list[capacity]){
                            display_html += "<button type='button' class='option_btn' value='"+car_list[capacity][car_type].Car_Type+"' onclick='option_select(this)'>" + car_list[capacity][car_type].Car_Type + "</button>"
                        }
                        display_html += "</div></div>";
                    }
                    $("#selection_area").html(display_html);
                },
                error: function(jqXHR) {
                    console.log(jqXHR.responseText);
                }
            })
        }
    }

    function Start_Datefo(target){
        if(target.value != ""){
            switch(target.id){
                case 'Customer_Phone':
                    document.getElementById("Customer_Email").required  = false;
                break;

                case 'Customer_Email':
                    document.getElementById("Customer_Phone").required  = false;
                break;
            }
        }
        else{
            switch(target.id){
                case 'Customer_Phone':
                    document.getElementById("Customer_Email").required  = true;
                break;

                case 'Customer_Email':
                    document.getElementById("Customer_Phone").required  = true;
                break;
            }
        }
    }

    function clear_value(){
        $(".Customer_Info").val("");
        $("#Customer_Sex").val("0");
        ValidateBothEmail();
    }

    function clear_Guarantor_value(){
        $(".Guarantor_Info").val("");
        $("#Guarantor_Sex").val("0");
        ValidateBothEmail();
    }

    function ValidateBothEmail(){
        if($("input[name='Guarantor_Requirment']:checked").val()=="true"){
            if(!ValidateEmail($("#Guarantor_Email").val()) || !ValidateEmail($("#Customer_Email").val())){
                for(i=0;i<document.getElementsByName("submit_button").length;i++)
                    document.getElementsByName("submit_button")[i].disabled = true;
            }
            else{
                for(i=0;i<document.getElementsByName("submit_button").length;i++)
                    document.getElementsByName("submit_button")[i].disabled = false;
            }
        }
        else{
            if(!ValidateEmail($("#Customer_Email").val())){
                for(i=0;i<document.getElementsByName("submit_button").length;i++)
                    document.getElementsByName("submit_button")[i].disabled = true;
            }
            else{
                for(i=0;i<document.getElementsByName("submit_button").length;i++)
                    document.getElementsByName("submit_button")[i].disabled = false;
            }
        }

    }

    function ValidateEmail(mail) {
        var mailformat = /^\w+([\.-]?\w+)*@\w+([\.-]?\w+)*(\.\w{2,3})+$/;
        if (mail.match(mailformat)){
            // alert("此為合法的信箱");
            return (true);
        }
        else{
            return (false);
        }
    }

    function get_project(){
        $.ajax({
            type: "POST",
            url: "sql_search.php",
            dataType: "json",
            data: {
                Category: 'Get_Project',
                Rooms: room_array,
                Start: $("#Start_Date").val(),
                End: $("#End_Date").val(),
            },
            success: function(data) {
                $("#Discount_ID").empty();
                for(i=0;i<data.length;i++){
                    $("#Discount_ID").append("<option value='"+data[i].ID+"'>"+data[i].Name+"</option>");
                }
            },
            error: function(jqXHR) {
                console.log(jqXHR.responseText);
            }
        })
    }
    
    function option_select(selected_option){
        $(".option_btn").removeClass('selected_btn');
        selected_option.classList.add("selected_btn");
        $("#selected_option").val(selected_option.value);
        calculate_all_info();
    }

    function check_customer(Customer_SSID){
        $.ajax({
            type: "POST",
            url: "service.php",
            dataType: "json",
            data: {
                Feature: 'Check_Customer',
                Customer_SSID: Customer_SSID
            },
            success: function(data) {
                if(data!=null){
                    if(data.Customer_Status == 0){
                        $("#Customer_ID").val(data.Customer_ID);
                        $("#Customer_Name").val(data.Customer_Name);
                        $("#Customer_Phone").val(data.Customer_Phone);
                        $("#Customer_Email").val(data.Customer_Email);
                        $("#Customer_Address").val(data.Customer_Address);
                        $("#Customer_BDay").val(data.Customer_BDay.replaceAll("-", "/"));
                        $("#Customer_Sex").val(data.Customer_Sex);
                        ValidateBothEmail();
                    }
                    else{
                        alert("此人在顧客黑名單中！");
                        clear_value();
                    }
                }
                else{
                    $("#Customer_ID").val("");
                }
            },
            error: function(jqXHR) {
                console.log("Error: " + jqXHR.responseText);
            }
        })
    }

    function check_guarantor(Guarantor_SSID){
        $.ajax({
            type: "POST",
            url: "service.php",
            dataType: "json",
            data: {
                Feature: 'Check_Customer',
                Customer_SSID: Guarantor_SSID
            },
            success: function(data) {
                if(data!=null){
                    if(data.Customer_Status == 0){
                        $("#Guarantor_ID").val(data.Customer_ID);
                        $("#Guarantor_Name").val(data.Customer_Name);
                        $("#Guarantor_Phone").val(data.Customer_Phone);
                        $("#Guarantor_Email").val(data.Customer_Email);
                        $("#Guarantor_Address").val(data.Customer_Address);
                        $("#Guarantor_BDay").val(data.Customer_BDay.replaceAll("-", "/"));
                        $("#Guarantor_Sex").val(data.Customer_Sex);
                        ValidateBothEmail();
                    }
                    else{
                        alert("此人在顧客黑名單中！");
                        clear_value();
                    }
                }
            },
            error: function(jqXHR) {
                console.log("Error: " + jqXHR.responseText);
            }
        })
    }

    function change_input_info(self, target_id){
        $(".section_option").removeClass('active');
        self.classList.add("active");

        $(".Input_Info").removeClass('II_Active');
        self.classList.add("II_Active");
        $("#"+target_id).addClass('II_Active');
    }

    function activate_Guarantor(active){
        if(active == "true"){
            $(".Guarantor_Info").prop('required',true);
            $("#Guarantor_Btn").prop('disabled',false);
        }
        else{
            $(".Guarantor_Info").prop('required',false);
            $("#Guarantor_Btn").prop('disabled',true);
        }
        ValidateBothEmail();
    }

    function formcheck() {
        let flag = true;
        $('#reservation').find("select, textarea, input").each(function(){
            if($(this).prop('required') && $(this).val()==''){
                switch(this.id){
                    case "Start_Date":
                        alert("請選擇起始時間");
                    break;

                    case "End_Date":
                        alert("請選擇結束時間");
                    break;

                    case "selected_option":
                        alert("請選擇車型");
                    break;

                    case "Customer_Name":
                        alert("請輸入租車人姓名");
                    break;

                    case "Customer_Phone":
                        alert("請輸入租車人電話");
                    break;

                    case "Customer_Email":
                        alert("請輸入租車人信箱");
                    break;

                    case "Customer_Address":
                        alert("請輸入租車人住址");
                    break;

                    case "Customer_BDay":
                        alert("請輸入租車人生日");
                    break;

                    case "Customer_SSID":
                        alert("請輸入租車人身分證號");
                    break;

                    case "Guarantor_Name":
                        alert("請輸入擔保人姓名");
                    break;

                    case "Guarantor_Phone":
                        alert("請輸入擔保人電話");
                    break;

                    case "Guarantor_Email":
                        alert("請輸入擔保人信箱");
                    break;

                    case "Guarantor_Address":
                        alert("請輸入擔保人住址");
                    break;

                    case "Guarantor_BDay":
                        alert("請輸入擔保人生日");
                    break;

                    case "Guarantor_SSID":
                        alert("請輸入擔保人身分證號");
                    break;
                }
                flag = !flag;
                return false;
            }
        });
        return flag;
    }

    function calculate_all_info(){
        if(car_list==null) return;
        let Start = moment($("#Start_Date").val() + " " + $("#Start_Date_Time").val() + ":00");
        let End = moment($("#End_Date").val() + " " + $("#End_Date_Time").val() + ":00");
        let car_info = $("#selected_option").val();
        let Origin_Start = moment($("#Start_Date").val());
        for(let capacity in car_list){
            for(let i = 0; i<car_list[capacity].length; i++){
                if(car_list[capacity][i]['Car_Type']==car_info){
                    car_info = car_list[capacity][i];
                    car_info.Special_Half = car_info.Weekday_Price/2;
                    car_info.Extra_Hour = Math.ceil(car_info.Weekday_Price/10);
                    car_info.Weekday_Price = parseInt(car_info.Weekday_Price);
                    car_info.Weekend_Price = parseInt(car_info.Weekend_Price);
                    car_info.Halfday_Price = parseInt(car_info.Halfday_Price);
                    car_info.Weekly = parseInt(car_info.Weekly);
                    car_info.Monthly = parseInt(car_info.Monthly);
                    car_info.Diff = parseInt(car_info.Weekend_Price - car_info.Weekday_Price);
                    break;
                }
            }
        }

        if(car_info==""){
            alert("請先選擇車型");
            return;
        }
        if(Start>=End){
            alert("請調整日期或時間");
            return;
        }
        
        let total_days = Math.abs(End.diff(Start,'hours')/24);
        let time_diff = End.diff(Start,'hours')%24;
        let weekday = 0;
        let weekend = 0;
        let total_price = 0;
        let special_offer = 0;
        let discount = 0;
        let extra_hour_fee = 0;
        let Special_Remark = "";

        if(Math.floor(total_days)==0){
            if(time_diff<=12 && Start.format('d')>0 && Start.format('d')<6){
                total_price += parseInt(car_info.Halfday_Price);
                total_days = 0;
            }
            else
                if(Start.format('d')>0 && Start.format('d')<6){
                    total_days = 1;
                    total_price += parseInt(car_info.Weekday_Price);
                }
                else{
                    total_days = 1;
                    total_price += parseInt(car_info.Weekend_Price);
                }
        }
        else if(total_days<6){
            total_price += Math.floor(total_days) * car_info.Weekday_Price;
            if(time_diff<=5){
                total_price += Math.ceil(time_diff) * car_info.Extra_Hour;
                total_days = Math.floor(total_days);
            }
            else if(time_diff<=12){
                total_price += car_info.Special_Half;
                total_days = Math.floor(total_days) + 0.5;
            }
            else{
                total_price = Math.ceil(total_days) * car_info.Weekday_Price;
                total_days = Math.floor(total_days) + 1;
            }

            special_offer = total_days;
            for(i=0;i<Math.floor(total_days);i++){
                switch(parseInt(Start.format('d'))){
                    case 1:
                    case 2:
                    case 3:
                    case 4:
                        weekday++;
                    break;

                    case 5:
                        if((parseInt($("#Start_Date_Time").val().split(":")[0]*60) + parseInt($("#Start_Date_Time").val().split(":")[1]))>720){
                            //consider as weekend
                            total_price += car_info.Diff;
                            weekend++;
                        }
                        else{
                            weekday++;
                        }
                    break;

                    case 6:
                        total_price += car_info.Diff;
                        weekend++;
                    break;

                    case 0:
                        if((parseInt($("#Start_Date_Time").val().split(":")[0]*60) + parseInt($("#Start_Date_Time").val().split(":")[1]))>720){
                            //consider as weekday
                            weekday++;
                        }
                        else{
                            total_price += car_info.Diff;
                            weekend++;
                        }
                    break;
                }
                Start.add("1","days");
            }
            if(total_days%1>0){
                switch(parseInt(End.format('d'))){
                    case 6:
                    case 0:
                        weekday--;
                        weekend++;
                        total_price += car_info.Diff;
                    break;
                }
            }
        }
        else if(total_days<30){
            if(total_days%1!=0){
                Special_Remark = "，自動進位一天，共" + Math.ceil(total_days) + "天";
            }
            total_price += Math.ceil(total_days)*parseInt(car_info.Weekly);
        }
        else{
            total_price += Math.ceil(total_days)*parseInt(car_info.Monthly);
            if(total_days%1!=0){
                Special_Remark = "，自動進位一天，共" + Math.ceil(total_days) + "天";
            }
        }
        if(special_offer%1>0){
            special_offer=Math.floor(total_days) - 1;
        }
        switch(special_offer){
            case 1:
                discount = 100;
            break;
            
            case 2:
                discount = 300;
            break;
            
            case 3:
                discount = 600;
            break;
            
            case 4:
                discount = 900;
            break;
            
            case 5:
                discount = 1200;
            break;

            default:
                special_offer = 0;
                discount = 0;
        }
        $('#Total_Price').val("$" + (total_price - discount));

    }

</script>