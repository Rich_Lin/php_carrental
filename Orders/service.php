<?php
    include_once "../mysql_connect.inc.php";
    header('Content-Type: application/json; charset=UTF-8');
    error_reporting(0);
    //年月日0001
    /////////////////////////////////////////////////////
    
    if($_SERVER['REQUEST_METHOD'] == "POST") {
      if(isset($_POST)){
        switch($_POST['Feature']){
          case "show_all":
            $Update_Info = $_POST;
            $sql = "SELECT `order_list`.`Order_ID`, `order_list`.`Created_DT`, `order_list`.`Rental_Area`, `order_list`.`Return_Area`, `order_list`.`Estimated_Rent_DateTime`, `order_list`.`Estimated_Return_DateTime`, `order_list`.`License_Plate`, `order_list`.`Car_Type`, `customer_list`.`Customer_Name`,`customer_list`.`Customer_Phone`, `customer_list`.`Customer_Email`, `employee_list`.`Employee_Name`, `order_list`.`Status` FROM `order_list`,`customer_list`,`employee_list`";
            $WHERE = " WHERE (`order_list`.`Customer_ID`=`customer_list`.`Customer_ID` AND `order_list`.`Employee_ID`=`employee_list`.`Employee_ID`)";
            $Fuzzy_Search = "";
            $Sorting = " ORDER BY `order_list`.`Estimated_Return_DateTime`";
            $Date = "";
            if(!empty($Update_Info["Fuzzy_Search"])){
              $Update_Info["Fuzzy_Search"] = addslashes($Update_Info["Fuzzy_Search"]);
              $Fuzzy_Search = " (`order_list`.`Order_ID` LIKE '%".$Update_Info["Fuzzy_Search"]."%' OR `order_list`.`Emergency_Name` LIKE '%".$Update_Info["Fuzzy_Search"]."%' OR `order_list`.`Emergency_Phone` LIKE '%".$Update_Info["Fuzzy_Search"]."%' OR `order_list`.`License_Plate` LIKE '%".$Update_Info["Fuzzy_Search"]."%' OR `order_list`.`Rental_Area` LIKE '%".$Update_Info["Fuzzy_Search"]."%' OR `order_list`.`Return_Area` LIKE '%".$Update_Info["Fuzzy_Search"]."%' OR `order_list`.`Total_Price` LIKE '%".$Update_Info["Fuzzy_Search"]."%' OR `customer_list`.`Customer_Name` LIKE '%".$Update_Info["Fuzzy_Search"]."%' OR `customer_list`.`Customer_SSID` LIKE '%".$Update_Info["Fuzzy_Search"]."%' OR `customer_list`.`Customer_Phone` LIKE '%".$Update_Info["Fuzzy_Search"]."%' OR `customer_list`.`Customer_Email` LIKE '%".$Update_Info["Fuzzy_Search"]."%' OR `customer_list`.`Customer_Address` LIKE '%".$Update_Info["Fuzzy_Search"]."%' OR `order_list`.`Car_Type` LIKE '%".$Update_Info["Fuzzy_Search"]."%') ";
            }
            if(isset($Update_Info["Sorting"])){
              switch($Update_Info["Sorting"]){
                case "RENT":
                  if(isset($Update_Info['Start']) || isset($Update_Info['End'])){
                    if(!empty($Update_Info['Start']) || !empty($Update_Info['End'])){
                      if(empty($Update_Info['End']))
                        $Date = " (`order_list`.`Estimated_Rent_DateTime` >= '" . $Update_Info['Start'] . "')";
                      else if(empty($Update_Info['Start']))
                          $Date = " (`order_list`.`Estimated_Rent_DateTime` <= '" . $Update_Info['End'] . "')";
                      else{
                        if($Update_Info['Start'] > $Update_Info['End'])
                          $Date = " ((`order_list`.`Estimated_Rent_DateTime` >= '" . $Update_Info['End'] . "') AND (`order_list`.`Estimated_Rent_DateTime` <= '" . $Update_Info['Start'] . "'))";
                        else
                          $Date = " ((`order_list`.`Estimated_Rent_DateTime` >= '" . $Update_Info['Start'] . "') AND (`order_list`.`Estimated_Rent_DateTime` <= '" . $Update_Info['End'] . "'))";
                      }
                    }
                  }
                  $Sorting = " ORDER BY `order_list`.`Estimated_Rent_DateTime`";
                break;

                case "RETURN":
                    if(isset($Update_Info['Start']) || isset($Update_Info['End'])){
                        if(!empty($Update_Info['Start']) || !empty($Update_Info['End'])){
                            if(empty($Update_Info['End']))
                                $Date = " (`order_list`.`Estimated_Return_DateTime` >= '" . $Update_Info['Start'] . "')";
                            else if(empty($Update_Info['Start']))
                                $Date = " (`order_list`.`Estimated_Return_DateTime` <= '" . $Update_Info['End'] . "')";
                            else{
                                if($Update_Info['Start'] > $Update_Info['End'])
                                    $Date = " ((`order_list`.`Estimated_Return_DateTime` >= '" . $Update_Info['End'] . "') AND (`order_list`.`Estimated_Return_DateTime` <= '" . $Update_Info['Start'] . "'))";
                                else
                                    $Date = " ((`order_list`.`Estimated_Return_DateTime` >= '" . $Update_Info['Start'] . "') AND (`order_list`.`Estimated_Return_DateTime` <= '" . $Update_Info['End'] . "'))";
                            }
                        }
                    }
                    $Sorting = " ORDER BY `order_list`.`Estimated_Return_DateTime`";
                break;

                case "BDATE":
                    if(isset($Update_Info['Start']) || isset($Update_Info['End'])){
                        if(!empty($Update_Info['Start']) || !empty($Update_Info['End'])){
                            if(empty($Update_Info['End']))
                                $Date = " (`order_list`.`Created_DT` >= '" . $Update_Info['Start'] . "')";
                            else if(empty($Update_Info['Start']))
                                $Date = " (`order_list`.`Created_DT` <= '" . $Update_Info['End'] . "')";
                            else{
                                if($Update_Info['Start'] > $Update_Info['End'])
                                    $Date = " ((`order_list`.`Created_DT` >= '" . $Update_Info['End'] . "') AND (`order_list`.`Created_DT` <= '" . $Update_Info['Start'] . "'))";
                                else
                                    $Date = " ((`order_list`.`Created_DT` >= '" . $Update_Info['Start'] . "') AND (`order_list`.`Created_DT` <= '" . $Update_Info['End'] . "'))";
                            }
                        }
                    }
                    $Sorting = " ORDER BY `order_list`.`Created_DT`";
                break;

                default:
                      if(isset($Update_Info['Start']) || isset($Update_Info['End'])){
                          $Update_Info['Start'] = addslashes($Update_Info['Start']);
                          $Update_Info['End'] = addslashes($Update_Info['End']);
                          if(!empty($Update_Info['Start']) || !empty($Update_Info['End'])){
                              if(empty($Update_Info['End']))
                                  $Date = " (`order_list`.`Estimated_Return_DateTime` >= '" . $Update_Info['Start'] . "')";
                              else if(empty($Update_Info['Start']))
                                  $Date = " (`order_list`.`Estimated_Rent_DateTime` <= '" . $Update_Info['End'] . "')";
                              else{
                                  $Date = " ((`order_list`.`Estimated_Rent_DateTime` <= '" . $Update_Info['End'] . "' AND '" . $Update_Info['End'] . "' <= `order_list`.`Estimated_Return_DateTime`) OR (`order_list`.`Estimated_Rent_DateTime` <= '" . $Update_Info['Start'] . "' AND '" . $Update_Info['Start'] . "' <= `order_list`.`Estimated_Return_DateTime`))";
                              }
                          }
                      }
                      $Sorting = " ORDER BY `order_list`.`Estimated_Rent_DateTime`";
              }
            }
            $sql .= $WHERE;
            if($Fuzzy_Search != '')
              $sql = $sql . " AND " . $Fuzzy_Search;
            if($Date != '')
              $sql .= " AND " . $Date;
            $sql = $sql . $Sorting . " DESC";
            $info_array = array();
            $result = mysqli_query($conn,$sql);
            $count = 0;
            while($row = $result->fetch_assoc()){
              $sql = "SELECT `BA_Name` FROM `branch_allocation` WHERE `BA_ID`='".$row['Rental_Area']."'";
              $row['Rental_Area'] = mysqli_query($conn,$sql)->fetch_assoc()['BA_Name'];

              $sql = "SELECT `BA_Name` FROM `branch_allocation` WHERE `BA_ID`='".$row['Return_Area']."'";
              $row['Return_Area'] = mysqli_query($conn,$sql)->fetch_assoc()['BA_Name'];
              
              $info_array[explode(" ",$row['Estimated_Return_DateTime'])[0]][] = $row;
            }
            echo json_encode($info_array,JSON_UNESCAPED_UNICODE);
          break;

          case "Get_Rental_Places":
            $places = array();
            $sql = "SELECT DISTINCT `branch_allocation`.`BA_ID`,`branch_allocation`.`BA_Name` FROM `branch_allocation`,`car_list` WHERE `car_list`.`Current_Location`=`branch_allocation`.`BA_ID`";
            $result = mysqli_query($conn,$sql);
            while($row = $result->fetch_assoc())
              $places[] = $row;
            echo json_encode($places,JSON_UNESCAPED_UNICODE);
          break;

          case "Get_Available_Viechle":
            $car_list = array();
            $sql = "SELECT * FROM `car_type` ORDER BY `Car_Capacity`";
            $result = mysqli_query($conn,$sql);
            while($row = $result->fetch_assoc())
              $car_list[$row['Car_Capacity']][] = $row;
            
            foreach($car_list as $key => $value){
              for($i=0;$i<count($value);$i++){
                $sql = "SELECT * FROM `car_list` WHERE `Car_Type` = '". $value[$i]['Car_Type']."' AND `Current_Location`='".$_POST['Rent_Place']."'";
                $result = mysqli_query($conn,$sql);
                while($row = $result->fetch_assoc())
                  $car_list[$key][$i]['A_List'][] = $row['License_Plate'];
              }
            }

            // $sql = "SELECT `License_Plate` FROM `order_list` WHERE `Status` != 2";
            $sql = "SELECT DISTINCT `License_Plate` FROM `order_list` WHERE (('".$_POST['Start_Date']."' BETWEEN `Estimated_Rent_DateTime` AND `Estimated_Return_DateTime`) OR ('".$_POST['End_Date']."' BETWEEN `Estimated_Rent_DateTime` AND `Estimated_Return_DateTime`) OR (`Estimated_Rent_DateTime` BETWEEN '".$_POST['Start_Date']."' AND '".$_POST['End_Date']."') OR (`Estimated_Return_DateTime` BETWEEN '".$_POST['Start_Date']."' AND '".$_POST['End_Date']."')) OR (('".$_POST['Start_Date']."' BETWEEN `Actual_Rent_DateTime` AND `Actual_Return_DateTime`) OR ('".$_POST['End_Date']."' BETWEEN `Actual_Rent_DateTime` AND `Actual_Return_DateTime`) OR (`Actual_Rent_DateTime` BETWEEN '".$_POST['Start_Date']."' AND '".$_POST['End_Date']."') OR (`Actual_Return_DateTime` BETWEEN '".$_POST['Start_Date']."' AND '".$_POST['End_Date']."'))";
            $result = mysqli_query($conn,$sql);
            while($row = $result->fetch_assoc()){
              foreach($car_list as $key => $value){
                for($i=0;$i<count($value);$i++){
                  if(in_array($row['License_Plate'], $car_list[$key][$i]['A_List'])){
                    unset($car_list[$key][$i]['A_List'][array_search($row['License_Plate'],$car_list[$key][$i]['A_List'])]);
                    sort($car_list[$key][$i]['A_List']);
                  }
                }
              }
            }

            // $sql = "SELECT `License_Plate` FROM `scheduling_list` WHERE `Status` != 2";
            $sql = "SELECT DISTINCT `License_Plate` FROM `scheduling_list` WHERE (('".$_POST['Start_Date']."' BETWEEN `Request_Create_DT` AND `Response_DT`) OR ('".$_POST['End_Date']."' BETWEEN `Request_Create_DT` AND `Response_DT`) OR (`Request_Create_DT` BETWEEN '".$_POST['Start_Date']."' AND '".$_POST['End_Date']."') OR (`Response_DT` BETWEEN '".$_POST['Start_Date']."' AND '".$_POST['End_Date']."'))";// OR (`Actual_Arrival_DT`)
            // echo $sql;die;
            $result = mysqli_query($conn,$sql);
            while($row = $result->fetch_assoc()){
              foreach($car_list as $key => $value){
                for($i=0;$i<count($value);$i++){
                  if(in_array($row['License_Plate'], $car_list[$key][$i]['A_List'])){
                    unset($car_list[$key][$i]['A_List'][array_search($row['License_Plate'],$car_list[$key][$i]['A_List'])]);
                    sort($car_list[$key][$i]['A_List']);
                  }
                }
              }
            }
            foreach($car_list as $key => $value){
              $value_count = count($value);
              for($i=0;$i<$value_count;$i++){
                if(count($car_list[$key][$i]['A_List'])==0){
                  unset($car_list[$key][$i]);
                  sort($car_list[$key]);
                  $value_count--;
                }
              }
              if(count($car_list[$key])==0){
                unset($car_list[$key]);
              }
            }
            echo json_encode($car_list,JSON_UNESCAPED_UNICODE);
          break;

          case "Check_Customer":
            $sql = "SELECT * FROM `customer_list` WHERE `Customer_SSID`='".$_POST['Customer_SSID']."'";
            $result = mysqli_query($conn,$sql);
            echo json_encode(mysqli_query($conn,$sql)->fetch_assoc(),JSON_UNESCAPED_UNICODE);
          break;

          case "Create_Order":
            $log = array();
            $log[] = $_POST;
            date_default_timezone_set('Asia/Taipei');
            $Current_Datetime = date('Y-m-d H:i:s', time());

            $sql = "SELECT COUNT(*) FROM `order_list` WHERE DATE(`Created_DT`)='".explode(" ",$Current_Datetime)[0]."'";
            $Order_ID = str_replace("-","",explode(" ",$Current_Datetime)[0]) . str_pad(mysqli_query($conn,$sql)->fetch_assoc()['COUNT(*)']+1, 4, "0", STR_PAD_LEFT);

            if(empty($_POST['Employee_ID']) || is_null($_POST['Employee_ID']) || !isset($_POST['Employee_ID'])){
              die;
            }

            $Employee_ID = $_POST['Employee_ID'];
            $Customer_ID = $_POST['Customer_ID'];
            $Guarantor_ID = $_POST['Guarantor_ID'];
            $Estimated_Rent_DateTime = str_replace("/","-",$_POST['Start_Date']) . " " . $_POST['Start_Date_Time'] . ":00";
            $Estimated_Return_DateTime = str_replace("/","-",$_POST['End_Date']) . " " . $_POST['End_Date_Time'] . ":00";
            if($_POST['Customer_ID']==""){
              $sql = "INSERT INTO `customer_list`(`Customer_Name`, `Customer_SSID`, `Customer_Sex`, `Customer_BDay`, `Customer_Phone`, `Customer_Email`, `Customer_Address`) VALUES ('".$_POST['Customer_Name']."','".$_POST['Customer_SSID']."','".$_POST['Customer_Sex']."','".$_POST['Customer_BDay']."','".$_POST['Customer_Phone']."','".$_POST['Customer_Email']."','".$_POST['Customer_Address']."')";
              if(!mysqli_query($conn,$sql)){
                echo "SQL Error: CO_CC";
                die;
              }
              $sql = "SELECT `Customer_ID` FROM `customer_list` WHERE `Customer_SSID`='".$_POST['Customer_SSID']."'";
              $Customer_ID = mysqli_query($conn,$sql)->fetch_assoc()['Customer_ID'];
            }

            if($_POST['Guarantor_ID']=="" && $_POST['Guarantor_Requirment']=="true"){
              $sql = "INSERT INTO `customer_list`(`Customer_Name`, `Customer_SSID`, `Customer_Sex`, `Customer_BDay`, `Customer_Phone`, `Customer_Email`, `Customer_Address`) VALUES ('".$_POST['Guarantor_Name']."','".$_POST['Guarantor_SSID']."','".$_POST['Guarantor_Sex']."','".$_POST['Guarantor_BDay']."','".$_POST['Guarantor_Phone']."','".$_POST['Guarantor_Email']."','".$_POST['Guarantor_Address']."')";
              if(!mysqli_query($conn,$sql)){
                echo "SQL Error: CO_CG";
                die;
              }
              $sql = "SELECT `Customer_ID` FROM `customer_list` WHERE `Customer_SSID`='".$_POST['Guarantor_SSID']."'";
              $Guarantor_ID = mysqli_query($conn,$sql)->fetch_assoc()['Customer_ID'];
            }
            
            // $sql = "";
            $License_Plate = array();
            $sql = "SELECT `License_Plate` FROM `car_list` WHERE `Car_Type`='".$_POST['selected_option']."' AND `Current_Location`='".$_POST['Rent_Place']."'";
            $result = mysqli_query($conn,$sql);
            while($row = $result->fetch_assoc()){
              $License_Plate[] = $row['License_Plate'];
            }
            for($i=0;$i<sizeof($License_Plate);$i++){
              $sql = "SELECT `License_Plate` FROM `order_list` WHERE `License_Plate`='".$License_Plate[$i]."' AND ((('".$Estimated_Rent_DateTime."' BETWEEN `Estimated_Rent_DateTime` AND `Estimated_Return_DateTime`) OR ('".$Estimated_Return_DateTime."' BETWEEN `Estimated_Rent_DateTime` AND `Estimated_Return_DateTime`) OR (`Estimated_Rent_DateTime` BETWEEN '".$Estimated_Rent_DateTime."' AND '".$Estimated_Return_DateTime."') OR (`Estimated_Return_DateTime` BETWEEN '".$Estimated_Rent_DateTime."' AND '".$Estimated_Return_DateTime."')) AND `Actual_Return_DateTime`='0000-00-00 00:00:00')";
              // echo $sql;die;
              if(mysqli_num_rows(mysqli_query($conn,$sql))>0){
                unset($License_Plate[$i]);
                sort($License_Plate);
              }
            }
            if(sizeof($License_Plate)==0){
              echo "沒有車子了OHO";
              die;
            }
            $sql = "INSERT INTO `order_list`(`Order_ID`, `Created_DT`, `Customer_ID`, `Guarantor_ID`, `Employee_ID`, `Emergency_Name`, `Emergency_Phone`, `Car_Type`, `License_Plate`, `Rental_Area`, `Return_Area`, `Estimated_Rent_DateTime`, `Estimated_Return_DateTime`, `Day_Count`, `Total_Price`, `Status`, `Remark`, `Log`) VALUES ('".$Order_ID."','".$Current_Datetime."','".$Customer_ID."','".$Guarantor_ID."','".$Employee_ID."','".$_POST['Emergency_Name']."','".$_POST['Emergency_Phone']."','".$_POST['selected_option']."','".$License_Plate[0]."','".$_POST['Rent_Place']."','".$_POST['Rent_Place']."','".$Estimated_Rent_DateTime."','".$Estimated_Return_DateTime."','".$_POST['Day_Count']."','".$_POST['Total_Price']."','0','".$_POST['Remark']."','".json_encode($log,JSON_UNESCAPED_UNICODE)."')";
            /*switch($_POST['Arrang_Type']){
              case "auto":
                $License_Plate = array();
                $sql = "SELECT `License_Plate` FROM `car_list` WHERE `Car_Type`='".$_POST['selected_option']."' AND `Current_Location`='".$_POST['Rent_Place']."'";
                $result = mysqli_query($conn,$sql);
                while($row = $result->fetch_assoc()){
                  $License_Plate[] = $row['License_Plate'];
                }
                for($i=0;$i<sizeof($License_Plate);$i++){
                  $sql = "SELECT `License_Plate` FROM `order_list` WHERE `License_Plate`='".$License_Plate[$i]."' AND `Status`<6";
                  if(mysqli_num_rows(mysqli_query($conn,$sql))>0){
                    unset($License_Plate[$i]);
                    sort($License_Plate);
                  }
                }
                if(sizeof($License_Plate)==0){
                  echo "沒有車子了OHO";
                  die;
                }
                $sql = "INSERT INTO `order_list`(`Order_ID`, `Created_DT`, `Customer_ID`, `Guarantor_ID`, `Employee_ID`, `Emergency_Name`, `Emergency_Phone`, `Car_Type`, `License_Plate`, `Rental_Area`, `Return_Area`, `Estimated_Rent_DateTime`, `Estimated_Return_DateTime`, `Day_Count`, `Total_Price`, `Status`, `Remark`, `Log`) VALUES ('".$Order_ID."','".$Current_Datetime."','".$Customer_ID."','".$Guarantor_ID."','".$Employee_ID."','".$_POST['Emergency_Name']."','".$_POST['Emergency_Phone']."','".$_POST['selected_option']."','".$License_Plate[0]."','".$_POST['Rent_Place']."','".$_POST['Rent_Place']."','".$Estimated_Rent_DateTime."','".$Estimated_Return_DateTime."','".$_POST['Day_Count']."','".$_POST['Total_Price']."','0','".$_POST['Remark']."','".json_encode($log,JSON_UNESCAPED_UNICODE)."')";
              break;

              case "manual":
                $sql = "INSERT INTO `order_list`(`Order_ID`, `Created_DT`, `Customer_ID`, `Guarantor_ID`, `Employee_ID`, `Emergency_Name`, `Emergency_Phone`, `Car_Type`, `Rental_Area`, `Return_Area`, `Estimated_Rent_DateTime`, `Estimated_Return_DateTime`, `Day_Count`, `Total_Price`, `Status`, `Remark`, `Log`) VALUES ('".$Order_ID."','".$Current_Datetime."','".$Customer_ID."','".$Guarantor_ID."','".$Employee_ID."','".$_POST['Emergency_Name']."','".$_POST['Emergency_Phone']."','".$_POST['selected_option']."','".$_POST['Rent_Place']."','".$_POST['Rent_Place']."','".$Estimated_Rent_DateTime."','".$Estimated_Return_DateTime."','".$_POST['Day_Count']."','".$_POST['Total_Price']."','0','".$_POST['Remark']."','".json_encode($log,JSON_UNESCAPED_UNICODE)."')";
              break;
            }*/
            if(!mysqli_query($conn,$sql)){
              echo "SQL Error: CO_CO";
              die;
            }
            echo json_encode(array('Success' => true),JSON_UNESCAPED_UNICODE);
          break;

          case "show_guarantor_info":
            $sql = "SELECT * FROM `customer_list` WHERE `Customer_ID`='".$_POST['Guarantor_ID']."'";
            echo json_encode(mysqli_query($conn,$sql)->fetch_assoc(),JSON_UNESCAPED_UNICODE);
          break;

          case "change_status":
            $sql = "SELECT `Status` FROM `order_list` WHERE `Order_ID`='".$_POST['OID']."'";
            $Status = mysqli_query($conn,$sql)->fetch_assoc()['Status'];
            switch($Status){
              case "0":
              case "1":
              case "2":
                $Status+=3;
              break;

              case "3":
              case "4":
              case "5":
                $Status=6;
              break;
            }
            $sql = "UPDATE `order_list` SET `Status`=".$Status." WHERE `Order_ID`='".$_POST['OID']."'";
            // echo $sql;die;
            if(!mysqli_query($conn,$sql)){
              echo "SQL Error: OD_CS";
              die;
            }
            echo json_encode(array('Success' => true, 'Status' => $Status),JSON_UNESCAPED_UNICODE);
          break;

          case "cancel_booking":
            $sql = "UPDATE `order_list` SET `Status`=8 WHERE `Order_ID`='".$_POST['OID']."'";
            if(!mysqli_query($conn,$sql)){
              echo "SQL Error: OD_CB";
              die;
            }
            echo json_encode(array('Success' => true),JSON_UNESCAPED_UNICODE);
            // echo $sql;die;
          break;

        }
      }
    }
    else{
      echo '<meta http-equiv=REFRESH CONTENT=0;url=index.php>';
      die;
    }
?>