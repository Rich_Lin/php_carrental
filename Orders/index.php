<?php
    header('Content-Type: text/html; charset=UTF-8');
    include_once "../session_stat.php";
?>
<head>
    <script type="text/javascript" src="../js/functions.js"></script>
    <script type="text/javascript" src="https://cdnjs.cloudflare.com/ajax/libs/moment.js/2.22.2/moment.min.js"></script>
    <link rel="stylesheet" href="//code.jquery.com/ui/1.12.1/themes/base/jquery-ui.css">
    <script src="https://code.jquery.com/jquery-1.12.4.js"></script>
    <script src="https://code.jquery.com/ui/1.12.1/jquery-ui.js"></script>
    <script type="text/javascript" src="../js/lightpick.js"></script>
    <link rel="stylesheet" type="text/css" href="../js/lightpick.css">

    <!-- <script type="text/javascript" src="https://code.jquery.com/jquery-3.3.1.slim.min.js"></script> -->
    <script type="text/javascript" src="https://cdnjs.cloudflare.com/ajax/libs/twitter-bootstrap/4.3.1/js/bootstrap.bundle.min.js"></script>
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/twitter-bootstrap/4.3.1/css/bootstrap.min.css">
    <style>
        .info_td{
            padding-left: 35px;
            font-size: 22px;
        }
        .function_td{
            font-size: 22px;
            text-align: center;
        }
        .separator {
            display: flex;
            align-items: center;
            text-align: left;
            font-size: 35px;
            width: 90%;
        }
        .separator::after {
            content: '';
            flex: 1;
            border-bottom: 1px solid #000;
            margin-left: 45px;
        }
        .info_button_table{
            width: 90%;
            height: 225px;
            background-color: WHITE;
            border: 2.5px solid #979797;
            border-radius:15px;
            margin: 25px;
        }
        .info_button_table:hover{
            /* border: 1px solid #DADADA; */
            background-color: #DADADA;
        }
        .blue_dot {
            height: 35px;
            width: 35px;
            background-color: #32C5FF;
            color:WHITE;
            border-radius: 50%;
            display: inline-block;
            text-align: center;
            /* padding-bottom: 2.5px; */
            margin: 0px 20px;
        }
        .red_dot {
            height: 35px;
            width: 35px;
            background-color: #F94D4D;
            color:WHITE;
            border-radius: 50%;
            display: inline-block;
            text-align: center;
            /* padding-bottom: 5px; */
            margin: 0px 20px;
        }
        .grey_dot {
            height: 30px;
            width: 35px;
            background-color: #C9C9C9;
            color:WHITE;
            border-radius: 50%;
            display: inline-block;
            text-align: center;
            padding-bottom: 5px;
            margin: 0px 20px;
        }
        .yellow_dot {
            height: 35px;
            width: 35px;
            background-color: #F7C700;
            color:WHITE;
            border-radius: 50%;
            display: inline-block;
            text-align: center;
            /* padding-bottom: 5px; */
            margin: 0px 20px;
        }
        .purple_dot {
            height: 35px;
            width: 35px;
            background-color: #c632e6;
            color:WHITE;
            border-radius: 50%;
            display: inline-block;
            text-align: center;
            /* padding-bottom: 5px; */
            margin: 0px 20px;
        }
        .info_table{
            width: 100%;
        }
        .info_table tr,td{
            vertical-align: center;
            padding: 5px 0px;
            font-size: 22px;
        }
    </style>
</head>
<body onload="includeHTML();">
    <div class='navbar-div' include-html="../navbar.php"></div>
    <div class='for_hyper left' include-html="../hyper.php"></div>
    <div class='right'>
        <center>
            <p id='result'></p>
            <div style='width: 100%'>
                <input type='text' class='Input_Search_Field' id='Fuzzy_Search' placeholder='關鍵字搜尋'>
                <select style='width:270px;height:50px;font-size:20px' class='modified_select' id='Sorting'>
                    <option value='BDATE' selected>訂車日期</option>
                    <option value='RENT'>租車日期</option>
                    <option value='RETURN'>還車日期</option>
                </select>
                <div style='display:inline-block'>
                    <input type='text' id='Start' value='' style='width:270px;height:50px;font-size:20px;display:inline-block' placeholder='點擊以選擇起始日'>
                    <div style='display:inline-block'> ～ </div>
                    <input type='text' id='End' value='' style='width:270px;height:50px;font-size:20px;display:inline-block' placeholder='點擊以選擇結束日'>
                </div>
                <button class='function_btn' id='Reset' style='width:110px;height:50px;font-size:20px;background-color:#0091FF'>重設</button>

                <button class='function_btn' id='Today' style='width:160px;height:50px;font-size:20px;background-color:#0091FF'>今日</button>
                <button class='function_btn' id='New_Order' style='width:160px;height:50px;font-size:20px;background-color:#F79B00'>新增訂單</button>
            </div>
            <br>
            <div id='container'>
            </div>
        </center>
    </div>
    <!-- 此程式由嵐叔獨力完成，若有相關需求，歡迎聯絡Facebook「嵐叔＆貓貓的日常」粉絲專頁 -->
</body>

<script>
    var Status_Text = ['未取車(未付訂)','未取車(已付訂)','未取車(已付清)','已出車(未付訂)','已出車(已付訂)','已出車(已付清)','已還車','保留中','訂單取消'];

    var picker = new Lightpick({
        field: document.getElementById('Start'),
        // startDate: moment().startOf('day'),
        singleDate: true,
        repick: true,
        onSelect: function(date){
            var tmp;
            if(document.getElementById('End').value != '' && document.getElementById('Start').value > document.getElementById('End').value){
                tmp = document.getElementById('End').value;
                document.getElementById('End').value = document.getElementById('Start').value;
                document.getElementById('Start').value = tmp;
            }
            $.ajax({
                type: "POST",
                url: "service.php",
                dataType: "json",
                data: {
                    Feature: "show_all",
                    Fuzzy_Search: $("#Fuzzy_Search").val(),
                    Sorting: $("#Sorting").val(),
                    // Grouping: $("#Grouping").val(),
                    Start: $("#Start").val(),
                    End: $("#End").val()
                },
                success: function(data) {
                    $("#result").html('');
                    $("#container").html('');
                    var json_array = data;
                    var pre_date = '';
                    var date = '';
                    var row = '';
                    for(var date in json_array){
                        row = "<div class='separator'>"+date.replace(/-/g, "/")+"</div>";
                        $("#container").append(row);
                        for(i=0;i<json_array[date].length;i++){
                            let Order_ID = json_array[date][i].Order_ID;
                            let Customer_Name = json_array[date][i].Customer_Name;
                            let Customer_Phone = json_array[date][i].Customer_Phone;
                            let Customer_Email = json_array[date][i].Customer_Email;
                            let Estimated_Rent_DateTime = json_array[date][i].Estimated_Rent_DateTime.replace(/-/g, "/");
                            Estimated_Rent_DateTime = Estimated_Rent_DateTime.split(":")[0] + ":" + Estimated_Rent_DateTime.split(":")[1];
                            let Estimated_Return_DateTime = json_array[date][i].Estimated_Return_DateTime.replace(/-/g, "/");
                            Estimated_Return_DateTime = Estimated_Return_DateTime.split(":")[0] + ":" + Estimated_Return_DateTime.split(":")[1];
                            let Car_Info = json_array[date][i].Car_Type + " / " + json_array[date][i].License_Plate;
                            if(json_array[date][i].License_Plate=="")
                                Car_Info = json_array[date][i].Car_Type + " / 未排車";
                            let Rental_Area = json_array[date][i].Rental_Area;

                            let Status = Status_Text[parseInt(json_array[date][i].Status)];

                            // if(i==0){
                            //     switch(document.getElementById("Sorting").value){
                            //         case ":D":
                            //         break;
                            //     }
                            // }
                            // else{
                            //     switch($("#Sorting").val()){
                                    
                            //     }
                            // }
                            row = "<form action='Order_Details.php' method='POST'><button type='submit' name='Action' value='"+Order_ID+"' class='info_button_table'><center><table class='info_table'><tr><td class='info_td' width='35%'>訂單編號：<span style='color:#0091FF'>"+Order_ID+"</span></td><td class='info_td' width='40%'>客戶姓名："+Customer_Name+"</td><td class='function_td'>" + Status + "</td></tr><tr><td class='info_td'>租車日期：<table style='display:inline' border='0'><tr><td>從"+Estimated_Rent_DateTime+"</td></tr><tr><td>到"+Estimated_Return_DateTime+"</td></tr></table></td><td class='info_td'>連絡電話："+Customer_Phone+"</td><td class='function_td'>出租店："+Rental_Area+"</td></tr><tr><td class='info_td'>車型資訊："+Car_Info+"</td><td class='info_td'>E-mail："+Customer_Email+"</td><td class='function_td'><button type='submit' name='Action' value='"+Order_ID+"' class='function_btn' style='font-size:20px;width:200px;height:50px;background-color:#0091FF'>訂單資訊</button></td></tr></table></center></button></form>";
                            $("#container").append(row);
                        }
                    }
                },
                error: function(jqXHR) {
                    $("#result").html('<font color="#ff0000">RRRRRRRRRRR' + jqXHR.responseText + '</font>');
                }
            })
            // Day count is disabled at line 719
        }
    });

    var picker2 = new Lightpick({
        field: document.getElementById('End'),
        singleDate: true,
        repick: true,
        onSelect: function(date){
            var tmp;
            if(document.getElementById('Start').value != '' && document.getElementById('Start').value > document.getElementById('End').value){
                console.log("Start: " + document.getElementById('Start').value + "\nEnd: " + document.getElementById('End').value);
                tmp = document.getElementById('End').value;
                document.getElementById('End').value = document.getElementById('Start').value;
                document.getElementById('Start').value = tmp;
            }
            $.ajax({
                type: "POST",
                url: "service.php",
                dataType: "json",
                data: {
                    Feature: "show_all",
                    Fuzzy_Search: $("#Fuzzy_Search").val(),
                    Sorting: $("#Sorting").val(),
                    // Grouping: $("#Grouping").val(),
                    Start: $("#Start").val(),
                    End: $("#End").val()
                },
                success: function(data) {
                    $("#result").html('');
                    $("#container").html('');
                    var json_array = data;
                    var pre_date = '';
                    var date = '';
                    var row = '';
                    for(var date in json_array){
                        row = "<div class='separator'>"+date.replace(/-/g, "/")+"</div>";
                        $("#container").append(row);
                        for(i=0;i<json_array[date].length;i++){
                            let Order_ID = json_array[date][i].Order_ID;
                            let Customer_Name = json_array[date][i].Customer_Name;
                            let Customer_Phone = json_array[date][i].Customer_Phone;
                            let Customer_Email = json_array[date][i].Customer_Email;
                            let Estimated_Rent_DateTime = json_array[date][i].Estimated_Rent_DateTime.replace(/-/g, "/");
                            Estimated_Rent_DateTime = Estimated_Rent_DateTime.split(":")[0] + ":" + Estimated_Rent_DateTime.split(":")[1];
                            let Estimated_Return_DateTime = json_array[date][i].Estimated_Return_DateTime.replace(/-/g, "/");
                            Estimated_Return_DateTime = Estimated_Return_DateTime.split(":")[0] + ":" + Estimated_Return_DateTime.split(":")[1];
                            let Car_Info = json_array[date][i].Car_Type + " / " + json_array[date][i].License_Plate;
                            if(json_array[date][i].License_Plate=="")
                                Car_Info = json_array[date][i].Car_Type + " / 未排車";
                            let Rental_Area = json_array[date][i].Rental_Area;

                            let Status = Status_Text[parseInt(json_array[date][i].Status)];

                            // if(i==0){
                            //     switch(document.getElementById("Sorting").value){
                            //         case ":D":
                            //         break;
                            //     }
                            // }
                            // else{
                            //     switch($("#Sorting").val()){
                                    
                            //     }
                            // }
                            row = "<form action='Order_Details.php' method='POST'><button type='submit' name='Action' value='"+Order_ID+"' class='info_button_table'><center><table class='info_table'><tr><td class='info_td' width='35%'>訂單編號：<span style='color:#0091FF'>"+Order_ID+"</span></td><td class='info_td' width='40%'>客戶姓名："+Customer_Name+"</td><td class='function_td'>" + Status + "</td></tr><tr><td class='info_td'>租車日期：<table style='display:inline' border='0'><tr><td>從"+Estimated_Rent_DateTime+"</td></tr><tr><td>到"+Estimated_Return_DateTime+"</td></tr></table></td><td class='info_td'>連絡電話："+Customer_Phone+"</td><td class='function_td'>出租店："+Rental_Area+"</td></tr><tr><td class='info_td'>車型資訊："+Car_Info+"</td><td class='info_td'>E-mail："+Customer_Email+"</td><td class='function_td'><button type='submit' name='Action' value='"+Order_ID+"' class='function_btn' style='font-size:20px;width:200px;height:50px;background-color:#0091FF'>訂單資訊</button></td></tr></table></center></button></form>";
                            $("#container").append(row);
                        }
                    }
                },
                error: function(jqXHR) {
                    $("#result").html('<font color="#ff0000">RRRRRRRRRRR' + jqXHR.responseText + '</font>');
                }
            })
            // Day count is disabled at line 719
        }
    });

    $(document).ready(function() {

        $("#Sorting").change(function() {
            $.ajax({
                type: "POST",
                url: "service.php",
                dataType: "json",
                data: {
                    Feature: "show_all",
                    Fuzzy_Search: $("#Fuzzy_Search").val(),
                    Sorting: $("#Sorting").val(),
                    // Grouping: $("#Grouping").val(),
                    Start: $("#Start").val(),
                    End: $("#End").val()
                },
                success: function(data) {
                    $("#result").html('');
                    $("#container").html('');
                    var json_array = data;
                    var pre_date = '';
                    var date = '';
                    var row = '';
                    for(var date in json_array){
                        row = "<div class='separator'>"+date.replace(/-/g, "/")+"</div>";
                        $("#container").append(row);
                        for(i=0;i<json_array[date].length;i++){
                            let Order_ID = json_array[date][i].Order_ID;
                            let Customer_Name = json_array[date][i].Customer_Name;
                            let Customer_Phone = json_array[date][i].Customer_Phone;
                            let Customer_Email = json_array[date][i].Customer_Email;
                            let Estimated_Rent_DateTime = json_array[date][i].Estimated_Rent_DateTime.replace(/-/g, "/");
                            Estimated_Rent_DateTime = Estimated_Rent_DateTime.split(":")[0] + ":" + Estimated_Rent_DateTime.split(":")[1];
                            let Estimated_Return_DateTime = json_array[date][i].Estimated_Return_DateTime.replace(/-/g, "/");
                            Estimated_Return_DateTime = Estimated_Return_DateTime.split(":")[0] + ":" + Estimated_Return_DateTime.split(":")[1];
                            let Car_Info = json_array[date][i].Car_Type + " / " + json_array[date][i].License_Plate;
                            if(json_array[date][i].License_Plate=="")
                                Car_Info = json_array[date][i].Car_Type + " / 未排車";
                            let Rental_Area = json_array[date][i].Rental_Area;

                            let Status = Status_Text[parseInt(json_array[date][i].Status)];

                            // if(i==0){
                            //     switch(document.getElementById("Sorting").value){
                            //         case ":D":
                            //         break;
                            //     }
                            // }
                            // else{
                            //     switch($("#Sorting").val()){
                                    
                            //     }
                            // }
                            row = "<form action='Order_Details.php' method='POST'><button type='submit' name='Action' value='"+Order_ID+"' class='info_button_table'><center><table class='info_table'><tr><td class='info_td' width='35%'>訂單編號：<span style='color:#0091FF'>"+Order_ID+"</span></td><td class='info_td' width='40%'>客戶姓名："+Customer_Name+"</td><td class='function_td'>" + Status + "</td></tr><tr><td class='info_td'>租車日期：<table style='display:inline' border='0'><tr><td>從"+Estimated_Rent_DateTime+"</td></tr><tr><td>到"+Estimated_Return_DateTime+"</td></tr></table></td><td class='info_td'>連絡電話："+Customer_Phone+"</td><td class='function_td'>出租店："+Rental_Area+"</td></tr><tr><td class='info_td'>車型資訊："+Car_Info+"</td><td class='info_td'>E-mail："+Customer_Email+"</td><td class='function_td'><button type='submit' name='Action' value='"+Order_ID+"' class='function_btn' style='font-size:20px;width:200px;height:50px;background-color:#0091FF'>訂單資訊</button></td></tr></table></center></button></form>";
                            $("#container").append(row);
                        }
                    }
                },
                error: function(jqXHR) {
                    $("#result").html('<font color="#ff0000">RRRRRRRRRRR' + jqXHR.responseText + '</font>');
                }
            })
        });

        $("#Fuzzy_Search").keyup(function() {
            $.ajax({
                type: "POST",
                url: "service.php",
                dataType: "json",
                data: {
                    Feature: "show_all",
                    Fuzzy_Search: $("#Fuzzy_Search").val(),
                    Sorting: $("#Sorting").val(),
                    // Grouping: $("#Grouping").val(),
                    Start: $("#Start").val(),
                    End: $("#End").val()
                },
                success: function(data) {
                    $("#result").html('');
                    $("#container").html('');
                    var json_array = data;
                    var pre_date = '';
                    var date = '';
                    var row = '';
                    for(var date in json_array){
                        row = "<div class='separator'>"+date.replace(/-/g, "/")+"</div>";
                        $("#container").append(row);
                        for(i=0;i<json_array[date].length;i++){
                            let Order_ID = json_array[date][i].Order_ID;
                            let Customer_Name = json_array[date][i].Customer_Name;
                            let Customer_Phone = json_array[date][i].Customer_Phone;
                            let Customer_Email = json_array[date][i].Customer_Email;
                            let Estimated_Rent_DateTime = json_array[date][i].Estimated_Rent_DateTime.replace(/-/g, "/");
                            Estimated_Rent_DateTime = Estimated_Rent_DateTime.split(":")[0] + ":" + Estimated_Rent_DateTime.split(":")[1];
                            let Estimated_Return_DateTime = json_array[date][i].Estimated_Return_DateTime.replace(/-/g, "/");
                            Estimated_Return_DateTime = Estimated_Return_DateTime.split(":")[0] + ":" + Estimated_Return_DateTime.split(":")[1];
                            let Car_Info = json_array[date][i].Car_Type + " / " + json_array[date][i].License_Plate;
                            if(json_array[date][i].License_Plate=="")
                                Car_Info = json_array[date][i].Car_Type + " / 未排車";
                            let Rental_Area = json_array[date][i].Rental_Area;

                            let Status = Status_Text[parseInt(json_array[date][i].Status)];

                            // if(i==0){
                            //     switch(document.getElementById("Sorting").value){
                            //         case ":D":
                            //         break;
                            //     }
                            // }
                            // else{
                            //     switch($("#Sorting").val()){
                                    
                            //     }
                            // }
                            row = "<form action='Order_Details.php' method='POST'><button type='submit' name='Action' value='"+Order_ID+"' class='info_button_table'><center><table class='info_table'><tr><td class='info_td' width='35%'>訂單編號：<span style='color:#0091FF'>"+Order_ID+"</span></td><td class='info_td' width='40%'>客戶姓名："+Customer_Name+"</td><td class='function_td'>" + Status + "</td></tr><tr><td class='info_td'>租車日期：<table style='display:inline' border='0'><tr><td>從"+Estimated_Rent_DateTime+"</td></tr><tr><td>到"+Estimated_Return_DateTime+"</td></tr></table></td><td class='info_td'>連絡電話："+Customer_Phone+"</td><td class='function_td'>出租店："+Rental_Area+"</td></tr><tr><td class='info_td'>車型資訊："+Car_Info+"</td><td class='info_td'>E-mail："+Customer_Email+"</td><td class='function_td'><button type='submit' name='Action' value='"+Order_ID+"' class='function_btn' style='font-size:20px;width:200px;height:50px;background-color:#0091FF'>訂單資訊</button></td></tr></table></center></button></form>";
                            $("#container").append(row);
                        }
                    }
                },
                error: function(jqXHR) {
                    $("#result").html('<font color="#ff0000">RRRRRRRRRRR' + jqXHR.responseText + '</font>');
                }
            })
        });

        $("#Today").click(function(){
            var d = new Date();
            var Y = d.getFullYear();
            var M = d.getMonth()+1;
            var D = d.getDate();
            D = parseInt(D);
            if(D/10==0)
                D = "0"+D;
            // $("#Grouping").prop('selectedIndex',0);
            $("#Sorting").prop('selectedIndex',0);
            $("#Fuzzy_Search").val('');
            $("#Start").val(Y+"/"+M+"/"+D);
            $("#End").val(Y+"/"+M+"/"+D);
            $.ajax({
                type: "POST",
                url: "service.php",
                dataType: "json",
                data: {
                    Feature: "show_all",
                    Fuzzy_Search: $("#Fuzzy_Search").val(),
                    Sorting: $("#Sorting").val(),
                    // Grouping: $("#Grouping").val(),
                    Start: $("#Start").val(),
                    End: $("#End").val()
                },
                success: function(data) {
                    $("#result").html('');
                    $("#container").html('');
                    var json_array = data;
                    var pre_date = '';
                    var date = '';
                    var row = '';
                    for(var date in json_array){
                        row = "<div class='separator'>"+date.replace(/-/g, "/")+"</div>";
                        $("#container").append(row);
                        for(i=0;i<json_array[date].length;i++){
                            let Order_ID = json_array[date][i].Order_ID;
                            let Customer_Name = json_array[date][i].Customer_Name;
                            let Customer_Phone = json_array[date][i].Customer_Phone;
                            let Customer_Email = json_array[date][i].Customer_Email;
                            let Estimated_Rent_DateTime = json_array[date][i].Estimated_Rent_DateTime.replace(/-/g, "/");
                            Estimated_Rent_DateTime = Estimated_Rent_DateTime.split(":")[0] + ":" + Estimated_Rent_DateTime.split(":")[1];
                            let Estimated_Return_DateTime = json_array[date][i].Estimated_Return_DateTime.replace(/-/g, "/");
                            Estimated_Return_DateTime = Estimated_Return_DateTime.split(":")[0] + ":" + Estimated_Return_DateTime.split(":")[1];
                            let Car_Info = json_array[date][i].Car_Type + " / " + json_array[date][i].License_Plate;
                            if(json_array[date][i].License_Plate=="")
                                Car_Info = json_array[date][i].Car_Type + " / 未排車";
                            let Rental_Area = json_array[date][i].Rental_Area;

                            let Status = Status_Text[parseInt(json_array[date][i].Status)];

                            // if(i==0){
                            //     switch(document.getElementById("Sorting").value){
                            //         case ":D":
                            //         break;
                            //     }
                            // }
                            // else{
                            //     switch($("#Sorting").val()){
                                    
                            //     }
                            // }
                            row = "<form action='Order_Details.php' method='POST'><button type='submit' name='Action' value='"+Order_ID+"' class='info_button_table'><center><table class='info_table'><tr><td class='info_td' width='35%'>訂單編號：<span style='color:#0091FF'>"+Order_ID+"</span></td><td class='info_td' width='40%'>客戶姓名："+Customer_Name+"</td><td class='function_td'>" + Status + "</td></tr><tr><td class='info_td'>租車日期：<table style='display:inline' border='0'><tr><td>從"+Estimated_Rent_DateTime+"</td></tr><tr><td>到"+Estimated_Return_DateTime+"</td></tr></table></td><td class='info_td'>連絡電話："+Customer_Phone+"</td><td class='function_td'>出租店："+Rental_Area+"</td></tr><tr><td class='info_td'>車型資訊："+Car_Info+"</td><td class='info_td'>E-mail："+Customer_Email+"</td><td class='function_td'><button type='submit' name='Action' value='"+Order_ID+"' class='function_btn' style='font-size:20px;width:200px;height:50px;background-color:#0091FF'>訂單資訊</button></td></tr></table></center></button></form>";
                            $("#container").append(row);
                        }
                    }
                },
                error: function(jqXHR) {
                    $("#result").html('<font color="#ff0000">RRRRRRRRRRR' + jqXHR.responseText + '</font>');
                }
            })
        })

        $("#Reset").click(function(){
            // $("#Grouping").prop('selectedIndex',0);
            $("#Sorting").prop('selectedIndex',0);
            $("#Fuzzy_Search").val('');
            $("#Start").val('');
            $("#End").val('');
            $.ajax({
                type: "POST",
                url: "service.php",
                dataType: "json",
                data: {
                    Feature: "show_all",
                    Fuzzy_Search: $("#Fuzzy_Search").val(),
                    Sorting: $("#Sorting").val(),
                    // Grouping: $("#Grouping").val(),
                    Start: $("#Start").val(),
                    End: $("#End").val()
                },
                success: function(data) {
                    $("#result").html('');
                    $("#container").html('');
                    var json_array = data;
                    var pre_date = '';
                    var date = '';
                    var row = '';
                    for(var date in json_array){
                        row = "<div class='separator'>"+date.replace(/-/g, "/")+"</div>";
                        $("#container").append(row);
                        for(i=0;i<json_array[date].length;i++){
                            let Order_ID = json_array[date][i].Order_ID;
                            let Customer_Name = json_array[date][i].Customer_Name;
                            let Customer_Phone = json_array[date][i].Customer_Phone;
                            let Customer_Email = json_array[date][i].Customer_Email;
                            let Estimated_Rent_DateTime = json_array[date][i].Estimated_Rent_DateTime.replace(/-/g, "/");
                            Estimated_Rent_DateTime = Estimated_Rent_DateTime.split(":")[0] + ":" + Estimated_Rent_DateTime.split(":")[1];
                            let Estimated_Return_DateTime = json_array[date][i].Estimated_Return_DateTime.replace(/-/g, "/");
                            Estimated_Return_DateTime = Estimated_Return_DateTime.split(":")[0] + ":" + Estimated_Return_DateTime.split(":")[1];
                            let Car_Info = json_array[date][i].Car_Type + " / " + json_array[date][i].License_Plate;
                            if(json_array[date][i].License_Plate=="")
                                Car_Info = json_array[date][i].Car_Type + " / 未排車";
                            let Rental_Area = json_array[date][i].Rental_Area;

                            let Status = Status_Text[parseInt(json_array[date][i].Status)];

                            // if(i==0){
                            //     switch(document.getElementById("Sorting").value){
                            //         case ":D":
                            //         break;
                            //     }
                            // }
                            // else{
                            //     switch($("#Sorting").val()){
                                    
                            //     }
                            // }
                            row = "<form action='Order_Details.php' method='POST'><button type='submit' name='Action' value='"+Order_ID+"' class='info_button_table'><center><table class='info_table'><tr><td class='info_td' width='35%'>訂單編號：<span style='color:#0091FF'>"+Order_ID+"</span></td><td class='info_td' width='40%'>客戶姓名："+Customer_Name+"</td><td class='function_td'>" + Status + "</td></tr><tr><td class='info_td'>租車日期：<table style='display:inline' border='0'><tr><td>從"+Estimated_Rent_DateTime+"</td></tr><tr><td>到"+Estimated_Return_DateTime+"</td></tr></table></td><td class='info_td'>連絡電話："+Customer_Phone+"</td><td class='function_td'>出租店："+Rental_Area+"</td></tr><tr><td class='info_td'>車型資訊："+Car_Info+"</td><td class='info_td'>E-mail："+Customer_Email+"</td><td class='function_td'><button type='submit' name='Action' value='"+Order_ID+"' class='function_btn' style='font-size:20px;width:200px;height:50px;background-color:#0091FF'>訂單資訊</button></td></tr></table></center></button></form>";
                            $("#container").append(row);
                        }
                    }
                },
                error: function(jqXHR) {
                    $("#result").html('<font color="#ff0000">RRRRRRRRRRR' + jqXHR.responseText + '</font>');
                }
            })
        })

        $("#New_Order").click(function(){
            location.href = '../Orders/New_Order.php';
        })

        $.ajax({
            type: "POST",
            url: "service.php",
            dataType: "json",
            data: {
                Feature: "show_all",
                Fuzzy_Search: $("#Fuzzy_Search").val(),
                Sorting: $("#Sorting").val(),
                // Grouping: $("#Grouping").val(),
                Start: $("#Start").val(),
                End: $("#End").val()
            },
            success: function(data) {
                $("#result").html('');
                $("#container").html('');
                var json_array = data;
                var pre_date = '';
                var date = '';
                var row = '';
                for(var date in json_array){
                    row = "<div class='separator'>"+date.replace(/-/g, "/")+"</div>";
                    $("#container").append(row);
                    for(i=0;i<json_array[date].length;i++){
                        let Order_ID = json_array[date][i].Order_ID;
                        let Customer_Name = json_array[date][i].Customer_Name;
                        let Customer_Phone = json_array[date][i].Customer_Phone;
                        let Customer_Email = json_array[date][i].Customer_Email;
                        let Estimated_Rent_DateTime = json_array[date][i].Estimated_Rent_DateTime.replace(/-/g, "/");
                        Estimated_Rent_DateTime = Estimated_Rent_DateTime.split(":")[0] + ":" + Estimated_Rent_DateTime.split(":")[1];
                        let Estimated_Return_DateTime = json_array[date][i].Estimated_Return_DateTime.replace(/-/g, "/");
                        Estimated_Return_DateTime = Estimated_Return_DateTime.split(":")[0] + ":" + Estimated_Return_DateTime.split(":")[1];
                        let Car_Info = json_array[date][i].Car_Type + " / " + json_array[date][i].License_Plate;
                        if(json_array[date][i].License_Plate=="")
                            Car_Info = json_array[date][i].Car_Type + " / 未排車";
                        let Rental_Area = json_array[date][i].Rental_Area;

                        let Status = Status_Text[parseInt(json_array[date][i].Status)];

                        // if(i==0){
                        //     switch(document.getElementById("Sorting").value){
                        //         case ":D":
                        //         break;
                        //     }
                        // }
                        // else{
                        //     switch($("#Sorting").val()){
                                
                        //     }
                        // }
                        row = "<form action='Order_Details.php' method='POST'><button type='submit' name='Action' value='"+Order_ID+"' class='info_button_table'><center><table class='info_table'><tr><td class='info_td' width='35%'>訂單編號：<span style='color:#0091FF'>"+Order_ID+"</span></td><td class='info_td' width='40%'>客戶姓名："+Customer_Name+"</td><td class='function_td'>" + Status + "</td></tr><tr><td class='info_td'>租車日期：<table style='display:inline' border='0'><tr><td>從"+Estimated_Rent_DateTime+"</td></tr><tr><td>到"+Estimated_Return_DateTime+"</td></tr></table></td><td class='info_td'>連絡電話："+Customer_Phone+"</td><td class='function_td'>出租店："+Rental_Area+"</td></tr><tr><td class='info_td'>車型資訊："+Car_Info+"</td><td class='info_td'>E-mail："+Customer_Email+"</td><td class='function_td'><button type='submit' name='Action' value='"+Order_ID+"' class='function_btn' style='font-size:20px;width:200px;height:50px;background-color:#0091FF'>訂單資訊</button></td></tr></table></center></button></form>";
                        $("#container").append(row);
                    }
                }
            },
            error: function(jqXHR) {
                $("#result").html('<font color="#ff0000">RRRRRRRRRRR' + jqXHR.responseText + '</font>');
            }
        })
    });
</script>