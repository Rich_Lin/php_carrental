<?php session_start();?>
<head>
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.7.0/css/font-awesome.min.css">
    <link rel="stylesheet" href="../RLC_CSS_Style.css">
</head>
<div class='centered vertical-menu'>
    <img src="../images/LWithN.png" height="auto" width='174px'>
    <a href="../Car_Arrangement/">車輛行事曆</a>
    <a href="../Orders/">所有訂單</a>
    <a href="../Customer/">客戶資料</a>
    <!-- <a href="../daily_report/">出租日報</a> -->
    <!-- <a href="../duty_shift/">結帳作業</a> -->
    <a href="#" class='dropdown-btn' onClick='dropdown_toogle(this)'>設定<i id='arrow-icon' class='fa fa-caret-down'></i></a>
    <div id='dropdown-container' style='display: none;'>
        <?php
            if($_SESSION['Employee_Level']==1)
                echo '
                    <a href="../Branch_Management/">店鋪管理</a>
                    <a href="../HR_System/">人員管理</a>
                ';
        ?>
        <a href="#" onClick='logout()'>登出</a>
    </div>
</div>