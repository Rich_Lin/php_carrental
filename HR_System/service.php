<?php
    header('Content-Type: application/json; charset=UTF-8');
    include_once "../mysql_connect.inc.php";
    include_once "../session_stat.php";
    // error_reporting(0);
    date_default_timezone_set('Asia/Taipei');
    $now = date('Y/m/d H:i:s', time());
    $sql = "INSERT INTO `log_of_all`(`By_Who`, `When_Did`, `Content`) VALUES ('', '".$now."', '')";
    // print_r($_COOKIE);die;
    if ($_SERVER['REQUEST_METHOD'] == "POST") {
        if(isset($_POST)){
            switch($_POST['Feature']){

                case 'get_allocations':
                    $Allocation_Array = array();
                    $sql = "SELECT `BA_ID`, `BA_Name` FROM `branch_allocation` WHERE 1";
                    $result = mysqli_query($conn,$sql);
                    while($row = $result->fetch_assoc())
                        $Allocation_Array[] = $row;
                    echo json_encode($Allocation_Array,JSON_UNESCAPED_UNICODE);
                break;

                case 'get_employees':
                    get_employees($conn);
                break;

                case 'get_specific_employees':
                    $sql = "SELECT * FROM `employee_list` WHERE `Employee_ID`='".$_POST['Employee_ID']."'";
                    echo json_encode(mysqli_query($conn,$sql)->fetch_assoc(),JSON_UNESCAPED_UNICODE);
                break;

                case 'IU_Employee':
                    if(isset($_POST['Employee_ID'])&&!empty($_POST['Employee_ID'])){
                        $sql = "UPDATE `employee_list` SET `Employee_Name`='".$_POST['Employee_Name']."',`L_Name`='".$_POST['L_Name']."',`L_PWord`='".$_POST['L_PWord']."',`Employee_Sex`='".$_POST['Employee_Sex']."',`Employee_Phone`='".$_POST['Employee_Phone']."',`Employee_SSID`='".$_POST['Employee_SSID']."',`Employee_Address`='".$_POST['Employee_Address']."',`Employee_Allocation`='".$_POST['Employee_Allocation']."',`Employee_Type`='".$_POST['Employee_Type']."',`Employee_Salary`='".$_POST['Employee_Salary']."',`Employee_Level`='".$_POST['Employee_Level']."',`Employee_Remark`='".$_POST['Employee_Remark']."'WHERE `Employee_ID`='".$_POST['Employee_ID']."'";
                        if(!mysqli_query($conn,$sql)){
                            echo "SQL Error: HR_IU_EDIT";
                            die;
                        }
                    }
                    else{
                        $sql = "INSERT INTO `employee_list`(`Employee_Name`, `L_Name`, `L_PWord`, `Employee_Sex`, `Employee_Phone`, `Employee_SSID`, `Employee_Address`, `Employee_Allocation`, `Employee_Type`, `Employee_Salary`, `Employee_Level`, `Employee_Remark`) VALUES ('".$_POST['Employee_Name']."','".$_POST['L_Name']."','".$_POST['L_PWord']."','".$_POST['Employee_Sex']."','".$_POST['Employee_Phone']."','".$_POST['Employee_SSID']."','".$_POST['Employee_Address']."','".$_POST['Employee_Allocation']."','".$_POST['Employee_Type']."','".$_POST['Employee_Salary']."','".$_POST['Employee_Level']."','".$_POST['Employee_Remark']."')";
                        if(!mysqli_query($conn,$sql)){
                            echo "SQL Error: HR_IU_NEW";
                            die;
                        }
                    }
                    echo json_encode(array('Success' => true),JSON_UNESCAPED_UNICODE);
                break;

                case 'delete_employee':
                    $sql = "DELETE FROM `employee_list` WHERE `Employee_ID`='".$_POST['Employee_ID']."'";
                    if(!mysqli_query($conn,$sql)){
                        echo "SQL Error: HR_IU_DEL";
                        die;
                    }
                    echo json_encode(array('Success' => true),JSON_UNESCAPED_UNICODE);
                break;
            }
        }
    }

    function get_employees($conn){
        $employee_array = array();
        $sql = "SELECT `Employee_ID`, `Employee_Name`, `Employee_Phone`, `Employee_Allocation`, `Employee_Type`, `Employee_Remark` FROM `employee_list` WHERE `Enable`=1";
        $result = mysqli_query($conn,$sql);
        $count = 0;
        while($row = $result->fetch_assoc()){
            $employee_array[$count] = $row;
            $sql = "SELECT `BA_Name` FROM `branch_allocation` WHERE `BA_ID`='".$row['Employee_Allocation']."'";
            $employee_array[$count]['Employee_Allocation'] = mysqli_query($conn,$sql)->fetch_assoc()['BA_Name'];
            $count++;
        }
        echo json_encode($employee_array,JSON_UNESCAPED_UNICODE);
    }
?>