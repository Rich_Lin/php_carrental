<?php
    include_once "../session_stat.php";
?>
<html>
    <head>
        <script type="text/javascript" src="../js/functions.js"></script>
        <script type="text/javascript" src="https://cdnjs.cloudflare.com/ajax/libs/moment.js/2.22.2/moment.min.js"></script>

        <link rel="stylesheet" href="//code.jquery.com/ui/1.12.1/themes/base/jquery-ui.css">
        <script src="https://code.jquery.com/jquery-1.12.4.js"></script>
        <script src="https://code.jquery.com/ui/1.12.1/jquery-ui.js"></script>
    
        <script type="text/javascript" src="https://cdnjs.cloudflare.com/ajax/libs/twitter-bootstrap/4.3.1/js/bootstrap.bundle.min.js"></script>
        <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/twitter-bootstrap/4.3.1/css/bootstrap.min.css">

        <script type="text/javascript" src="../js/lightpick.js"></script>
        <link rel="stylesheet" type="text/css" href="../js/lightpick.css">

        <style>
            *{
                font-size: 22px;
            }
            p{
                margin-bottom: unset !important;
            }
            #Employee_Table{
                margin-top: 15px;
            }
            #Employee_Table tr:hover{
                background-color: #ffff99;
            }
            #Employee_Table th,td{
                text-align:center;
                padding: 5px 0px;
            }
            #Employee_Table th{
                background-color: #6236FF;
                color: WHITE;
                white-space:nowrap;
            }
            #Employee_Table td{
                width:1%;
                white-space:nowrap;
            }
            #Employee_Form_Table td{
                padding: 10px 0px;
                white-space:nowrap;
            }
            .function_btn{
                margin: 0px 5px;
            }
            .ui-widget.ui-widget-content{
                border-radius: 20px;
                border-width: 20px;
                /* background-color: #DADADA; */
                border: 1px solid #DADADA;
            }
            .ui-widget-content {
                border-radius: 20px;
                border-width: 20px;
            }
            .ui-widget-overlay{
                /* background-color: transparent; */
            }
            .ui-dialog-titlebar{
                display: none
            }
            .HR_Input{
                width: 90%
            }
        </style>

    </head>

    <body onload="includeHTML();get_employees();">
        <div class='navbar-div' include-html="../navbar.php"></div>
        <div class='for_hyper left' include-html="../hyper.php"></div>
        <div class='right'>
            <center>
                <div>
                    <input type='button' class='function_btn' style='width:130px;height:50px;background-color:#F79B00;float:right;margin:15px 30px;' value='新增員工' onClick='Open_Employee_Dialog("New")'>
                </div>
                <div style='width:100%;'>
                    <center>
                        <table id='Employee_Table' width='95%' border='1' style='margin-bottom: 100px;'>
                            <tr>
                                <th>姓名</th>
                                <th>電話</th>
                                <th>雇用形式</th>
                                <th>指派地</th>
                                <th>備註</th>
                                <th></th>
                            </tr>
                        </table>
                    </center>
                </div>
            </center>
        </div>
    </body>
</html>


<!----------Dialog---------->
    <div id='Employee_Dialog' name='dialog_section'><br>
        <center>
            <h2>新增/修改員工資訊</h2>
            <form id='Employee_Form'>
                <input type="hidden" name='Feature' value='IU_Employee' autofocus="true" />
                <input type="hidden" name='Employee_ID' id='Employee_ID' class='HR_Input' autofocus="true" />
                <table id='Employee_Form_Table' width='97.5%'>
                    <tr>
                        <td style='text-align:right;font-size:26px;width:20%'>
                            姓名：
                        </td>
                        <td style='font-size:26px;'>
                            <input type='text' class='HR_Input' id='Employee_Name' name='Employee_Name' placeholder='請輸入姓名' required>
                        </td>
                        
                        <td style='text-align:right;font-size:26px;width:20%'>
                            性別：
                        </td>
                        <td style='font-size:26px;'>
                            <select class='HR_Input' id='Employee_Sex' name='Employee_Sex' required>
                                <option value=''>請選擇性別</option>
                                <option value='0'>女</option>
                                <option value='1'>男</option>
                                <option value='2'>戰鬥直升機</option>
                            </select>
                        </td>
                    </tr>
                    <tr>
                        <td style='text-align:right;font-size:26px;'>
                            帳號：
                        </td>
                        <td style='font-size:26px;'>
                            <input type='text' class='HR_Input' id='L_Name' name='L_Name' placeholder='請輸入帳號' required>
                        </td>
                        
                        <td style='text-align:right;font-size:26px;'>
                            密碼：
                        </td>
                        <td style='font-size:26px;'>
                            <input type='password' class='HR_Input' id='L_PWord' name='L_PWord' placeholder='請輸入密碼' required>
                        </td>
                    </tr>
                    <tr>
                        <td style='text-align:right;font-size:26px;'>
                            身分證字號：
                        </td>
                        <td style='font-size:26px;'>
                            <input type='text' class='HR_Input' id='Employee_SSID' name='Employee_SSID' placeholder='請輸入身分證字號' required>
                        </td>
                        
                        <td style='text-align:right;font-size:26px;'>
                            手機：
                        </td>
                        <td style='font-size:26px;'>
                            <input type='text' class='HR_Input' id='Employee_Phone' name='Employee_Phone' placeholder='請輸入手機' required>
                        </td>
                    </tr>
                    <tr>
                        <td style='text-align:right;font-size:26px;'>
                            地址：
                        </td>
                        <td style='font-size:26px;text-align:left' colspan='3'>
                            <input type='text' class='HR_Input' id='Employee_Address' name='Employee_Address' placeholder='請輸入地址' required>
                        </td>
                    </tr>
                    <tr>
                        <td style='text-align:right;font-size:26px;'>
                            所屬地：
                        </td>
                        <td style='font-size:26px;'>
                            <select class='HR_Input' id='Employee_Allocation' name='Employee_Allocation' required>
                            </select>
                        </td>
                        
                        <td style='text-align:right;font-size:26px;'>
                            權限等級：
                        </td>
                        <td style='font-size:26px;'>
                            <select class='HR_Input' id='Employee_Level' name='Employee_Level' required>
                                <option value=''>請選擇等級</option>
                                <option value='1'>最高權限(含以下功能)</option>
                                <option value='2'>管理權限(含以下功能)</option>
                                <option value='3'>操作權限(含以下功能)</option>
                                <option value='4'>檢視權限</option>
                            </select>
                        </td>
                    </tr>
                    <tr>
                        <td style='text-align:right;font-size:26px;'>
                            雇用類形：
                        </td>
                        <td style='font-size:26px;'>
                            <select class='HR_Input' id='Employee_Type' name='Employee_Type' required>
                                <option value=''>請選擇雇用形態</option>
                                <option value='0'>正職</option>
                                <option value='1'>兼職</option>
                                <option value='2'>不明</option>
                            </select>
                        </td>
                        
                        <td style='text-align:right;font-size:26px;'>
                            薪水：
                        </td>
                        <td style='font-size:26px;'>
                            <input type='number' class='HR_Input' id='Employee_Salary' name='Employee_Salary' placeholder='請輸入薪水' min='0' required>
                        </td>
                    </tr>
                    <tr>
                        <td style='text-align:right;font-size:26px;vertical-align:top'>
                            備註：
                        </td>
                        <td style='font-size:26px;' colspan='3'>
                            <textarea class='HR_Input' id='Employee_Remark' name='Employee_Remark' style='width: 90%;height: 200px;'></textarea>
                        </td>
                    </tr>
                </table>
                <input type='submit' class='function_btn' style='background-color:#0091FF;font-size:24px;' value='新增/修改'>
                <input type='button' class='function_btn' style='background-color:tomato;font-size:24px;' value='取消' onClick='Close_Dialog()'>
            </form>
        </center>
    </div>
<!-------------------------->

<script>
    var Employee_Type = ['正職','兼職','不明'];

    $(document).ready(function() {
        $("#Employee_Dialog").dialog({
            // height: 750,
            width: 850,
            autoResize:true,
            autoOpen: false,
            resizable: false,
            draggable: false,
            modal: true
            // dialogClass: 'ui-dialog-osx',
        });

        $("#Employee_Form").submit(function(){
            event.preventDefault();
            $.ajax({
                type: "POST",
                url: "service.php",
                data: $("#Employee_Form").serialize(),
                success: function(data) {
                    if(data.Success){
                        get_employees();
                        Close_Dialog();
                    }
                },
                error: function(jqXHR) {
                    console.log("error: " + jqXHR.responseText);
                }
            })
        });
        
        $.ajax({
            type: "POST",
            url: "service.php",
            dataType: "json",
            data: {
                Feature: "get_allocations"
            },
            success: function(data) {
                let options = "<option value=''>請選擇所屬地</option>";
                for(let i=0;i<data.length;i++){
                    options += "<option value='"+data[i].BA_ID+"'>"+data[i].BA_Name+"</option>";
                }
                $("#Employee_Allocation").append(options);
            },
            error: function(jqXHR) {
                $("#result").html('<font color="#ff0000">發生錯誤：GB</font>');
                console.log(jqXHR.responseText);
            }
        })
    });

    $("body").on("click",".ui-widget-overlay",function() {
        Close_Dialog();
    });

    function get_employees(){
        $.ajax({
            type: "POST",
            url: "service.php",
            dataType: "json",
            data: {
                Feature: "get_employees"
            },
            success: function(data) {
                let rows = "<tr><th>姓名</th><th>電話</th><th>雇用形式</th><th>指派地</th><th>備註</th><th></th></tr>";
                for(let i=0;i<data.length;i++){
                    rows += "<tr><td>"+data[i].Employee_Name+"</td><td><p>"+data[i].Employee_Phone+"<td>"+Employee_Type[data[i].Employee_Type]+"</td><td>"+data[i].Employee_Allocation+"</td><td><span style='text-overflow:ellipsis;overflow:hidden;width:280px;display:inline-block;white-space:unset'>"+data[i].Employee_Remark+"</span></td><td><button class='function_btn' style='background-color: #0091FF;;width:50px' onclick='Open_Employee_Dialog(\"EDIT_"+data[i].Employee_ID+"\")'>編輯</button><button class='function_btn' onclick='Open_Employee_Dialog(\"DEL_"+data[i].Employee_ID+"\")' style='background-color: RED;width:50px'>刪除</button></td></tr>";
                }
                $("#Employee_Table").html(rows);
            },
            error: function(jqXHR) {
                $("#result").html('<font color="#ff0000">發生錯誤：GB</font>');
                console.log(jqXHR.responseText);
            }
        })
    }

    function Open_Employee_Dialog(action){
        if(action == "New"){
            $("#Employee_Dialog").dialog("open");
        }
        else{
            let Employee_ID = action.split("_")[1];
            action = action.split("_")[0];
            switch(action){
                case "EDIT":
                    $.ajax({
                        type: "POST",
                        url: "service.php",
                        dataType: "json",
                        data: {
                            Feature: "get_specific_employees",
                            Employee_ID: Employee_ID
                        },
                        success: function(data) {
                            $("#Employee_ID").val(data.Employee_ID);
                            $("#Employee_Name").val(data.Employee_Name);
                            $("#Employee_Sex").val(data.Employee_Sex);
                            $("#L_Name").val(data.L_Name);
                            $("#L_PWord").val(data.L_PWord);
                            $("#Employee_SSID").val(data.Employee_SSID);
                            $("#Employee_Phone").val(data.Employee_Phone);
                            $("#Employee_Address").val(data.Employee_Address);
                            $("#Employee_Allocation").val(data.Employee_Allocation);
                            $("#Employee_Level").val(data.Employee_Level);
                            $("#Employee_Type").val(data.Employee_Type);
                            $("#Employee_Salary").val(data.Employee_Salary);
                            $("#Employee_Remark").val(data.Employee_Remark);
                            
                            $("#Employee_Dialog").dialog("open");
                        },
                        error: function(jqXHR) {
                            $("#result").html('<font color="#ff0000">發生錯誤：GB</font>');
                            console.log(jqXHR.responseText);
                        }
                    })
                break;
                
                case "DEL":
                    $.ajax({
                        type: "POST",
                        url: "service.php",
                        dataType: "json",
                        data: {
                            Feature: "delete_employee",
                            Employee_ID: Employee_ID
                        },
                        success: function(data) {
                            get_employees();
                        },
                        error: function(jqXHR) {
                            console.log("error: " + jqXHR.responseText);
                        }
                    })
                break;
            }
        }
    }

    function Close_Dialog(){
        let Targets = document.getElementsByName('dialog_section');
        for(i=0;i<Targets.length;i++)
            $('#'+Targets[i].id).dialog( "close" );
        Targets = document.getElementsByClassName('HR_Input');
        for(i=0;i<Targets.length;i++){
            if(Targets[i].tagName == "SELECT")
                Targets[i].selectedIndex = 0;
            else
                Targets[i].value = "";
        }
        $("#display_color").css("background-color", "");
    }
</script>