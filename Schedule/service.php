<?php
    header('Content-Type: application/json; charset=UTF-8');
    include_once "../mysql_connect.inc.php";
    // error_reporting(0);
    if ($_SERVER['REQUEST_METHOD'] == "POST") {
        //////////////////Customer info//////////////////
        if(isset($_POST)){
            switch($_POST['Feature']){
                case 'show_all':
                    display_daily_schedule($_POST['Date'],$conn);
                break;

                case 'show_booking_list':
                    // date_default_timezone_set('Asia/Taipei');
                    // $Today_Date = date('Y/m/d', time());
                    // $info_array = array();
                    // $sql = " SELECT * FROM `booking_index`,`booking_detail`,`customer` WHERE `booking_index`.`Booking_ID`=`booking_detail`.`Booking_ID` AND `booking_index`.`Customer_ID`=`customer`.`Customer_ID` AND (DATE(`booking_index`.`CIN_Date`)<='$Today_Date' AND DATE(`booking_index`.`COUT_Date`)>='$Today_Date') AND `booking_detail`.`Room_Status`=0";
                    // $GROUP_BY = " GROUP BY `booking_index`.`Booking_ID`";
                    // $Fuzzy_Search = "";
                    // if(!empty($_POST['Fuzzy_Search'])){
                    //     $Fuzzy_Search = " (`customer`.`Customer_Name` LIKE '%".$_POST['Fuzzy_Search']."%' OR `customer`.`Customer_Phone` LIKE '%".$_POST['Fuzzy_Search']."%' OR `customer`.`Customer_Email` LIKE '%".$_POST['Fuzzy_Search']."%' OR `customer`.`Customer_SSID` LIKE '%".$_POST['Fuzzy_Search']."%' OR `customer`.`Customer_Passport` LIKE '%".$_POST['Fuzzy_Search']."%' OR `booking_index`.`Booking_ID` LIKE '%".$_POST['Fuzzy_Search']."%' OR `booking_detail`.`Room_Type` LIKE '%".$_POST['Fuzzy_Search']."%' OR `booking_detail`.`Room_Num` LIKE '%".$_POST['Fuzzy_Search']."%') ";
                    //     $sql .= " AND " . $Fuzzy_Search . $GROUP_BY;
                    // }
                    // else
                    //     $sql .= $GROUP_BY;
                    // $result = mysqli_query($conn,$sql);
                    // $booking_counter=0;
                    // while($row=$result->fetch_assoc()){
                    //     $info_array[$booking_counter]=$row;
                    //     if($row['Room_Count']>1){
                    //         $counter = 1;
                    //         $rooms = '';
                    //         $sql = "SELECT `Room_Num` FROM `booking_detail` WHERE `Booking_ID`='".$row['Booking_ID']."'";
                    //         $room_result = mysqli_query($conn,$sql);
                    //         while($room_row=$room_result->fetch_assoc()){
                    //             $rooms .= $room_row['Room_Num'];
                    //             if($counter<intval($row['Room_Count'])){
                    //                 $rooms .= ', ';
                    //             }
                    //             $counter++;
                    //         }
                    //         $info_array[$booking_counter]['Room_Num'] = $rooms;
                    //     }
                    //     $booking_counter++;
                    // }
                    // for($i=0;$i<sizeof($info_array);$i++){
                    //     $info_array[$i]['Paid'] = 0;
                    //     $sql = "SELECT `Amount` FROM `payment` WHERE `Booking_ID`='".$info_array[$i]['Booking_ID']."'";
                    //     $result = mysqli_query($conn,$sql);
                    //     while($row=$result->fetch_assoc())
                    //         $info_array[$i]['Paid'] += $row['Amount'];
                    // }
                    // // print_r($info_array);
                    // echo json_encode($info_array,JSON_UNESCAPED_UNICODE);
                break;
            }
        }
    }

    function display_daily_schedule($Date,$conn){
        // $Room_Status_Text_Array = array('訂','住','待','清','休','空','清','保','已取消');
        $car_array = array();
        date_default_timezone_set('Asia/Taipei');
        $Today_Date = date('Y/m/d', strtotime($Date));
        $sql = "SELECT * FROM `car_list` ORDER BY `Car_Type`";
        $result = mysqli_query($conn,$sql);
        while($row = $result->fetch_assoc()){
            $car_array[$row['Car_Type']][] = $row['License_Plate'];
        }
        echo json_encode($car_array,JSON_UNESCAPED_UNICODE);
    }
?>