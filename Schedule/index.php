<html>
    <head>
        <style>
            .floor_td{
                vertical-align: top;
                width: 230px;
            }
            .Room_Block{
                position: relative;
                display: block;
                width: 220px;
                height: 120px;
                font-size: 18px;
                padding-left: 10px;
                padding-top: 10px;
                /* border-bottom: 1px solid BLACK; */
            }
            .default_even{
                background-color: #D4D4D4;
            }
            .default_odd{
                background-color: #EFEFEF;
            }
            .background{
                position: absolute;
                color: WHITE;
                font-size: 52px;
                padding-left: 90px;
            }
            .date_btn{
                width: 65px;
                height:54px;
                border-radius: 7.5px 7.5px 0px 0px ;
                background-color: WHITE;
                font-size: 21px;
                margin: 0px;
            }
            .checking{
                background-color: #BFBFBF !important;
                width: 220px !important;
            }
            .current_date{
                background-color: #E5E5E5;
            }
            .ui-widget.ui-widget-content{
                border-radius: 20px;
                border-width: 20px;
                /* background-color: #DADADA; */
                border: 1px solid #DADADA;
            }
            .ui-widget-content {
                border-radius: 20px;
                border-width: 20px;
            }
            .ui-widget-overlay{
                background-color: transparent;
            }
            .ui-dialog-titlebar{
                display: none
            }
            .info_table{
                width: 90%
            }
            .info_table tr,td{
                /* vertical-align: center;
                padding: 5px 0px;
                font-size: 22px; */
                font-size: 22px;
            }
            .info_button_table{
                width: 100%;
                height: 175px;
                background-color: WHITE;
                border: 2.5px solid #979797;
                border-radius:15px;
                margin: 10px;
            }
            .info_button_table:hover{
                /* border: 1px solid #DADADA; */
                background-color: #DADADA;
            }
            #Booked_Room_Dialog td{
                padding: 4px 0px;
            }
            select{
                width: 405;
                height: 50;
                font-size: 24px;
                text-align: center;
            }
        </style>
        <link rel="stylesheet" type="text/css" href="daily_review.css">
        <script type="text/javascript" src="../js/functions.js"></script>
        <script type="text/javascript" src="https://cdnjs.cloudflare.com/ajax/libs/moment.js/2.22.2/moment.min.js"></script>

        <link rel="stylesheet" href="//code.jquery.com/ui/1.12.1/themes/base/jquery-ui.css">
        <script src="https://code.jquery.com/jquery-1.12.4.js"></script>
        <script src="https://code.jquery.com/ui/1.12.1/jquery-ui.js"></script>
        
        <!-- <script type="text/javascript" src="https://code.jquery.com/jquery-3.3.1.slim.min.js"></script> -->
        <script type="text/javascript" src="https://cdnjs.cloudflare.com/ajax/libs/twitter-bootstrap/4.3.1/js/bootstrap.bundle.min.js"></script>
        <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/twitter-bootstrap/4.3.1/css/bootstrap.min.css">

        <script type="text/javascript" src="../js/lightpick.js"></script>
        <link rel="stylesheet" type="text/css" href="../js/lightpick.css">
    </head>
    <body onload='includeHTML();create_datebar();'>
        <div class='navbar-div' include-html="../navbar.html"></div>
        <div class='for_hyper left' include-html="../hyper.html"></div>
        <div class='right'>
            <!-- overflow-y: scroll; -->
            <center>
                <div>
                    <table width='75%' border='0'>
                        <tr>
                            <td><input type='text' style='width:300px;height:50px;font-size:22px;text-align:center;' id='Date'><input type='button' class='function_btn' style='width:130px;height:50px;background-color:#0091FF;' value='今日' onClick='set_Date()'></td>
                            <td style='text-align:right'><input type='button' class='function_btn' style='width:130px;height:50px;background-color:#F79B00;' value='休息登入' onClick='create_rest()'><input type='button' class='function_btn' style='width:130px;height:50px;background-color:#F79B00;' value='訂單入住' onClick='checkin_list()'><input type='button' class='function_btn' style='width:130px;height:50px;background-color:#F79B00;' value='新增訂單' onClick="location.href = '../check_in/reservation.php';"></td>
                        </tr>
                    </table>
                </div>
                <div>
                    <table width='95%'  style='table-layout:fixed;' border='0'>
                        <tr>
                            <td width='7.5%'><button class='function_btn' style='width:110;height:50px;color:BLACK;background-color:WHITE;' onClick='set_Month(-1);'>上個月</button></td>
                            <td width='85%' style='text-align:center'><div id='datebar_container'></div></td>
                            <td width='7.5%' style='text-align:right'><button class='function_btn' style='width:110;height:50px;color:BLACK;background-color:WHITE;' onClick='set_Month(1);'>下個月</button></td>
                        </tr>                    
                    </table>
                            <center><div id='roomtable_container' style='width:80%;overflow-x: scroll;'></div></center>
                        
                </div>
            </center>
        </div>
    </body>
</html>
<!-- Dialogs -->

    <div id="Rest_Dialog" name='dialog_section'><br>
        <form id='rest_form' name='rest_form' action='service.php' method='POST'>
            <input type='hidden' name='Feature' value='create_rest_order'>
            <div id='reservation'>
                <center>
                    <table>
                        <tr><td style='font-size:57px;text-align:center;'>
                            休息登入
                        </td></tr>

                        <tr><td style='font-size:26px;text-align:center;'>
                            <select id='room_type_selector' name='room_type_selector' class='modified_select' style='width:541.4px;height:87.2px' onchange='select_room(this.value);' required>
                                <option value=''>請選擇房型</option>
                            </select>
                        </td></tr>

                        <tr><td style='font-size:26px;text-align:center;'>
                            <select id='room_num_selector' name='room_num_selector' class='modified_select' style='width:541.4px;height:87.2px' required>
                                <option value=''>請選擇房號</option>
                            </select>
                        </td></tr>
                        
                        <tr><td style='font-size:26px;'>
                            <div style='width:49%;text-align:left;display:inline-block'>
                                時數：<input type='number' name='rest_hour' style='height:50px;width:80px;font-size:26px;' id='rest_hour' min='1' value='1' onkeyup='set_rest_price(this.value);' onChange='set_rest_price(this.value);' required>
                            </div>
                            <div style='width:49%;text-align:right;display:inline-block'>
                                總額：$<span id='rest_Total_Price_text'></span>
                            </div>
                        </td></tr>
                        
                        <tr><td style='font-size:26px;text-align:center;height:50px;'>
                            <span id='warning_message'></span>
                        </td></tr>
                        
                        <tr><td style='font-size:26px;text-align:right;'>
                            <button type='button' class='function_btn' style='background-color:#0091FF' id='to_payment' onClick='page_to_payment()'>收款</button>
                        </td></tr>
                    </table>
                </center>
            </div>
            <div id='payment' style='display:none;'>
                <center>
                    <table cellspacing='10'>
                        <tr><td style='font-size:32px;'>
                            總額：<span id='Rest_Total_Price'></span>
                            <input type='hidden' id='rest_Total_Price' name='rest_Total_Price'>
                        </td></tr>
                        <tr><td style='font-size:26px;'>
                            收款人員：
                            <select id='staff_selector' class='modified_select' style='width:250px;height:50px;' name='staff_selector' required>
                                <option value=''>請選擇人員</option>
                            </select>
                        </td></tr>
                        <tr><td style='font-size:26px;'>
                            <div id='payment_area'>
                                <center>
                                    <table border='1' width='100%' cellspacing='10' style='border-radius:15px;'>
                                        <tr>
                                            <td style='font-size:26px;text-align:right;border:0'>收款方式：</td>
                                            <td style='font-size:26px;border:0'>
                                                <select class='modified_select' name='payment_method[]' style='width:250px;height:50px;' required>
                                                    <option value=''>請選擇付款方式</option>
                                                    <option value='0'>現金</option>
                                                    <option value='1'>信用卡</option>
                                                    <option value='2'>轉帳</option>
                                                    <option value='3'>其它</option>
                                                </select>
                                            </td>
                                        </tr>
                                        <tr>
                                            <td style='font-size:26px;text-align:right;border:0'>收款金額：</td>
                                            <td style='font-size:26px;border:0'><input type='number' style='width:250px;height:50px;' name='payment_amount[]' min='0' required></td>
                                        </tr>
                                        <tr>
                                            <td style='font-size:26px;text-align:right;vertical-align:top;border:0'>備註：</td>
                                            <td style='font-size:26px;border:0'><textarea name='Remark[]' rows='3'></textarea></td>
                                        </tr>
                                    </table>
                                </center>
                            </div>
                        </td></tr>
                        <tr><td style='font-size:32px;text-align:center;'>
                            <button type='button' id='new_btn' style='width:520px;height:66px;color:#0091FF' class='function_btn' onClick='display_another_payment()'>+ 增加</button>
                        </td></tr>
                        <tr><td style='font-size:32px;text-align:center;'>
                            <button style='width:520px;height:66px;background-color:#0091FF' class='function_btn'>完成</button>
                        </td></tr>
                    </table>
                </center>
            </div>
        </form>
    </div>

    <div id="Empty_Room_Info_Dialog" name='dialog_section'><br>
        <center>
            <table border='0' width='100%'>
                <tr>
                    <td style='font-size:32px'>房型資訊：</td>
                </tr>
                <tr>
                    <td><br></td>
                </tr>
                <tr>
                    <td width='30%' style='font-size:26px'>名稱：<span id='Room_Type_Text'></span></td>
                </tr>
                <tr>
                    <td width='30%' style='font-size:26px'>介紹：<span id='Room_Description'>無</span></td>
                </tr>
                <tr>
                    <td style='text-align:right'><button class='function_btn' style='width:130px;height:50px;font-size:24px;background-color:#0091FF;' onClick='close_dialog()'>確定</button></td>
                </tr>
            </table>
        </center>
    </div>

    <div id="Booked_Room_Dialog" name='dialog_section'><br>
        <center>
            <table id='Index_Table' width='90%'>
                <tr><td rowspan='2'><center><img src='../images/LWithoutN.png' style='width:80px;height:80px;'></center></td><td style='font-size:32px'>訂單編號：<span id='Dialog_Booking_ID' style='font-size:26px;color:#0091FF'></td></tr>
                <tr><td style='font-size:32px'>訂房日期：<span id='Dialog_Booking_Date' style='font-size:26px'></span></td></tr>
            </table>
            <br>
            <table id='Customer_Table' width='90%'>
                <tr><td style='font-size:26px'>旅客姓名：<span id='Customer_Name' style='font-size:26px'></span></td></tr>
                <tr><td style='font-size:26px'>旅客性別：<span id='Customer_Sex' style='font-size:26px'></span></td></tr>
                <tr><td style='font-size:26px'>旅客國籍：<span id='Customer_Nationality' style='font-size:26px'></span></td></tr>
                <tr><td style='font-size:26px'>入住日期：<span id='CIN_Date' style='font-size:26px'></span></td></tr>
                <tr><td style='font-size:26px'>退房日期：<span id='COUT_Date' style='font-size:26px'></span></td></tr>
                <tr><td style='font-size:26px'>連絡電話：<span id='Customer_Phone' style='font-size:26px'></span></td></tr>
                <tr><td style='font-size:26px'>E-Mail：<span id='Customer_Email' style='font-size:26px'></span></td></tr>
            </table>
            <br>
            <table id='Detail_Table' width='90%'>
                <tr>
                    <td style='font-size:26px'>房型：<span id='Room_Type' style='font-size:26px'></span></td>
                    <td style='font-size:26px'>總價：<span id='Total_Price' style='font-size:26px'></span></td>
                </tr>
                <tr>
                    <td style='font-size:26px'>房號：<span id='Room_Num' style='font-size:26px'></span></td>
                    <td style='font-size:26px'>已收金額：<span id='Total_Paid' style='font-size:26px'></span></td>
                </tr>
                <tr>
                    <td style='font-size:26px'>入住人數：<span id='People_Count' style='font-size:26px'></span></td>
                    <td style='font-size:26px'>代收金額：<span id='Unpaid' style='font-size:26px'></span></td>
                </tr>
                <tr>
                    <td style='font-size:26px'>入住天數：<span id='Day_Count' style='font-size:26px'></span></td>
                    <td style='font-size:26px'>房卡數量：<span id='Keycard_Counter' style='font-size:26px'></span></td>
                </tr>
                <tr>
                    <td colspan='2' style='font-size:26px'>備註：<span id='Index_Remark' style='font-size:26px'></span></td>
                </tr>
            </table>
            <br>
            <table width='90%'>
                <tr>
                    <td style='text-align:left'>
                        <form style='display:inline' action='change_room.php' method='POST'>
                            <input type='hidden' class='Booked_Room_Dialog_Booking_ID' name='Booking_ID'>
                            <input type='hidden' class='Booked_Room_Dialog_Room_Status' name='Room_Status'>
                            <input type='hidden' class='Booked_Room_Dialog_Room_Num' name='Room_Num'>
                            <input type='submit' style='width:130px;height:50px;background-color:#0091FF;font-size:24px;margin:0px 5px;' class='function_btn' value='換房'>
                        </form>
                    </td>
                    <td style='text-align:right'>
                        <form id='Room_Status_Change' style='display:inline' action='service.php' method='POST'>
                            <input type='hidden' name='Feature' value='Room_Status_Change'>
                            <input type='hidden' class='Booked_Room_Dialog_Booking_ID' name='Booking_ID'>
                            <input type='hidden' class='Booked_Room_Dialog_Room_Status' name='Room_Status'>
                            <input type='hidden' class='Booked_Room_Dialog_Room_Num' name='Room_Num'>
                            <button id='Check_Button' style='width:130px;height:50px;background-color:#0091FF;font-size:24px;margin:0px 5px;' class='function_btn'></button>
                        </form>
                        <button type="button" style='width:130px;height:50px;background-color:#0091FF;font-size:24px;margin:0px 5px;' class='function_btn' onClick="close_dialog()">確定</button>
                    </td>
                </tr>
            </table>
        </center>
    </div>

    <div id='Booking_List_Dialog' name='dialog_section'><br>
        <center>
            <table width='80%'>
                <tr><td style='text-align:center;font-size:36px;'>未入住訂單：<input type='text' id='Fuzzy_Search' style='text-align:center;font-size:36px;font-family:Microsoft JhengHei;' placeholder='關鍵字搜尋'></td></tr>
            </table>
            <form action='../booking_list/change_info.php' method='POST'>
                <table id='Booking_List_Table' width='90%' style='table-layout: fixed;'>
                </table>
            </form>
        </center>
    </div>
    
    <div id="Room_Cleaning_Info_Dialog" name='dialog_section'><br>
        <center>
            <form id='cleaning_request_form' action='service.php' method='POST'>
                <input type='hidden' name='Feature' value='change_cleaning_status'>
                <input type='hidden' name='Room_Num_Hidden' id='Cleaning_Room_Num_Hidden'>
                <input type='hidden' name='Booking_ID_Hidden' id='Cleaning_Booking_ID_Hidden'>
                <input type='hidden' name='Room_Status_Hidden' id='Cleaning_Room_Status_Hidden'>
                <table border='0' width='100%'>
                    <tr>
                        <td style='font-size:32px'>下一筆訂單資訊：</td>
                    </tr>
                    <tr>
                        <td><br></td>
                    </tr>
                    <tr>
                        <td width='30%' style='font-size:26px'>宿期：<span id='Cleaning_Next_Period'>無</span></td>
                    </tr>
                    <tr>
                        <td width='30%' style='font-size:26px'>訂單備註：<span id='Cleaning_Next_Booking_Remark'></span></td>
                    </tr>
                    <tr>
                        <td width='30%' style='font-size:26px;height:18px;'></td>
                    </tr>
                    <tr>
                        <td style='text-align:right'>
                            <button class='function_btn' id='Cancel_btn' style='width:130px;height:50px;font-size:24px;background-color:#0091FF;display:none;' onClick='close_dialog()'>取消</button>
                            <select id='cleaning_staff' name='cleaning_staff' style='width:250px;' required><option value=''>請選擇清潔人員</option></select>
                            <button class='function_btn' id='Cleaning_btn' style='width:130px;height:50px;font-size:24px;background-color:#0091FF;'></button>
                        </td>
                    </tr>
                </table>
            </form>
        </center>
    </div>

<!------------------>
<script>
    var gender = ['女','男','不明'];
    
    var room_json = [];
    
    $(document).ready(function() {

        $("#Rest_Dialog").dialog({
            height: 600,
            width: 700,
            autoOpen: false,
            resizable: false,
            draggable: false,
            modal: true
            // dialogClass: 'ui-dialog-osx',
        });

        $("#Booked_Room_Dialog").dialog({
            height: 876,
            width: 669,
            autoOpen: false,
            resizable: false,
            draggable: false,
            modal: true
            // dialogClass: 'ui-dialog-osx',
        });
        
        $("#Empty_Room_Info_Dialog").dialog({
            height: 378,
            width: 456,
            autoOpen: false,
            resizable: false,
            draggable: false,
            modal: true
            // dialogClass: 'ui-dialog-osx',
        });
        
        $("#Room_Cleaning_Info_Dialog").dialog({
            height: 330,
            width: 560,
            autoOpen: false,
            resizable: false,
            draggable: false,
            modal: true
            // dialogClass: 'ui-dialog-osx',
        });
        
        $("#Booking_List_Dialog").dialog({
            height: 854,
            width: 970,
            autoOpen: false,
            resizable: false,
            draggable: false,
            modal: true
            // dialogClass: 'ui-dialog-osx',
        });

        $("#Room_Status_Change").submit(function(){
            event.preventDefault();
            $.ajax({
                type: "POST",
                url: "service.php",
                data: $("#Room_Status_Change").serialize(),
                success: function(data) {
                    switch(data['Room_Status']){
                        case 0:
                            $("#Check_Button").html('辦理入住');
                            if(moment().startOf('day')<moment(new Date(data['CIN_Date']))){
                                document.getElementById("Check_Button").disabled = true;
                            }
                            else{
                                document.getElementById("Check_Button").disabled = false;
                            }
                            close_dialog();
                        break;

                        case 1:
                            $("#Check_Button").html('退房');
                            close_dialog();
                        break;

                        default:
                            close_dialog();
                    }
                    $("#Room_Status_Hidden").val(data['Room_Status']);
                    display_cartable();
                },
                error: function(jqXHR) {
                    console.log("error: " + jqXHR.responseText);
                }
            })
        });

        $("#cleaning_request_form").submit(function(){
            event.preventDefault();
            $.ajax({
                type: "POST",
                url: "service.php",
                data: $("#cleaning_request_form").serialize(),
                success: function(data) {
                    display_cartable();
                    close_dialog();
                },
                error: function(jqXHR) {
                    console.log("error: " + jqXHR.responseText);
                }
            })
        });

        $("#rest_form").submit(function(){
            event.preventDefault();
            $.ajax({
                type: "POST",
                url: "service.php",
                data: $("#rest_form").serialize(),
                success: function(data) {
                    display_cartable();
                    close_dialog();
                },
                error: function(jqXHR) {
                    console.log("error: " + jqXHR.responseText);
                }
            })
        });

        $("#Fuzzy_Search").keyup(function(){
            checkin_list();
        });
    });

    var picker = new Lightpick({
        field: document.getElementById('Date'),
        startDate: moment().startOf('day'),
        singleDate: true,
        repick: true,
        onSelect: function(date){
            create_datebar();
            // Day count is disabled at line 719
        }
    });

    function display_cartable(){
        $.ajax({
            type: "POST",
            url: "service.php",
            dataType: "json",
            data: {
                Feature: "show_all",
                Date: $("#Date").val(),
            },
            success: function(data) {
                $("#roomtable_container").html('');
                var json_array = data;
                table = "<table><tr>";
                for(var floor in json_array){
                    table +="<td class='floor_td'>";
                    var count = 0;
                    var div = '';
                    for(var room in json_array[floor]){
                        div += "<div id='"+room+"' class='Room_Block ";
                        console.log(json_array[floor]);
                        if(json_array[floor][room].length === 0){
                            if(count%2==0)
                                div += "default_even' onClick='open_dialog(this);'><span style='font-size:22px;'>" + room + "</span>";
                            else
                                div += "default_odd' onClick='open_dialog(this);'><span style='font-size:22px;'>" + room + "</span>";
                        }
                        else{
                            var onClick_function = "";
                            var Booking_ID = json_array[floor][room]['Booking_ID'];
                            var customer_name = json_array[floor][room]['Customer_Name'];
                            var Order_Period = json_array[floor][room]['Order_Period'];
                            var Room_Status = json_array[floor][room]['Room_Status'];
                            var Room_Status_Text = json_array[floor][room]['Room_Status_Text'];
                            switch(json_array[floor][room]['First_Status']){
                                case '2':
                                case '3':
                                    onClick_function = "open_cleaning_dialog(this);";
                                break;

                                default:
                                    onClick_function = "open_dialog(this);";

                            }
                            div += Room_Status + "' name='" + Booking_ID + "' onClick='" + onClick_function + "'><div class='background'>" + Room_Status_Text + "</div><span style='font-size:22px;'>" + room + "</span><br><br><span>" + customer_name + "<br>" + Order_Period + "</span>";
                        }
                        div += "</div>";
                        count++;
                    }
                    table += div + "</td>";
                }
                table += "</tr></table>";
                $("#roomtable_container").html(table);
            },
            error: function(jqXHR) {
                console.log(jqXHR.responseText);
            }
        })
        setTimeout('display_cartable()',60000);
    }

    function set_Date(){
        $("#Date").val(moment().startOf('day').format('YYYY/MM/DD'));
        create_datebar();
    }

    function create_datebar(){
        var Today_Date = moment().startOf('day');
        var Start_Date = $("#Date").val();
        var minDate =  moment(new Date(Start_Date)).subtract(7, 'day').format('YYYY/MM/DD');
        var row='';
        for(i=0;i<15;i++){
            row += "<input type='button' id='" + moment(new Date(minDate)).add(i, 'day').format('YYYY/MM/DD') + "' class='date_btn";
            if(i<4 || i>10){
                row += " over_size";
            }
            if(moment(new Date(minDate)).add(i, 'day').format('YYYY/MM/DD')==moment(new Date(Today_Date)).format('YYYY/MM/DD')){
                row += " current_date";
            }
            if(i==7)
                row += " checking' value='" + moment(new Date(minDate)).add(i, 'day').format('YYYY/MM/DD') + "' onClick='set_Checking_Date(this.id)'>";
            else
                row += "' value='" + moment(new Date(minDate)).add(i, 'day').format('MM/DD') + "' onClick='set_Checking_Date(this.id)'>";
        }
        $("#datebar_container").html(row);
        display_cartable();
    }

    function set_Checking_Date(target_value){
        $("#Date").val(target_value);
        create_datebar();
    }

    function set_Month(value){
        var Current_Date = $("#Date").val();
        switch(value){
            case -1:
                $("#Date").val(moment(new Date(Current_Date)).subtract(1, 'month').format('YYYY/MM/DD'));
                create_datebar();
            break;

            case 1:
                $("#Date").val(moment(new Date(Current_Date)).add(1, 'month').format('YYYY/MM/DD'));
                create_datebar();
            break;
        }
    }

    function open_dialog(target){
        var The_Name = target.getAttribute("name");
        var The_ID = target.id;
        if(The_Name){
            ////////if name is set, which means it has booking_id
            $.ajax({
                type: "POST",
                url: "service.php",
                dataType: "json",
                data: {
                    Feature: "check_booked_room",
                    Room_Num: The_ID,
                    Booking_ID: The_Name,
                },
                success: function(data) {


                    // <input type='hidden' class='Booked_Room_Dialog_Booking_ID' name='Booking_ID'>
                    // <input type='hidden' class='Booked_Room_Dialog_Room_Status' name='Room_Status'>
                    // <input type='hidden' class='Booked_Room_Dialog_Room_Num' name='Room_Num'>

                    var x = document.getElementsByClassName("Booked_Room_Dialog_Booking_ID");
                    for(i=0;i<x.length;i++)
                        x[i].value = data['Booking_ID'];
                    
                    var x = document.getElementsByClassName("Booked_Room_Dialog_Room_Status");
                    for(i=0;i<x.length;i++)
                        x[i].value = data['Room_Status'];
                    
                    var x = document.getElementsByClassName("Booked_Room_Dialog_Room_Num");
                    for(i=0;i<x.length;i++)
                        x[i].value = data['Room_Num'];

                    $("#Dialog_Booking_ID").html(data['Booking_ID']);
                    $("#Dialog_Booking_Date").html(data['Booking_Date'].replace(/-/g, "/"));
                    $("#Customer_Name").html(data['Customer_Name']);
                    $("#Customer_Sex").html(gender[data['Customer_Sex']]);
                    $("#Customer_Nationality").html(data['Customer_Nationality']);
                    $("#Customer_Phone").html(data['Customer_Phone']);
                    $("#Customer_Email").html(data['Customer_Email']);
                    $("#CIN_Date").html(data['CIN_Date'].replace(/-/g, "/"));
                    $("#COUT_Date").html(data['COUT_Date'].replace(/-/g, "/"));
                    $("#Room_Type").html(data['Room_Type']);
                    $("#Room_Num").html(data['Room_Num']);
                    $("#People_Count").html(data['People_Count']);
                    $("#Day_Count").html(data['Day_Count']);
                    $("#Keycard_Counter").html(data['Keycard_Counter']);
                    $("#Index_Remark").html(data['Index_Remark']);
                    $("#Total_Price").html(data['Total_Price']);
                    $("#Total_Paid").html(data['Total_Paid']);
                    $("#Unpaid").html(data['Total_Price']-data['Total_Paid']);
                    switch(data['Room_Status']){
                        case '0':
                            $("#Check_Button").html('辦理入住');
                            if(moment().startOf('day').format('YYYY/MM/DD')<moment(new Date(data['CIN_Date'])).format('YYYY/MM/DD')){
                                document.getElementById("Check_Button").disabled = true;
                            }
                            else{
                                document.getElementById("Check_Button").disabled = false;
                            }
                        break;

                        case '1':
                        case '4':
                            $("#Check_Button").html('退房');
                        break;
                        case '7':
                            $("#Check_Button").html('取消房間');
                        break;
                    }

                    $( "#Booked_Room_Dialog" ).dialog( "open" );
                },
                error: function(jqXHR) {
                    console.log(jqXHR.responseText);
                }
            })
        }
        else{
            ////////if name is unset, which means the room is currently available
            // alert(The_ID);
            $.ajax({
                type: "POST",
                url: "service.php",
                dataType: "json",
                data: {
                    Feature: "check_empty_room",
                    Room_Num: The_ID,
                },
                success: function(data) {
                    $("#Room_Type_Text").html(data['Room_Type']);
                    $("#Room_Description").html(data['Description']);
                    $( "#Empty_Room_Info_Dialog" ).dialog( "open" );
                },
                error: function(jqXHR) {
                    console.log(jqXHR.responseText);
                }
            })
        }
    }

    function open_cleaning_dialog(target){
        var The_Name = target.getAttribute("name");
        var The_ID = target.id;
        $.ajax({
                type: "POST",
                url: "service.php",
                dataType: "json",
                data: {
                    Feature: "check_cleaning_room",
                    Room_Num: The_ID,
                    Booking_ID: The_Name,
                },
                success: function(data) {
                    $("#Cleaning_Booking_ID_Hidden").val(data['Booking_ID']);
                    $("#Cleaning_Room_Status_Hidden").val(data['Room_Status']);
                    $("#Cleaning_Room_Num_Hidden").val(data['Room_Num']);

                    $("#Cleaning_Next_Period").html(data['Next_Period']);
                    $("#Cleaning_Next_Booking_Remark").html(data['Index_Remark']);

                    switch(data['Room_Status']){
                        case '2':
                            $("#Cleaning_btn").html('清掃');
                            document.getElementById("cleaning_staff").style.display = 'inline-block';
                            document.getElementById("cleaning_staff").required = true;
                        break;

                        case '3':
                            $("#Cleaning_btn").html('清掃完成');
                            document.getElementById("cleaning_staff").style.display = 'none';
                            document.getElementById("cleaning_staff").required = false;
                        break;
                    }
                    var options = data['cleaning_staff'].split("(*)");
                    var option_row = "<option value=''>請選擇清潔人員</option>";
                    for(i=0;i<options.length;i++){
                        var cleaning_staff = options[i].split("_");
                        option_row += "<option value='"+cleaning_staff[0]+"'>"+cleaning_staff[1]+"</option>"
                    }
                    $("#cleaning_staff").html(option_row);
                    $( "#Room_Cleaning_Info_Dialog" ).dialog( "open" );
                },
                error: function(jqXHR) {
                    console.log(jqXHR.responseText);
                }
            })
    }

    function checkin_list(){
        $.ajax({
            type: "POST",
            url: "service.php",
            dataType: "json",
            data: {
                Feature: "show_booking_list",
                Fuzzy_Search: $("#Fuzzy_Search").val()
            },
            success: function(data) {
                $("#Booking_List_Table").html('');
                var json_data = data;
                var row = "";
                for(i=0;i<json_data.length;i++){
                    row += "<tr><td><button type='submit' class='info_button_table' value='"+json_data[i]['Booking_ID']+"_info' name='Action'><center><table class='info_table'><tr><td width='50%'>訂單編號：<span style='color:#0091FF;'>"+json_data[i]['Booking_ID']+"</span></td><td>入住日期："+json_data[i]['CIN_Date'].replace(/-/g, "/")+"</td></tr><tr><td>房號：<span style='color:#0091FF;'>"+json_data[i]['Room_Num']+"</span></td><td>退房日期："+json_data[i]['COUT_Date']+"</td></tr><tr><td>房型："+json_data[i]['Room_Type']+"</td><td>旅客姓名："+json_data[i]['Customer_Name']+"</td></tr><tr><td>代收金額："+(json_data[i]['Total_Price']-json_data[i]['Paid'])+"</td><td>連絡電話："+json_data[i]['Customer_Phone']+"</td></tr></table></center></button></td></tr>";
                }
                $("#Booking_List_Table").html(row);
                $( "#Booking_List_Dialog" ).dialog( "open" );
            },
            error: function(jqXHR) {
                console.log(jqXHR.responseText);
            }
        })
    }

    function close_dialog(){
        var Dialog_Section = document.getElementsByName('dialog_section');
        for(i=0;i<Dialog_Section.length;i++)
            $('#'+Dialog_Section[i].id).dialog( "close" );
        $("#cleaning_staff").val('');
        $("#Fuzzy_Search").val('');
        $("#room_type_selector").val('');
        $("#room_num_selector").val('');
        $("#rest_hour").val('1');
        $("#rest_Total_Price").val('');
        $("#staff_selector").val('');
        // $("#").val('');
        $("#rest_Total_Price_text").html('');
        $("#warning_message").html('');
        // $("#").html('');

        document.getElementById('reservation').style.display = 'block';
        document.getElementById('payment').style.display = 'none';

        var x = document.getElementsByName('payment_method[]');
        for(i=0;i<x.length;i++)
            x[i].value = '';
        var x = document.getElementsByName('payment_amount[]');
        for(i=0;i<x.length;i++)
            x[i].value = '';
        var x = document.getElementsByName('Remark[]');
        for(i=0;i<x.length;i++)
            x[i].value = '';

        var x = document.getElementsByClassName('new_payment');
        for(i=0;i<x.length;i++)
            x[i].required = false;
        document.getElementById('new_btn').style.display = "block";
    }

    function create_rest(){
        $.ajax({
            type: "POST",
            url: "service.php",
            dataType: "json",
            data: {
                Feature: "get_rest_room",
            },
            success: function(data) {
                room_json = data;
                var room_type_option = "<option value=''>請選擇房型</option>";
                for(var room_type in room_json){
                    room_type_option += "<option value='"+room_type+"'>"+room_type+"</option>";
                }
                $("#room_type_selector").html(room_type_option);
                $("#Rest_Dialog").dialog( "open" );
            },
            error: function(jqXHR) {
                console.log(jqXHR.responseText);
            }
        })
    }

    function select_room(room_type){
        var room_num_option = "<option value=''>請選擇房號</option>";
        for(i=0;i<room_json[room_type]['Room_Num'].length;i++){
            room_num_option += "<option value='"+room_json[room_type]['Room_Num'][i]+"'>"+room_json[room_type]['Room_Num'][i]+"</option>";
        }
        $("#rest_Total_Price_text").html(parseInt(room_json[room_type]['default_rest_price'],10)+parseInt(($("#rest_hour").val()-1)*room_json[room_type]['rest_per_hour_price'],10));
        $("#room_num_selector").html(room_num_option);
        $("#Rest_Total_Price").html($("#rest_Total_Price_text").html());
        $("#rest_Total_Price").val($("#rest_Total_Price_text").html());
    }

    function set_rest_price(target_value){
        if(target_value>=8){
            $("#warning_message").html('<font color="#ff0000">超過休息時數上限，請新增過夜訂單</font>');
            document.getElementById('to_payment').disabled = true;
            $("#rest_Total_Price_text").html('XXX');
            $("#Rest_Total_Price").html($("#rest_Total_Price_text").html());
            $("#rest_Total_Price").val('0');
        }
        else{
            $("#warning_message").html('');
            document.getElementById('to_payment').disabled = false;
            $("#rest_Total_Price_text").html(parseInt(room_json[$("#room_type_selector").val()]['default_rest_price'],10)+parseInt((target_value-1)*room_json[$("#room_type_selector").val()]['rest_per_hour_price'],10));
            $("#Rest_Total_Price").html($("#rest_Total_Price_text").html());
            $("#rest_Total_Price").val($("#rest_Total_Price_text").html());
        }
    }

    function page_to_payment(){
        if($("#room_type_selector").val()=='' || $("#room_num_selector").val()==''){
            alert("尚有部分未填寫完成");
            return;
        }
        $.ajax({
            type: "POST",
            url: "service.php",
            dataType: "json",
            data: {
                Feature: "get_staff",
            },
            success: function(data) {
                var new_decodedCookie = decodeURIComponent(document.cookie).split(';');
                var ID = new_decodedCookie[0].split('=')[1];
                staff_json = data;
                var staff_option = "<option value=''>請選擇人員</option>";
                for(i=0;i<staff_json.length;i++){
                    staff_option += "<option value='"+staff_json[i].Staff_ID+"'";
                    if(ID == staff_json[i].Staff_ID)
                        staff_option += " selected";
                    staff_option += ">"+staff_json[i].Staff_Name+"</option>";
                }
                $("#staff_selector").html(staff_option);
                document.getElementById('reservation').style.display = 'none';
                document.getElementById('payment').style.display = 'block';
            },
            error: function(jqXHR) {
                console.log(jqXHR.responseText);
            }
        })
    }

    function display_another_payment(){
        var new_payment = "<table border='1' width='100%' cellspacing='10' style='border-radius:15px;margin-top:25px;margin-bottom:25px;'><tr><td style='font-size:26px;text-align:right;border:0'>收款方式：</td><td style='font-size:26px;border:0'><select class='modified_select new_payment' name='payment_method[]' style='width:250px;height:50px;'><option value=''>請選擇付款方式</option><option value='0'>現金</option><option value='1'>信用卡</option><option value='2'>轉帳</option><option value='3'>其它</option></select></td></tr><tr><td style='font-size:26px;text-align:right;border:0'>收款金額：</td><td style='font-size:26px;border:0'><input type='number' class='new_payment' style='width:250px;height:50px;' id='new_payment_amount' name='payment_amount[]' min='0'></td></tr><tr><td style='font-size:26px;text-align:right;vertical-align:top;border:0'>備註：</td><td style='font-size:26px;border:0'><textarea name='Remark[]' rows='3'></textarea></td></tr></table>";
        document.getElementById("payment_area").innerHTML += new_payment;
    }

    $("body").on("click",".ui-widget-overlay",function() {
        close_dialog();
    });

</script>