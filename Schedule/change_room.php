<?php
    include_once "../mysql_connect.inc.php";
    if(isset($_POST['change_submit'])){
        $sql = "UPDATE `booking_detail` SET `Room_Type`='".$_POST['Room_Type']."',`Room_Num`='".$_POST['Room_Num']."' WHERE `Code`='".$_POST['Code']."'";
        if(!mysqli_query($conn,$sql)){
            echo "This SQL: " . $sql . "<br>";
            die;
        }
        if(!$_POST['Follow_Previous']){
            $sql = "UPDATE `booking_detail` SET `Price`=`Price`+'".$_POST['extra_income']."' WHERE `Code`='".$_POST['Code']."'";
            if(!mysqli_query($conn,$sql)){
                echo "This SQL: " . $sql . "<br>";
                die;
            }
            $sql = "UPDATE `booking_index` SET `Total_Price`=`Total_Price`+'".$_POST['extra_income']."' WHERE `Booking_ID`='".$_POST['Booking_ID']."'";
            if(!mysqli_query($conn,$sql)){
                echo "This SQL: " . $sql . "<br>";
                die;
            }
            $sql = "SELECT `Timezone_Set` FROM `hotel_info` WHERE 1";
            $result = mysqli_query($conn,$sql);
            $row = $result->fetch_assoc();
            date_default_timezone_set($row['Timezone_Set']);
            $Payment_Date = date('Y/m/d H:i:s', time());
            $sql = "INSERT INTO `payment`(`Booking_ID`, `Payment_Type`, `Payment_Method_Num`, `Amount`, `Payment_Remark`, `Staff_ID`, `Payment_Datetime`) VALUES ('".$_POST['Booking_ID']."','1','".$_POST['Payment_Method']."','".$_POST['extra_income']."','換房費用','".$_COOKIE['Staff_ID']."','".$Payment_Date."')";
            if(!mysqli_query($conn,$sql)){
                echo "This SQL: " . $sql . "<br>";
                die;
            }
        }
        echo '<meta http-equiv=REFRESH CONTENT=0;url=index.php>';
        die;
    }
    else{
        if((empty($_POST['Booking_ID']) || !isset($_POST['Booking_ID'])) && (empty($_POST['Room_Status']) || !isset($_POST['Room_Status'])) && (empty($_POST['Room_Num']) || !isset($_POST['Room_Num']))){
            echo '<meta http-equiv=REFRESH CONTENT=0;url=index.php>';
            die;
        }
        $sql = "SELECT * FROM `booking_index`,`booking_detail`,`room_type` WHERE `booking_detail`.`Booking_ID`='".$_POST['Booking_ID']."' AND `booking_detail`.`Room_Num`='".$_POST['Room_Num']."' AND `booking_index`.`Booking_ID`=`booking_detail`.`Booking_ID` AND `room_type`.`Room_Type`=`booking_detail`.`Room_Type`";
        $result = mysqli_query($conn,$sql);
        $row = $result->fetch_assoc();

        $Code = $row['Code'];
        $Booking_ID = $row['Booking_ID'];
        $Booking_Date = $row['Booking_Date'];
        $CIN_Date = $row['CIN_Date'];
        $COUT_Date = $row['COUT_Date'];
        $Room_Type = $row['Room_Type'];
        $Room_Num = $row['Room_Num'];
        $Price = $row['Price'];
        $Tenant = $row['Tenant'];
        $Index_Remark = $row['Index_Remark'];
        $Discount_ID = $row['Discount_ID'];
    }
?>
    
<html>
    <head>
        <script type="text/javascript" src="../functions.js"></script>

        <link rel="stylesheet" href="//code.jquery.com/ui/1.12.1/themes/base/jquery-ui.css">
        <script src="https://code.jquery.com/jquery-1.12.4.js"></script>
        <script src="https://code.jquery.com/ui/1.12.1/jquery-ui.js"></script>
        
        <!-- <script type="text/javascript" src="https://code.jquery.com/jquery-3.3.1.slim.min.js"></script> -->
        <script type="text/javascript" src="https://cdnjs.cloudflare.com/ajax/libs/twitter-bootstrap/4.3.1/js/bootstrap.bundle.min.js"></script>
        <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/twitter-bootstrap/4.3.1/css/bootstrap.min.css">
        <style>
            *{
                font-size: 22px;
            }
            input[type=text],textarea{
                border-radius:10px;
            }
            td{
                padding-top:7.5px;
                border: 0px;
            }
        </style>
    </head>
    <body onload="includeHTML();">
        <div class='navbar-div' include-html="../navbar.html"></div>
        <div class='for_hyper left' include-html="../hyper.html"></div>
        <div class='right'>
            <center>
                <div>
                <form action='' method='POST'>
                    <input type='hidden' name='Code' value='<?php echo $Code;?>'>
                    <input type='hidden' name='Booking_ID' value='<?php echo $Booking_ID;?>'>
                    <table style='width:80%;table-layout:fixed' border='1'>
                        <tr>
                            <td width='30%'><h2>換房作業</h2></td>
                            <td width='20%'></td>
                            <td width='50%'></td>
                        </tr>
                        
                        <tr>
                            <td style='text-align:center'>
                                訂房日期：<?php echo str_replace("-", "/",explode(" ",$Booking_Date)[0]); ?>
                            </td>
                            <td style='text-align:right'>
                                原房型/房間
                            </td>
                            <td style='text-align:center'>
                                <input type='text' style='width:40%;text-align:center' value='<?php echo $Room_Type;?>' readonly>
                                <input type='text' style='width:20%;margin-left:20px;text-align:center' value='<?php echo $Room_Num;?>' readonly>
                                <input type='hidden' id='CIN_Date' value='<?php echo $CIN_Date;?>' readonly>
                                <input type='hidden' id='COUT_Date' value='<?php echo $COUT_Date;?>' readonly>
                                <input type='hidden' id='Discount_ID' value='<?php echo $Discount_ID;?>' readonly>
                            </td>
                        </tr>

                        <tr>
                            <td style='text-align:center'>
                                入住日期：<?php echo str_replace("-", "/",explode(" ",$CIN_Date)[0]); ?>
                            </td>
                            <td style='text-align:right'>
                                更換為
                            </td>
                            <td style='text-align:center'>
                                <select id='Room_Type' name='Room_Type' onchange="room_changed(this.value)" required>
                                </select>
                                <select id='Room_Num' name='Room_Num' required>
                                    <option value=''>選擇房號</option>
                                </select>
                            </td>
                        </tr>

                        <tr>
                            <td style='text-align:center'>
                                退房日期：<?php echo str_replace("-", "/",explode(" ",$COUT_Date)[0]); ?>
                            </td>
                            <td style='text-align:right'>
                                保留原始房價
                            </td>
                            <td style='text-align:center'>
                                <label><input name='Follow_Previous' type='radio' value='1' onchange='input_payment(this.value)' checked>是</label>
                                <label><input name='Follow_Previous' type='radio' value='0' onchange='input_payment(this.value)'>否</label>
                                <input type='number' name='extra_income' id='extra_income' style='width:50%' min='1' placeholder='輸入收款金額' onkeyup='extra_income_cal(this.value)' onchange='extra_income_cal(this.value)' readonly>
                                <input type='hidden' id='original' value='<?php echo $Price?>'>
                                <select name='Payment_Method'>
                                    <option value='0'>現金</option>
                                    <option value='1'>信用卡</option>
                                    <option value='2'>轉帳</option>
                                    <option value='3'>其它</option>
                                </select>
                            </td>
                        </tr>

                        <tr>
                            <td style='text-align:center'>
                                入住人數：<span id='Tenant'><?php echo $Tenant;?>人</span>
                            </td>
                            <td colspan='2'>
                                <center>
                                    <table width='95%'></tr>
                                        <td><span>原始總價：$<span id='Original_Price'><?php echo $Price;?></span></span></td>
                                        <td><span>新房總價：<span id='Price'>$0</span></span></td>
                                        <td><span>最終價格：<span id='End_Price'>$<?php echo $Price;?></span></span></td>
                                    </tr></table>
                                </center>
                            </td>
                        </tr>
                        
                        <tr>
                            <td colspan='3' style='text-align:center'>
                                <textarea style='width:95%' rows='5' readonly><?php echo str_replace("<br />", chr(13).chr(10),nl2br($Index_Remark));?></textarea>
                            </td>
                        </tr>

                        <tr>
                            <td style='text-align:right' colspan='3'>
                                <button class='function_btn' style='background-color:#969696' onclick='goback()'>取消換房</button>
                                <button type='submit' name='change_submit' class='function_btn' style='background-color:#0091FF;margin-left:30px;'>確定</button>
                            </td>
                        </tr>

                    </table>
                </form>
                </div>
            </center>
        </div>
    </body>
</html>

<script>
    var room_array = new Array();

    $(document).ready(function() {
        $.ajax({
            type: "POST",
            url: "service.php",
            dataType: "json",
            data: {
                Feature: "get_available_rooms",
                CIN_Date: $("#CIN_Date").val(),
                COUT_Date: $("#COUT_Date").val(),
                Discount_ID: $("#Discount_ID").val(),
            },
            success: function(data) {
                room_array = data;
                room_json = data;
                var room_type_option = "<option value=''>選擇房型</option>";
                for(i=0;i<room_json.length;i++){
                    room_type_option += "<option value='"+room_json[i].room_type+"'>"+room_json[i].room_type+"</option>";                    
                }
                // for(var room_type in room_json){
                //     room_type_option += "<option value='"+room_type+"'>"+room_type+"</option>";
                // }
                $("#Room_Type").html(room_type_option);
            },
            error: function(jqXHR) {
                console.log(jqXHR.responseText);
            }
        })
    });

    function room_changed(Room_Type){
        var room_num_option = "<option value=''>選擇房號</option>";
        var i = 0;
        for(;i<room_array.length;i++){
            if(room_array[i].room_type==Room_Type){
                for(j=0;j<room_array[i].room_num.length;j++){
                    room_num_option += "<option value='"+room_array[i].room_num[j]+"'>"+room_array[i].room_num[j]+"</option>";
                }
                break;
            }
        }
        $("#Room_Num").html(room_num_option);
        $("#Price").html("$" + room_array[i].Total_Price);
        $("#Tenant").html(room_array[i].Tenant + "人");
    }

    function input_payment(situ){
        switch(situ){
            case '0':
                $("#extra_income").attr('readonly', false);
                $("#extra_income").attr('required', true);
                $("#extra_income").val("");
                $("#End_Price").html("$" + parseInt($("#Original_Price").html()));
            break;

            case '1':
                $("#extra_income").attr('readonly', true);
                $("#extra_income").attr('required', false);
                $("#extra_income").val("");
                $("#End_Price").html("$" + parseInt($("#Original_Price").html()));
            break;
        }
    }

    function extra_income_cal(x_in){
        var original = parseInt($("#Original_Price").html());
        $("#End_Price").html("$" + (parseInt(x_in) + original));
    }
    
    function goback(){
        window.location = "index.php";
    }
</script>