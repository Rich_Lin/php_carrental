<html>
    <head>
        <script src="../js/jquery-1.12.4.js"></script>
        <script src="../js/moment.min.js"></script>
        <script src="../js/lightpick.js"></script>
        <link rel="stylesheet" type="text/css" href="../js/lightpick.css">

        <style>
            .function_btn{
                width:130px;
                height:50px;
                border-radius:15px;
            }
            .dropdown{
                position: relative;
                display: inline-block;
            }
            .dropdown-content{
                text-align: left;
                display: none;
                position: absolute;
                background-color: WHITE;
                width: 430px;
                box-shadow: 0px 8px 16px 0px rgba(0,0,0,0.2);
                z-index: 1;
            }
            .dropdown-content td{
                color: black;
                font-size: 26px;
                padding-left: 30px;
                text-decoration: none;
                height: 80px;
                vertical-align: center;
            }
            .dropdown-content p:hover{
                background-color: #ddd;
            }
            .dropdown:hover .dropdown-content{
                display: block;
            }
            .dropdown:hover .dropbtn{
                background-color: #3e8e41;
            }
            .collapsible, .sub_collapsible {
                background-color: #777;
                color: white;
                cursor: pointer;
                padding: 18px;
                width: 85%;
                border: none;
                text-align: left;
                outline: none;
                font-size: 15px;
                border: 1px solid BLACK;
            }
            .collapsible:hover, .sub_collapsible:hover {
                background-color: #555;
            }
            .content {
                padding: 0 18px;
                display: none;
                overflow: hidden;
                /* background-color: #f1f1f1; */
            }
            .date_select{
                display: none;
            }
            .active{
                display: contents;
            }
            input[type='text'], input[type='number'], select{
                font-size: 1.25vw;
                border-radius: 7.5px;
                text-align: center;
                border: solid 3px #696969;
            }
            select{
                width: 100px;
                height: 50px;
            }
        </style>
    </head>

    <body>
        <!-- <button style='font-size: 1.5vw' onclick="go_index()">回首頁</button> -->
        <center>
            <button style='font-size: 1.5vw;float:right' onclick="go_cartype()">檢視車型</button>
            <button style='font-size: 1.5vw;float:right' onclick="go_carlist()">檢視車輛</button>

            <br>

            <h1>價格計算</h1>
            <input type="text" id="ajax" list="json-datalist" placeholder="請輸入車型" onchange='display_car_info(this.value)'>
            <datalist id="json-datalist"></datalist>

            <div>
                <table id='car_info' border='1'>
                </table>
            </div>

            <div style='display:inline-block'>
                <input type='text' id='Start'><br>
                <input type="time" id="Start_Time" value='09:00'>
            </div>
            <div style='display:inline-block'>
                <input type='text' id='End'><br>
                <input type="time" id="End_Time" value='09:00'>
            </div>
            <br>
            <button onclick='calculate_all_info()'>確定</button>
            <div>
                <p>總天數：<span id='total_days'></span></p>
                <p>平日天數：<span id='total_weekdays'></span></p>
                <p>假日天數：<span id='total_weekends'></span></p>
                <p>小時差：<span id='hour_diff'></span></p>
                <p>優惠方案：<span id='special_offer'>天</span> 折扣：<span id='discount'></span>元</p>
                <p>價格：<span id='total_price'></span></p>
                <p>超時費用：<span id='extra_hour_fee'></span></p>
                <p>最終總價：<span id='end_price'></span></p>
            </div>

        </center>
    </body>
</html>

<script>
    var car_type;
    var dataList = document.getElementById('json-datalist');
    var input = document.getElementById('ajax');
    $.ajax({
        type: "POST",
        url: "service.php",
        data: {
            Feature: "get_car_type"
        },
        success: function(data) {
            car_type = data;
            data.forEach(function(item) {
                var option = document.createElement('option');
                option.value = item.Car_Type;
                dataList.appendChild(option);
            });
            input.placeholder = "請輸入車型";
        },
        error: function(jqXHR) {
            alert(jqXHR.responseText);
            console.log(jqXHR.responseText);
        }
    })
    input.placeholder = "載入車輛資料中...";

    var picker = new Lightpick({
        field: document.getElementById('Start'),
        secondField: document.getElementById('End'),
        startDate: moment().startOf('day'),
        endDate: moment().startOf('day'),
        // Day count can be disabled at line 719, for this case, days are minus 1 for purpose.
        singleDate: false,
        repick: true,
        // onSelect: function(date){
        //     var tmp;
        //     if(document.getElementById('End').value != '' && document.getElementById('Start').value > document.getElementById('End').value){
        //         tmp = document.getElementById('End').value;
        //         document.getElementById('End').value = document.getElementById('Start').value;
        //         document.getElementById('Start').value = tmp;
        //     }
        // }
    });

    function display_car_info(Car_Type){

        var th = "<tr><th>車型</th><th>平日</th><th>假日</th><th>平日半天</th><th>優惠半天</th><th>超收(小時)</th><th>週租</th><th>月租</th><th>備註</th></tr>";
        var info = '';
        var flag = 0;
        for(var i=0;i<car_type.length;i++)
            if(car_type[i].Car_Type==Car_Type){
                flag = 1;
                info = "<tr><td>"+car_type[i].Car_Type+"</td><td>"+car_type[i].Weekday_Price+"</td><td>"+car_type[i].Weekend_Price+"</td><td>"+car_type[i].Halfday_Price+"</td><td>"+car_type[i].Weekday_Price/2+"</td><td>"+car_type[i].Weekday_Price/10+"</td><td>"+car_type[i].Weekly+"</td><td>"+car_type[i].Monthly+"</td><td>"+car_type[i].Remark+"</td></tr>";
            }
        if(flag==0)
            $("#car_info").html("");
        else
            $("#car_info").html(th + info);

    }

    function calculate_all_info(){
        let Start = moment($("#Start").val() + " " + $("#Start_Time").val() + ":00");
        let End = moment($("#End").val() + " " + $("#End_Time").val() + ":00");
        let car_info = $("#ajax").val();
        let Origin_Start = moment($("#Start").val());
        for(i=0;i<car_type.length;i++){
            if(car_type[i].Car_Type==car_info){
                car_info = car_type[i];
                car_info.Special_Half = car_info.Weekday_Price/2;
                car_info.Extra_Hour = Math.ceil(car_info.Weekday_Price/10);
                car_info.Weekday_Price = parseInt(car_info.Weekday_Price);
                car_info.Weekend_Price = parseInt(car_info.Weekend_Price);
                car_info.Halfday_Price = parseInt(car_info.Halfday_Price);
                car_info.Weekly = parseInt(car_info.Weekly);
                car_info.Monthly = parseInt(car_info.Monthly);
                car_info.Diff = parseInt(car_info.Weekend_Price - car_info.Weekday_Price);
                break;
            }
        }

        if(car_info==""){
            alert("請先選擇車型");
            return;
        }
        if(!Start.diff(End,'minutes') && $("#Start_Time").val()>=$("#End_Time").val()){
            alert("請調整日期或時間");
            return;
        }
        
        let total_days = Math.abs(End.diff(Start,'hours')/24);
        let time_diff = End.diff(Start,'hours')%24;
        let weekday = 0;
        let weekend = 0;
        let total_price = 0;
        let special_offer = 0;
        let discount = 0;
        let extra_hour_fee = 0;
        let Special_Remark = "";

        if(Math.floor(total_days)==0){
            if(time_diff<=12 && Start.format('d')>0 && Start.format('d')<6){
                total_price += parseInt(car_info.Halfday_Price);
                total_days = 0;
            }
            else
                if(Start.format('d')>0 && Start.format('d')<6){
                    total_days = 1;
                    total_price += parseInt(car_info.Weekday_Price);
                }
                else{
                    total_days = 1;
                    total_price += parseInt(car_info.Weekend_Price);
                }
        }
        else if(total_days<6){
            total_price += Math.floor(total_days) * car_info.Weekday_Price;
            if(time_diff<=5){
                total_price += Math.ceil(time_diff) * car_info.Extra_Hour;
                total_days = Math.floor(total_days);
            }
            else if(time_diff<=12){
                total_price += car_info.Special_Half;
                total_days = Math.floor(total_days) + 0.5;
            }
            else{
                total_price = Math.ceil(total_days) * car_info.Weekday_Price;
                total_days = Math.floor(total_days) + 1;
            }

            special_offer = total_days;
            for(i=0;i<Math.floor(total_days);i++){
                switch(parseInt(Start.format('d'))){
                    case 1:
                    case 2:
                    case 3:
                    case 4:
                        weekday++;
                    break;

                    case 5:
                        if((parseInt($("#Start_Time").val().split(":")[0]*60) + parseInt($("#Start_Time").val().split(":")[1]))>720){
                            //consider as weekend
                            total_price += car_info.Diff;
                            weekend++;
                        }
                        else{
                            weekday++;
                        }
                    break;

                    case 6:
                        total_price += car_info.Diff;
                        weekend++;
                    break;

                    case 0:
                        if((parseInt($("#Start_Time").val().split(":")[0]*60) + parseInt($("#Start_Time").val().split(":")[1]))>720){
                            //consider as weekday
                            weekday++;
                        }
                        else{
                            total_price += car_info.Diff;
                            weekend++;
                        }
                    break;
                }
                Start.add("1","days");
            }
            if(total_days%1>0){
                switch(parseInt(End.format('d'))){
                    case 6:
                    case 0:
                        weekday--;
                        weekend++;
                        total_price += car_info.Diff;
                    break;
                }
            }
        }
        else if(total_days<30){
            if(total_days%1!=0){
                Special_Remark = "，自動進位一天，共" + Math.ceil(total_days) + "天";
            }
            total_price += Math.ceil(total_days)*parseInt(car_info.Weekly);
        }
        else{
            total_price += Math.ceil(total_days)*parseInt(car_info.Monthly);
            if(total_days%1!=0){
                Special_Remark = "，自動進位一天，共" + Math.ceil(total_days) + "天";
            }
        }
        if(special_offer%1>0){
            special_offer=Math.floor(total_days) - 1;
        }
        switch(special_offer){
            case 1:
                discount = 100;
            break;
            
            case 2:
                discount = 300;
            break;
            
            case 3:
                discount = 600;
            break;
            
            case 4:
                discount = 900;
            break;
            
            case 5:
                discount = 1200;
            break;

            default:
                special_offer = 0;
                discount = 0;
        }

        $('#total_days').html(total_days + "天" + Special_Remark);
        $('#total_weekdays').html(weekday + "天");
        $('#total_weekends').html(weekend + "天");
        $('#hour_diff').html(time_diff + "小時");
        $('#special_offer').html(special_offer);
        $('#discount').html("$" + discount);
        $('#total_price').html("$" + (total_price - discount));
        $('#extra_hour_fee').html(extra_hour_fee);
        $('#end_price').html((total_price - discount + extra_hour_fee));

    }

    function go_index(){
        window.location = "index.php";
    }

    function go_carlist(){
        window.location = "car_list.php";
    }

    function go_cartype(){
        window.location = "car_type.php";
    }

</script>