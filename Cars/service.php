<?php
    include_once "../mysql_connect.inc.php";
    header('Content-Type: application/json; charset=UTF-8');
    // error_reporting(0);
    /////////////////////////////////////////////////////
    
    if($_SERVER['REQUEST_METHOD'] == "POST") {
      if(isset($_POST)){
        switch($_POST['Feature']){
          case "get_car_type":
              $car_type = array();
              $sql = "SELECT * FROM `car_type` ORDER BY `Car_Type`";
              $result = mysqli_query($conn,$sql);
              while($row = $result->fetch_assoc()){
                  $car_type[] = $row;
              }
              echo json_encode($car_type,JSON_UNESCAPED_UNICODE);
          break;
          
          case "AoU_Car":
            if(empty($_POST['Car_ID'])){
              $sql = "INSERT INTO `car_type`(`Car_Type`, `Weekday_Price`, `Weekend_Price`, `Halfday_Price`, `Weekly`, `Monthly`, `Remark`) VALUES ('".$_POST['Car_Type']."','".$_POST['Weekday_Price']."','".$_POST['Weekend_Price']."','".$_POST['Halfday_Price']."','".$_POST['Weekly']."','".$_POST['Monthly']."','".$_POST['Remark']."')";
            }
            else{
              $sql = "UPDATE `car_type` SET `Car_Type`='".$_POST['Car_Type']."',`Weekday_Price`='".$_POST['Weekday_Price']."',`Weekend_Price`='".$_POST['Weekend_Price']."',`Halfday_Price`='".$_POST['Halfday_Price']."',`Weekly`='".$_POST['Weekly']."',`Monthly`='".$_POST['Monthly']."',`Remark`='".$_POST['Remark']."' WHERE `Car_ID`='".$_POST['Car_ID']."'";
            }
            if(!mysqli_query($conn,$sql)){
              echo "資料庫錯誤！請盡速與阿嵐聯絡>.0";
              die;
            }
            echo true;
          break;
        
          case "get_specific_car_info":
            $sql = "SELECT * FROM `car_type` WHERE `Car_ID`='".$_POST['Car_ID']."'";
            echo json_encode(mysqli_query($conn,$sql)->fetch_assoc(),JSON_UNESCAPED_UNICODE);
          break;
        
          case "delete_car":
            if(empty($_POST['Car_ID'])){
              echo "傳入空值！請聯絡阿嵐！";
              die;
            }
            $sql = "DELETE FROM `car_type` WHERE `Car_ID`='".$_POST['Car_ID']."'";
            if(!mysqli_query($conn,$sql)){
              echo "資料庫錯誤！請盡速與阿嵐聯絡>.0";
              die;
            }
            echo true;
          break;

          case "get_car_list":
            $car_list = array();
            $sql = "SELECT * FROM `car_list` ORDER BY `Car_Type`";
            $result = mysqli_query($conn,$sql);
            while($row = $result->fetch_assoc()){
                $car_list[] = $row;
            }
            echo json_encode($car_list,JSON_UNESCAPED_UNICODE);
          break;
        
          case "get_specific_carlist_info":
            $sql = "SELECT * FROM `car_list` WHERE `License_Plate`='".$_POST['License_Plate']."'";
            echo json_encode(mysqli_query($conn,$sql)->fetch_assoc(),JSON_UNESCAPED_UNICODE);
          break;

          case "upload_car_list_csv":
            // print_r($_FILES['CSV_File']);die;
            $csv = $_FILES['CSV_File']['tmp_name'];
            $csv = array_map('str_getcsv', file($csv));
            foreach($csv as $key => $value){
                if($key!=0 && !empty($value[3]) && !empty($value[4])){
                    // Insert Company Info
                    if(!empty($value[0])){
                        $sql = "SELECT * FROM `company_list` WHERE `Company_Name`='".$value[0]."'";
                        $result = mysqli_query($conn,$sql);
                        if(mysqli_num_rows($result)==0){
                            $sql = "INSERT INTO `company_list`(`Company_Name`, `Uniform_Number`) VALUES ('".$value[0]."','".$value[1]."')";
                            if(!mysqli_query($conn,$sql)){
                              echo "資料輸入錯誤！請盡速與阿嵐聯絡>.0<br>Error Code: C_I_CoL_0";
                            }
                        }
                    }
                    // Insert Car Type
                    $sql = "SELECT * FROM `car_type` WHERE `Car_Type`='".$value[4]."'";
                    $result = mysqli_query($conn,$sql);
                    if(mysqli_num_rows($result)==0){
                        $sql = "INSERT INTO `car_type`(`Car_Type`, `Weekday_Price`, `Weekend_Price`, `Halfday_Price`, `Weekly`, `Monthly`) VALUES ('".$value[4]."','".$value[9]."','".$value[8]."','".$value[10]."','".$value[11]."','".$value[12]."')";
                        if(!mysqli_query($conn,$sql)){
                          echo "資料輸入錯誤！請盡速與阿嵐聯絡>.0<br>Error Code: C_I_CT";
                        }
                    }
                    // Insert Car List
                    $sql = "SELECT * FROM `car_list` WHERE `License_Plate`='".$value[3]."'";
                    $result = mysqli_query($conn,$sql);
                    if(mysqli_num_rows($result)==0){
                        $sql = "INSERT INTO `car_list`(`Company_Name`, `Vehicle_Inspection`, `License_Plate`, `Car_Type`, `Displacement`, `Manufacturing_Date`, `Color`, `ISOFIX`, `Equipment`, `Car_Style`, `Model`, `Engine_Number`, `Body_Number`, `Registration`) VALUES ('".$value[0]."','".$value[2]."','".$value[3]."','".$value[4]."','".$value[5]."','".$value[6]."','".$value[7]."','".$value[13]."','".$value[14]."','".$value[15]."','".$value[16]."','".$value[17]."','".$value[18]."','".$value[19]."')";
                        if(!mysqli_query($conn,$sql)){
                          echo "資料輸入錯誤！請盡速與阿嵐聯絡>.0<br>Error Code: C_I_CL";
                        }
                    }
                    else{
                        $sql = "UPDATE `car_list` SET `Company_Name`='".$value[0]."',`Vehicle_Inspection`='".$value[2]."',`Car_Type`='".$value[4]."',`Displacement`='".$value[5]."',`Manufacturing_Date`='".$value[6]."',`Color`='".$value[7]."',`ISOFIX`='".$value[13]."',`Equipment`='".$value[14]."',`Car_Style`='".$value[15]."',`Model`='".$value[16]."',`Engine_Number`='".$value[17]."',`Body_Number`='".$value[18]."',`Registration`='".$value[19]."' WHERE `License_Plate`='".$value[3]."'";
                        if(!mysqli_query($conn,$sql)){
                          echo "資料輸入錯誤！請盡速與阿嵐聯絡>.0<br>Error Code: C_U_CL";
                        }
                    }
                }
            }
            echo true;
          break;
        }
      }
    }
    else{
      echo '<meta http-equiv=REFRESH CONTENT=0;url=index.php>';
      die;
    }
?>