<html>
    <head>
        <script src="../js/jquery-1.12.4.js"></script>
        <link rel="stylesheet" href="//code.jquery.com/ui/1.12.1/themes/base/jquery-ui.css">
        <script src="https://code.jquery.com/ui/1.12.1/jquery-ui.js"></script>

        <style>
            *{
                font-size:1.5vw;
            }
            th,td{
                padding: 5px 5px;
            }
            .function_btn{
                text-align: center;
                padding: 0px 5%;
                margin: 5px 5px;
                border-radius:15px;
                height: 50px;
            }
            .ui-dialog-titlebar{
                display: none
            }
            .ui-widget, .ui-widget-content, .ui-dialog-content{
                border-radius: 20px;
                /* border-width: 20px; */
                /* background-color: #DADADA;
                border: 1px solid #DADADA; */
            }
            .ui-widget-content {
                /* border-radius: 20px; */
                /* border-width: 20px; */
            }
            .dialog_section{
                display: none;
            }
            .active{
                display: contents;
            }
            #container_table tr:hover{
                background-color: #ffff99;
            }
        </style>
    </head>

    <body onload="get_car_list()">
        <button style='font-size: 1.5vw' onclick="go_index()">回首頁</button>
        <center>
            <button style='font-size: 1.5vw;float:right' onclick="go_cartype()">檢視車型</button>
            <!-- <button style='font-size: 1.5vw;float:right' onclick="go_carlist()">檢視車輛</button> -->

            <div>
                <h1>車輛管理</h1>
                <!-- <input class='function_btn' style='display:block;background-color:#0091FF;color:white;float:right;margin-bottom:20px;' type='button' id='Add_Staff' value='新增車輛'> -->
            </div>
            <div id='container' style='display:block;width:90%;height:85%;overflow:auto'>
            </div>
        </center>
    </body>
</html>

<div id="dialog">
    <br>
    <center>
        <table style='width:80%'>
            <tr>
                <td style='width:50%;text-align:center'><label><input type='radio' name='select_section' value='import_csv' onchange='change_section(this.value)'>上傳CSV</label></td>
                <td style='text-align:center'><label><input type='radio' name='select_section' value='add_single' onchange='change_section(this.value)' disabled>單個新增</label></td>
            </tr>
        </table>
        <div class='dialog_section' id='import_csv'>
            <form id='import_form' action='service.php'>
                <input type='file' accept=".csv" name='CSV_File' id='CSV_File'>
                <button type='submit' class='function_btn' style='background-color:#0091FF;color:WHITE'>匯入</button>
            </form>
        </div>
        <div class='dialog_section' id='add_single'>
            <!-- <form id='myform' action='service.php'>
                <input type='hidden' name='Car_ID' id='Car_ID'>
                <input type='hidden' name='Feature' value='AoU_Car'>
                <table width='95%'>
                    <tr>
                        <td style='width:20%;text-align:right'></td>
                        <td style='width:30%'></td>
                        <td style='width:20%;text-align:right'></td>
                        <td></td>
                    </tr>
                    <tr>
                        <td style='font-size:36px;' colspan='4'>新增/編輯車型：</td>
                    </tr>
                    <tr>
                        <td style='text-align:right'>車型名稱：</td>
                        <td style=''><input type='text' style='width:100%;' name='Car_Type' id='Car_Type' required></td>
                        <td style='text-align:right' colspan='2'></td>
                    </tr>
                    <tr>
                        <td style='text-align:right'>平日價格：</td>
                        <td style=''><input type='number' style='width:100%;' name='Weekday_Price' id='Weekday_Price' required></td>
                        <td style='text-align:right'>半日價格：</td>
                        <td style=''><input type='number' style='width:100%;' name='Halfday_Price' id='Halfday_Price' required></td>
                        </td>
                    </tr>
                    <tr>
                        <td style='text-align:right'>假日價格：</td>
                        <td style=''><input type='text' style='width:100%;' name='Weekend_Price' id='Weekend_Price' required></td>
                        <td style='text-align:right' colspan='2'></td>
                    </tr>
                    <tr>
                        <td style='text-align:right'>周租價格：</td>
                        <td><input type='number' style='width:100%;' name='Weekly' id='Weekly'></td>
                        <td style='text-align:right'>月租價格：</td>
                        <td><input type='number' style='width:100%;' name='Monthly' id='Monthly'></td>
                    </tr>
                    <tr>
                        <td style='text-align:right'>備註：</td>
                        <td colspan="3"><input type='text' style='width:100%;' name='Remark' id='Remark'></td>
                    </tr>
                    <tr>
                        <td style='text-align:center;' colspan="4"><button type='submit' class='function_btn' style='background-color:#0091FF;color:WHITE'>確定</button></td>
                    </tr>
                </table>
            </form> -->
        </div>
    </center>
</div>

<div id="Show_Info_dialog">
    <br>
    <center>
        <input type='hidden' name='Car_ID' id='Car_ID'>
        <input type='hidden' name='Feature' value='AoU_Car'>
        <table width='95%'>
            <tr>
                <td style='width:20%;text-align:right'></td>
                <td style='width:30%'></td>
                <td style='width:20%;text-align:right'></td>
                <td></td>
            </tr>
            <tr>
                <td style='font-size:36px;' colspan='4'>檢視車輛：</td>
            </tr>
            <tr>
                <td style='text-align:right'>公司名稱：</td>
                <td style=''><input type='text' style='width:100%;' id='Company_Name' readonly></td>
                <td style='text-align:right' colspan='2'></td>
            </tr>
            <tr>
                <td style='text-align:right'>車款：</td>
                <td style=''><input type='text' style='width:100%;' id='Car_Type' readonly></td>
                <td style='text-align:right'>車號：</td>
                <td style=''><input type='text' style='width:100%;' id='License_Plate' readonly></td>
                </td>
            </tr>
            <tr>
                <td style='text-align:right'>驗車：</td>
                <td style=''><input type='text' style='width:100%;' id='Vehicle_Inspection' readonly></td>
                <td style='text-align:right'>排氣量：</td>
                <td style=''><input type='text' style='width:100%;' id='Displacement' readonly></td>
                </td>
            </tr>
            <tr>
                <td style='text-align:right'>出廠年月：</td>
                <td style=''><input type='text' style='width:100%;' id='Manufacturing_Date' readonly></td>
                <td style='text-align:right'>車色：</td>
                <td style=''><input type='text' style='width:100%;' id='Color' readonly></td>
                </td>
            </tr>
            <tr>
                <td style='text-align:right'>ISOFIX：</td>
                <td style=''><input type='text' style='width:100%;' id='ISOFIX' readonly></td>
                <td style='text-align:right' colspan='2'></td>
            </tr>
            <tr>
                <td style='text-align:right'>配備：</td>
                <td style='' colspan='3'><input type='text' style='width:100%;' id='Equipment' readonly></td>
            </tr>
            <tr>
                <td style='text-align:right'>車型：</td>
                <td style=''><input type='text' style='width:100%;' id='Car_Style' readonly></td>
                <td style='text-align:right'>型式：</td>
                <td style=''><input type='text' style='width:100%;' id='Model' readonly></td>
                </td>
            </tr>
            <tr>
                <td style='text-align:right'>引擎號碼：</td>
                <td style=''><input type='text' style='width:100%;' id='Engine_Number' readonly></td>
                <td style='text-align:right'>車身號碼：</td>
                <td style=''><input type='text' style='width:100%;' id='Body_Number' readonly></td>
                </td>
            </tr>
            <tr>
                <td style='text-align:center;' colspan="4"><button type='submit' class='function_btn' style='background-color:#0091FF;color:WHITE' onclick='close_dialog()'>確定</button></td>
            </tr>
        </table>
    </center>
</div>

<script>

    var car_list;

    $(document).ready(function() {
        $("#dialog").dialog({
            height: 350,
            width: 800,
            autoOpen: false,
            resizable: false,
            draggable: false,
            modal: true
            // dialogClass: 'ui-dialog-osx',
        });

        $("#Show_Info_dialog").dialog({
            height: 600,
            width: 1200,
            autoOpen: false,
            resizable: false,
            draggable: false,
            modal: true
            // dialogClass: 'ui-dialog-osx',
        });

        $("#Add_Staff").click(function(){
            $("#Staff_ID").val("");
            $( "#dialog" ).dialog( "open" );
        });

        $("#import_form").submit(function(){
            event.preventDefault();
            let formData = new FormData();
            formData.append('Feature', "upload_car_list_csv");
            formData.append('CSV_File', $('#CSV_File').prop('files')[0]);
            $.ajax({
                type: "POST",
                url: "service.php",
                data: formData,
                processData : false,
                contentType : false,
                success: function(data) {
                    close_dialog();
                    get_car_list();
                },
                error: function(jqXHR) {
                    // alert(jqXHR.responseText);
                    console.log(jqXHR.responseText);
                    get_car_list();
                }
            })
        })

        $("#myform").submit(function(){
            event.preventDefault();
            $.ajax({
                type: "POST",
                url: "service.php",
                data: $("#myform").serialize(),
                success: function(data) {
                    close_dialog();
                    get_all_car();
                },
                error: function(jqXHR) {
                    alert(jqXHR.responseText);
                    console.log(jqXHR.responseText);
                    get_all_car();
                }
            })
        })
    });

    $("body").on("click",".ui-widget-overlay",function() {
        close_dialog();
    });

    function close_dialog(){
        $("#Car_ID").val("");
        $('#myform').trigger("reset");
        $('#dialog').dialog( "close" );
        $('#Show_Info_dialog').dialog( "close" );
    }

    function Edit_Car(License_Plate){
        $.ajax({
            type: "POST",
            url: "service.php",
            dataType: "json",
            data: {
                Feature: "get_specific_carlist_info",
                License_Plate: License_Plate
            },
            success: function(data) {
                $("#Company_Name").val(data.Company_Name);
                $("#Car_Type").val(data.Car_Type);
                $("#License_Plate").val(data.License_Plate);
                $("#Vehicle_Inspection").val(data.Vehicle_Inspection);
                $("#Displacement").val(data.Displacement);
                $("#Manufacturing_Date").val(data.Manufacturing_Date);
                $("#Color").val(data.Color);
                $("#ISOFIX").val(data.ISOFIX);
                $("#Equipment").val(data.Equipment);
                $("#Car_Style").val(data.Car_Style);
                $("#Model").val(data.Model);
                $("#Engine_Number").val(data.Engine_Number);
                $("#Body_Number").val(data.Body_Number);
                $("#Registration").val(data.Registration);

                $( "#Show_Info_dialog" ).dialog( "open" );
            },
            error: function(jqXHR) {
                // $("#result").html('<font color="#ff0000">發生錯誤：GGGGGGGGGGGGGGGG' + jqXHR.responseText + '</font>');
                console.log(jqXHR.responseText);

            }
        })
    }
    
    function Delete_Car(Car_ID){
        $.ajax({
            type: "POST",
            url: "service.php",
            dataType: "json",
            data: {
                Feature: "delete_car",
                Car_ID: Car_ID
            },
            success: function(data) {
                get_all_car();
            },
            error: function(jqXHR) {
                // $("#result").html('<font color="#ff0000">發生錯誤：GGGGGGGGGGGGGGGG' + jqXHR.responseText + '</font>');
                console.log(jqXHR.responseText);

            }
        })
    }

    function get_car_list(){
        $.ajax({
            type: "POST",
            url: "service.php",
            dataType: "json",
            data: {
                Feature: "get_car_list",
            },
            success: function(data) {
                $("#container").html("<table id='container_table' style='width:95%;border-collapse:collapse;' border='1'><tr style='background-color:#6236FF;color:white'><th style='text-align:center;white-space:nowrap'>公司名稱</th><th style='text-align:center;white-space:nowrap'>車款</th><th style='text-align:center;white-space:nowrap'>車號</th><th style='text-align:center;white-space:nowrap'>驗車</th><th style='text-align:center;white-space:nowrap'>排氣量</th><th style='text-align:center;white-space:nowrap'>出廠年月</th><th style='text-align:center;white-space:nowrap'>車色</th><th style='text-align:center;white-space:nowrap'>ISOFIX</th><th style='text-align:center;white-space:nowrap'>配備</th><th style='text-align:center;white-space:nowrap'>車型</th><th style='text-align:center;white-space:nowrap'>型式</th><th style='text-align:center;white-space:nowrap'>引擎號碼</th><th style='text-align:center;white-space:nowrap'>車身號碼</th><th style='text-align:center;white-space:nowrap'>行照</th><!--<th></th>--></tr>");
                car_list = data;
                for(i=0;i<car_list.length;i++){
                    var row='';
                    var Company_Name = car_list[i].Company_Name;
                    var Car_Type = car_list[i].Car_Type;
                    var License_Plate = car_list[i].License_Plate;
                    var Vehicle_Inspection = car_list[i].Vehicle_Inspection;
                    var Displacement = car_list[i].Displacement;
                    var Manufacturing_Date = car_list[i].Manufacturing_Date;
                    var Color = car_list[i].Color;
                    var ISOFIX = car_list[i].ISOFIX;
                    var Equipment = car_list[i].Equipment;
                    var Car_Style = car_list[i].Car_Style;
                    var Model = car_list[i].Model;
                    var Engine_Number = car_list[i].Engine_Number;
                    var Body_Number = car_list[i].Body_Number;
                    var Registration = car_list[i].Registration;
                    row += "<tr onclick='Edit_Car(\""+License_Plate+"\")'><td style='white-space:nowrap'>"+Company_Name+"</td><td style='white-space:nowrap'>"+Car_Type+"</td><td style='text-align:center;white-space:nowrap'>"+License_Plate+"</td><td style='text-align:center;white-space:nowrap'>"+Vehicle_Inspection+"</td><td style='text-align:center;white-space:nowrap'>"+Displacement+"</td><td style='text-align:center;white-space:nowrap'>"+Manufacturing_Date+"</td><td style='text-align:center;white-space:nowrap'>"+Color+"</td><td style='text-align:center;white-space:nowrap'>"+ISOFIX+"</td><td style='text-align:center;white-space:nowrap'>"+Equipment+"</td><td style='white-space:nowrap'>"+Car_Style+"</td><td style='text-align:center;white-space:nowrap'>"+Model+"</td><td style='text-align:center;white-space:nowrap'>"+Engine_Number+"</td><td style='text-align:center;white-space:nowrap'>"+Body_Number+"</td><td style='text-align:center;white-space:nowrap'>"+/*Registration+*/"</td><!--<td style='white-space:nowrap'><button class='function_btn' style='background-color:WHITE;color:BLACK;float:left;display:inline-block;padding:' value='"+License_Plate+"' onClick='Edit_Car(this.value)' disabled>編輯</button><button class='function_btn' style='background-color:#E02020;color:WHITE;float:right;display:inline-block' value='"+License_Plate+"' onClick='Delete_Car(this.value)' disabled>刪除</button></td>--></tr>";
                    $("#container_table").append(row);
                }
                $("#container").append("</table>");
            },
            error: function(jqXHR) {
                // $("#result").html('<font color="#ff0000">發生錯誤：GGGGGGGGGGGGGGGG' + jqXHR.responseText + '</font>');
                console.log(jqXHR.responseText);

            }
        })
    }

    function change_section(Section_ID){
        rest_section();
        for(i=0;i<$(".dialog_section").length;i++){
            let $tmp = $(".dialog_section")[i];
            if($($tmp).attr('id')==Section_ID)
                $($tmp).addClass("active");
        }
    }

    function rest_section(){
        // $('input[name="select_section"]').prop('checked', false);
        for(i=0;i<$(".dialog_section").length;i++){
            let $tmp = $(".dialog_section")[i];
            if($($tmp).hasClass("active"))
                $($tmp).removeClass("active");
        }
    }

    function go_index(){
        window.location = "index.php";
    }

    function go_carlist(){
        window.location = "car_list.php";
    }

    function go_cartype(){
        window.location = "car_type.php";
    }
</script>