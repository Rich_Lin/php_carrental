<html>
    <head>
        <script src="../js/jquery-1.12.4.js"></script>
        <link rel="stylesheet" href="//code.jquery.com/ui/1.12.1/themes/base/jquery-ui.css">
        <script src="https://code.jquery.com/ui/1.12.1/jquery-ui.js"></script>

        <style>
            *{
                font-size:1.5vw;
            }
            .function_btn{
                text-align: center;
                padding: 0px 5%;
                border-radius:15px;
                height: 50px;
            }
            .ui-dialog-titlebar{
                display: none
            }
            .ui-widget, .ui-widget-content, .ui-dialog-content{
                border-radius: 20px;
                /* border-width: 20px; */
                /* background-color: #DADADA;
                border: 1px solid #DADADA; */
            }
            .ui-widget-content {
                /* border-radius: 20px; */
                /* border-width: 20px; */
            }
        </style>
    </head>

    <body onload="get_all_car()">
        <button style='font-size: 1.5vw' onclick="go_index()">回首頁</button>
        <center>
            <!-- <button style='font-size: 1.5vw;float:right' onclick="go_cartype()">檢視車型</button> -->
            <button style='font-size: 1.5vw;float:right' onclick="go_carlist()">檢視車輛</button>
            
            <div>
                <h1>車型管理</h1>
                <input class='function_btn' style='display:block;background-color:#0091FF;color:white;float:right;margin-bottom:20px;' type='button' id='Add_Staff' value='新增車型'>
            </div>
            <div id='container' style='display:block;width:85%;height:75%;overflow:auto'>
            </div>
        </center>
    </body>
</html>

<div id="dialog">
    <br>
    <center>
        <form id='myform' action='service.php'>
            <input type='hidden' name='Car_ID' id='Car_ID'>
            <input type='hidden' name='Feature' value='AoU_Car'>
            <table width='95%'>
                <tr>
                    <td style='width:20%;text-align:right'></td>
                    <td style='width:30%'></td>
                    <td style='width:20%;text-align:right'></td>
                    <td></td>
                </tr>
                <tr>
                    <td style='font-size:36px;' colspan='4'>新增/編輯車型：</td>
                </tr>
                <tr>
                    <td style='text-align:right'>車型名稱：</td>
                    <td style=''><input type='text' style='width:100%;' name='Car_Type' id='Car_Type' required></td>
                    <td style='text-align:right' colspan='2'></td>
                </tr>
                <tr>
                    <td style='text-align:right'>平日價格：</td>
                    <td style=''><input type='number' style='width:100%;' name='Weekday_Price' id='Weekday_Price' required></td>
                    <td style='text-align:right'>半日價格：</td>
                    <td style=''><input type='number' style='width:100%;' name='Halfday_Price' id='Halfday_Price' required></td>
                    </td>
                </tr>
                <tr>
                    <td style='text-align:right'>假日價格：</td>
                    <td style=''><input type='text' style='width:100%;' name='Weekend_Price' id='Weekend_Price' required></td>
                    <td style='text-align:right' colspan='2'></td>
                </tr>
                <tr>
                    <td style='text-align:right'>周租價格：</td>
                    <td><input type='number' style='width:100%;' name='Weekly' id='Weekly'></td>
                    <td style='text-align:right'>月租價格：</td>
                    <td><input type='number' style='width:100%;' name='Monthly' id='Monthly'></td>
                </tr>
                <tr>
                    <td style='text-align:right'>備註：</td>
                    <td colspan="3"><input type='text' style='width:100%;' name='Remark' id='Remark'></td>
                </tr>
                <tr>
                    <td style='text-align:center;' colspan="4"><button type='submit' class='function_btn' style='background-color:#0091FF;color:WHITE'>確定</button></td>
                </tr>
            </table>
        </form>
    </center>
</div>

<script>

    var car_type;

    $(document).ready(function() {
        $("#dialog").dialog({
            height: 350,
            width: 800,
            autoOpen: false,
            resizable: false,
            draggable: false,
            modal: true
            // dialogClass: 'ui-dialog-osx',
        });

        $("#Add_Staff").click(function(){
            $("#Staff_ID").val("");
            $( "#dialog" ).dialog( "open" );
        });

        $("#myform").submit(function(){
            event.preventDefault();
            $.ajax({
                type: "POST",
                url: "service.php",
                data: $("#myform").serialize(),
                success: function(data) {
                    close_dialog();
                    get_all_car();
                },
                error: function(jqXHR) {
                    alert(jqXHR.responseText);
                    console.log(jqXHR.responseText);
                    get_all_car();
                }
            })
        })
    });

    $("body").on("click",".ui-widget-overlay",function() {
        close_dialog();
    });

    function close_dialog(){
        $("#Car_ID").val("");
        $('#myform').trigger("reset");
        $('#dialog').dialog( "close" );
    }

    function Edit_Car(Car_ID){
        $.ajax({
            type: "POST",
            url: "service.php",
            dataType: "json",
            data: {
                Feature: "get_specific_car_info",
                Car_ID: Car_ID
            },
            success: function(data) {
                $("#Car_ID").val(data.Car_ID);
                $("#Car_Type").val(data.Car_Type);
                $("#Weekday_Price").val(data.Weekday_Price);
                $("#Halfday_Price").val(data.Halfday_Price);
                $("#Weekend_Price").val(data.Weekend_Price);
                $("#Weekly").val(data.Weekly);
                $("#Monthly").val(data.Monthly);
                $("#Remark").val(data.Remark);
                                
                $( "#dialog" ).dialog( "open" );
            },
            error: function(jqXHR) {
                // $("#result").html('<font color="#ff0000">發生錯誤：GGGGGGGGGGGGGGGG' + jqXHR.responseText + '</font>');
                console.log(jqXHR.responseText);

            }
        })
    }
    
    function Delete_Car(Car_ID){
        $.ajax({
            type: "POST",
            url: "service.php",
            dataType: "json",
            data: {
                Feature: "delete_car",
                Car_ID: Car_ID
            },
            success: function(data) {
                get_all_car();
            },
            error: function(jqXHR) {
                // $("#result").html('<font color="#ff0000">發生錯誤：GGGGGGGGGGGGGGGG' + jqXHR.responseText + '</font>');
                console.log(jqXHR.responseText);

            }
        })
    }

    function get_all_car(){
        $.ajax({
            type: "POST",
            url: "service.php",
            dataType: "json",
            data: {
                Feature: "get_car_type",
            },
            success: function(data) {
                $("#container").html("<table id='container_table' style='width:95%;border-collapse:collapse;' border='1'><tr style='background-color:#6236FF;color:white'><td style='text-align:center'>車型名稱</td><td style='text-align:center'>平日價格</td><td style='text-align:center'>假日價格</td><td style='text-align:center'>半日價格</td><td style='text-align:center'>周租價格</td><td style='text-align:center'>月租價格</td><td style='text-align:center'>備註</td><td></td></tr>");
                car_type = data;
                for(i=0;i<car_type.length;i++){
                    var row='';
                    var Car_ID = car_type[i].Car_ID;
                    var Car_Type = car_type[i].Car_Type;
                    var Weekday_Price = car_type[i].Weekday_Price;
                    var Weekend_Price = car_type[i].Weekend_Price;
                    var Halfday_Price = car_type[i].Halfday_Price;
                    var Weekly = car_type[i].Weekly;
                    var Monthly = car_type[i].Monthly;
                    var Remark = car_type[i].Remark;
                    row += "<tr><td style='text-align:center'>"+Car_Type+"</td><td style='text-align:right'>"+Weekday_Price+"</td><td style='text-align:right'>"+Weekend_Price+"</td><td style='text-align:right'>"+Halfday_Price+"</td><td style='text-align:right'>"+Weekly+"</td><td style='text-align:right'>"+Monthly+"</td><td>"+Remark+"</td><td><button class='function_btn' style='background-color:WHITE;color:BLACK;float:left;display:inline-block' value='"+Car_ID+"' onClick='Edit_Car(this.value)'>編輯</button><!--<button class='function_btn' style='background-color:#E02020;color:WHITE;float:right;display:inline-block' value='"+Car_ID+"' onClick='Delete_Car(this.value)'>刪除</button>--></td></tr>";
                    $("#container_table").append(row);
                }
                $("#container").append("</table>");
            },
            error: function(jqXHR) {
                // $("#result").html('<font color="#ff0000">發生錯誤：GGGGGGGGGGGGGGGG' + jqXHR.responseText + '</font>');
                console.log(jqXHR.responseText);

            }
        })
    }
    
    function go_index(){
        window.location = "index.php";
    }

    function go_carlist(){
        window.location = "car_list.php";
    }

    function go_cartype(){
        window.location = "car_type.php";
    }
</script>