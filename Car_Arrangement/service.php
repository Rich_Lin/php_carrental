<?php
    header('Content-Type: application/json; charset=UTF-8');
    include_once "../mysql_connect.inc.php";
    include_once "../session_stat.php";
    // error_reporting(0);
    if ($_SERVER['REQUEST_METHOD'] == "POST") {
        if(isset($_POST)){
            switch($_POST['Feature']){
                case 'get_carlist':
                    get_carlist($conn);
                break;

                case 'get_booked_list':
                    get_booked_list($_POST['minDate'],$conn);
                break;

                case 'show_booking_list':
                    date_default_timezone_set('Asia/Taipei');
                    $Today_Date = date('Y/m/d', time());
                    $info_array = array();
                    $sql = "SELECT `order_list`.`Order_ID`,`order_list`.`Estimated_Rent_DateTime`,`order_list`.`Estimated_Return_DateTime`,`order_list`.`Rental_Area`,`order_list`.`Return_Area`,`order_list`.`Car_Type`,`order_list`.`License_Plate`,`customer_list`.`Customer_Name`,`customer_list`.`Customer_Phone`,`customer_list`.`Customer_Email` FROM `order_list`,`customer_list` WHERE DATE(`order_list`.`Estimated_Rent_DateTime`)<='".$Today_Date."' AND `order_list`.`Status`<=2 AND `order_list`.`Customer_ID`=`customer_list`.`Customer_ID`";
                    $GROUP_BY = " ORDER BY `order_list`.`Estimated_Rent_DateTime`";
                    $Fuzzy_Search = "";
                    if(!empty($_POST['Fuzzy_Search'])){
                        $Fuzzy_Search = "(`order_list`.`Order_ID` LIKE '%".$_POST['Fuzzy_Search']."%' OR `order_list`.`Emergency_Name` LIKE '%".$_POST['Fuzzy_Search']."%' OR `order_list`.`Emergency_Phone` LIKE '%".$_POST['Fuzzy_Search']."%' OR `order_list`.`Car_Type` LIKE '%".$_POST['Fuzzy_Search']."%' OR `order_list`.`License_Plate` LIKE '%".$_POST['Fuzzy_Search']."%' OR `customer_list`.`Customer_Name` LIKE '%".$_POST['Fuzzy_Search']."%' OR `customer_list`.`Customer_Phone` LIKE '%".$_POST['Fuzzy_Search']."%' OR `customer_list`.`Customer_Email` LIKE '%".$_POST['Fuzzy_Search']."%')";
                        $sql .= " AND " . $Fuzzy_Search . $GROUP_BY;
                    }
                    else
                        $sql .= $GROUP_BY;
                    // echo $sql;die;
                    $result = mysqli_query($conn,$sql);
                    while($row=$result->fetch_assoc()){
                        $info_array[]=$row;
                    }
                    // print_r($info_array);
                    echo json_encode($info_array,JSON_UNESCAPED_UNICODE);
                break;

                case 'get_booked_info':
                    $Order_ID = $_POST['Booking_ID'];
                    $sql = "SELECT `Order_ID`,`Created_DT`,`Customer_ID`,`Estimated_Rent_DateTime`,`Estimated_Return_DateTime`,`Car_Type`,`License_Plate`,`Total_Price`,`Status`,`Rental_Area`,`Return_Area`,`Remark` FROM `order_list` WHERE `Order_ID`='".$Order_ID."'";
                    $resule = mysqli_query($conn,$sql);
                    $Order_Info = $resule->fetch_assoc();
                    $sql = "SELECT `BA_Name` FROM `branch_allocation` WHERE `BA_ID`='".$Order_Info['Rental_Area']."'";
                    $Order_Info['Rental_Area'] = mysqli_query($conn,$sql)->fetch_assoc()['BA_Name'];
                    $sql = "SELECT `BA_Name` FROM `branch_allocation` WHERE `BA_ID`='".$Order_Info['Return_Area']."'";
                    $Order_Info['Return_Area'] = mysqli_query($conn,$sql)->fetch_assoc()['BA_Name'];
                    $sql = "SELECT `Customer_Name`,`Customer_SSID`,`Customer_Sex`,`Customer_Phone`,`Customer_Email` FROM `customer_list` WHERE `Customer_ID`='".$Order_Info['Customer_ID']."'";
                    $Customer_Info = mysqli_query($conn,$sql)->fetch_assoc();
                    foreach($Customer_Info as $key => $value)
                        $Order_Info[$key] = $value;
                    
                    date_default_timezone_set('Asia/Taipei');
                    // $Today_Date = ;
                    $Order_Info['Remark'] = str_replace(chr(13).chr(10), "<br />",$Order_Info['Remark']);
                    if(intval(date_diff(date_create(date('Y/m/d H:i:s', time())),date_create($Order_Info['Estimated_Rent_DateTime']))->format('%r%a'))<=0)
                        $Order_Info['SC_Flag'] = false;
                    else
                        $Order_Info['SC_Flag'] = true;

                    echo json_encode($Order_Info,JSON_UNESCAPED_UNICODE);
                break;

                case 'Status_Change':
                    // print_r($_POST);die;
                    date_default_timezone_set('Asia/Taipei');
                    $current_DT = date('Y-m-d H:i:s', time());
                    if($_POST['Status']<3){
                        $_POST['Status']+=3;
                        $sql = "UPDATE `order_list` SET `Actual_Rent_DateTime`='".$current_DT."',`Status`='".$_POST['Status']."' ";
                    }
                    else if($_POST['Status']>=3 && $_POST['Status']<=5){
                        $_POST['Status']=6;
                        $sql = "UPDATE `order_list` SET `Actual_Return_DateTime`='".$current_DT."',`Status`='".$_POST['Status']."' ";
                    }
                    $sql .= "WHERE `Order_ID`='".$_POST['Order_ID']."'";
                    if(!mysqli_query($conn,$sql)){
                      echo "SQL Error: CA_SC";
                      die;
                    }
                    echo json_encode(array('Success' => true),JSON_UNESCAPED_UNICODE);
                break;
            }
        }
    }

    function get_carlist($conn){
        $vehicle_array = array();
        $sql = "SELECT `car_list`.`License_Plate`,`car_list`.`Car_Type`,`car_type`.`Car_Capacity` FROM `car_list`,`car_type` WHERE `car_type`.`Car_Type`=`car_list`.`Car_Type` ORDER BY `car_type`.`Car_Capacity`";
        $result = mysqli_query($conn,$sql);
        while($row = $result->fetch_assoc()){
            $vehicle_array[$row['Car_Capacity']][$row['Car_Type']][] = $row;
        }
        echo json_encode($vehicle_array,JSON_UNESCAPED_UNICODE);
    }

    function get_booked_list($minDate,$conn){

        $booked_array = array();
        $maxDate = date('Y/m/d', strtotime($minDate. ' + 14 days'));
        $sql = "SELECT `order_list`.`Order_ID`,`order_list`.`Car_Type`,`order_list`.`License_Plate`,`order_list`.`Rental_Area`,`order_list`.`Return_Area`,DATE(`order_list`.`Estimated_Rent_DateTime`),DATE(`order_list`.`Estimated_Return_DateTime`),`order_list`.`Status` FROM `order_list`,`car_type` WHERE `car_type`.`Car_Type`=`order_list`.`Car_Type` AND ((DATE(`order_list`.`Estimated_Rent_DateTime`) BETWEEN '".$minDate."' AND '".$maxDate."' ) OR (DATE(`order_list`.`Estimated_Return_DateTime`) BETWEEN '".$minDate."' AND '".$maxDate."' ) OR ('".$minDate."' BETWEEN DATE(`order_list`.`Estimated_Rent_DateTime`) AND DATE(`order_list`.`Estimated_Return_DateTime`)) OR ('".$maxDate."' BETWEEN DATE(`order_list`.`Estimated_Rent_DateTime`) AND DATE(`order_list`.`Estimated_Return_DateTime`))) AND (`order_list`.`Status` BETWEEN 0 AND 5 OR `order_list`.`Status`=7) AND `order_list`.`License_Plate`!='' ORDER BY `car_type`.`Car_Capacity`";
        $result = mysqli_query($conn,$sql);
        $count = 0;
        while($row = $result->fetch_assoc()){
            foreach($row as $key => $value){
                switch($key){
                    case "DATE(`order_list`.`Estimated_Rent_DateTime`)":
                        $booked_array[$count]['Estimated_Rent_DateTime'] = $row['DATE(`order_list`.`Estimated_Rent_DateTime`)'];
                    break;
                    
                    case "DATE(`order_list`.`Estimated_Return_DateTime`)":
                        $booked_array[$count]['Estimated_Return_DateTime'] = $row['DATE(`order_list`.`Estimated_Return_DateTime`)'];
                    break;

                    default:
                        $booked_array[$count][$key] = $value;
                }
            }
            if(date_create($booked_array[$count]['Estimated_Rent_DateTime']) < date_create($minDate))
                $booked_array[$count]['Estimated_Rent_DateTime'] = $minDate;
            if(date_create($booked_array[$count]['Estimated_Return_DateTime']) > date_create($maxDate))
                $booked_array[$count]['Estimated_Return_DateTime'] = $maxDate;
            $booked_array[$count]['Day_Count'] = date_diff(date_create($booked_array[$count]['Estimated_Rent_DateTime']),date_create($booked_array[$count]['Estimated_Return_DateTime']))->format("%a") + 1;
            $tmp = date_diff(date_create($booked_array[$count]['Estimated_Rent_DateTime']),date_create($minDate))->format("%a");
            if($tmp < 0)
                $tmp = 0;
            $booked_array[$count]['Start_From'] = $tmp;
            
            $booked_array[$count][$key] = $value;
            $sql = "SELECT `BA_Color` FROM `branch_allocation` WHERE `BA_ID`='".$booked_array[$count]['Rental_Area']."'";
            $booked_array[$count]['Color1'] = mysqli_query($conn,$sql)->fetch_assoc()['BA_Color'];
            $sql = "SELECT `BA_Color` FROM `branch_allocation` WHERE `BA_ID`='".$booked_array[$count]['Return_Area']."'";
            $booked_array[$count]['Color2'] = mysqli_query($conn,$sql)->fetch_assoc()['BA_Color'];            
            $count++;
        }
        // print_r($booked_array);
        // die;
        echo json_encode($booked_array,JSON_UNESCAPED_UNICODE);
    }

    function get_no_roomlist($conn){
        $booked_array = array();
        $count = 0;
        $sql = "SELECT `booking_detail`.`Code`,`customer`.`Customer_Name`,`booking_detail`.`Room_Type`,`booking_index`.`CIN_Date`,`booking_index`.`COUT_Date` FROM `booking_index`,`booking_detail`,`customer` WHERE `customer`.`Customer_ID`=`booking_index`.`Customer_ID` AND `booking_detail`.`Room_Num`='' AND `booking_index`.`Booking_ID`=`booking_detail`.`Booking_ID` AND (`booking_detail`.`Room_Status`=0 OR `booking_detail`.`Room_Status`=7) ORDER BY `booking_index`.`CIN_Date`, `booking_detail`.`Code`";
        $result = mysqli_query($conn,$sql);
        while($row = $result->fetch_assoc()){
            $booked_array[$count]['Code'] = $row['Code'];
            $booked_array[$count]['Customer_Name'] = $row['Customer_Name'];
            $booked_array[$count]['Room_Type'] = $row['Room_Type'];
            $booked_array[$count]['CIN_Date'] = date("Y/m/d",strtotime($row['CIN_Date']));
            $booked_array[$count]['COUT_Date'] = date("Y/m/d",strtotime($row['COUT_Date']));
            $count++;
        }
        echo json_encode($booked_array,JSON_UNESCAPED_UNICODE);
    }
?>