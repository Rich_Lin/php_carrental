<?php
    include_once "../session_stat.php";
?>
<html>
    <head>
        <style>
            .date_btn{
                width: 55px;
                height:71px;
                border-radius: 7.5px;
                background-color: WHITE;
                border: 1px solid #6D7278;
                font-size: 16px;
                margin: 0px;
            }
            .checking{
                background-color: #6D7278 !important;
                color: WHITE !important;
                /* width: 220px !important; */
            }
            .current_date{
                background-color: #E5E5E5;
            }
            .the_table{
                border-collapse: separate;
            }
            .the_table td{
                border-radius:7.5px;
                height: 68px;
                width: 65px;
                background-color: #C2C2C2;
                text-align: center;
                white-space: nowrap;
            }
            .pr{
                position: relative;
            }
            .main_function_div{
                /* width: 100%; */
                height: 750px;
                overflow-x: hidden;
                overflow-y: scroll;
    			position: relative;
            }
            .main_function_div::-webkit-scrollbar{
                display: none
            }
            .unset_booking_block{
                background-image: linear-gradient(90deg, #34C6FF, #007EFA);
                width: 233px;
                height: 142px;
                border-radius: 15px;
                margin: 10px 0px;
                color: WHITE;
                font-size: 22px;
                padding-top: 20px;
                padding-left: 20px;
    			position: relative;
            }
            .arranged_booking{
                top: 0;
                left: 0;
                height: 100%;
                width: 100%;
                border-radius: 7.5px;
            }
            .ui-widget.ui-widget-content{
                border-radius: 20px;
                border-width: 20px;
                /* background-color: #DADADA; */
                border: 1px solid #DADADA;
            }
            .ui-widget-content {
                border-radius: 20px;
                border-width: 20px;
            }
            .ui-widget-overlay{
                background-color: transparent;
            }
            .ui-dialog-titlebar{
                display: none
            }
            .info_table{
                width: 90%
            }
            .info_table tr,td{
                /* vertical-align: center;
                padding: 5px 0px;
                font-size: 22px; */
                font-size: 22px;
            }
            .info_button_table{
                width: 100%;
                height: 175px;
                background-color: WHITE;
                border: 2.5px solid #979797;
                border-radius:15px;
                margin: 10px;
            }
            .info_button_table:hover{
                /* border: 1px solid #DADADA; */
                background-color: #DADADA;
            }
        </style>
        <meta charset="UTF-8" />
        <link rel="stylesheet" type="text/css" href="daily_review.css">
        <script type="text/javascript" src="../js/functions.js"></script>
        <script type="text/javascript" src="https://cdnjs.cloudflare.com/ajax/libs/moment.js/2.22.2/moment.min.js"></script>

        <script type="text/javascript" src="https://code.jquery.com/jquery-3.3.1.slim.min.js"></script>
        <script type="text/javascript" src="https://cdnjs.cloudflare.com/ajax/libs/twitter-bootstrap/4.3.1/js/bootstrap.bundle.min.js"></script>
        <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/twitter-bootstrap/4.3.1/css/bootstrap.min.css">

        <link rel="stylesheet" href="//code.jquery.com/ui/1.12.1/themes/base/jquery-ui.css">
        <script src="https://code.jquery.com/jquery-1.12.4.js"></script>
        <script src="https://code.jquery.com/ui/1.12.1/jquery-ui.js"></script>

		<script type="text/javascript" src="../js/lightpick.js"></script>
        <link rel="stylesheet" type="text/css" href="../js/lightpick.css">
    </head>
    <body onload='includeHTML();create_datebar();'>
        <div class='navbar-div' include-html="../navbar.php"></div>
        <div class='for_hyper left' include-html="../hyper.php"></div>
        <div class='right'>
            <center>
                <div>
                    <table width='95%'>
                        <tr>
                            <td><input type='text' style='width:300px;height:50px;font-size:22px;text-align:center;' id='Date'><input type='button' class='function_btn' style='width:130px;height:50px;background-color:#0091FF;' value='今日' onClick='set_Date()'></td>
                            <td style='text-align:right'><input type='button' class='function_btn' style='width:130px;height:50px;background-color:#F79B00;' value='訂單取車' onClick='available_list()'><input type='button' class='function_btn' style='width:130px;height:50px;background-color:#F79B00;' value='新增訂單' onClick="location.href = '../Orders/New_Order.php';"></td>
                        </tr>
                    </table>
                </div>
                <div style='width:100%'>
                    <center>
                        <table style='table-layout:fixed;display:inline;width:95%;'>
                            <tr>
                                <td></td>
                                <td><center><table style='width:100%;border-collapse: separate;' cellspacing='10'><tr id='datebar_container'></tr></table></center></td>
                            </tr>
                            <tr>
                                <td style='vertical-align:top;'>
                                    <div id='roomlist_container' class='main_function_div'>
                                        <table id='roomlist_table' width='100%' class='the_table' style='font-size:28px;' cellspacing='10'>
                                        </table>
                                    </div>
                                </td>
                                <td style='vertical-align:top'>
                                    <center>
                                        <div id='vehicledate_container' class='main_function_div'>
                                            <table id='vehicledate_table' class='the_table' style='width:100%' cellspacing='10'>
                                            </table>
                                        </div>
                                    </center>
                                </td>
                            </tr>
                        </table>
                    </center>
                </div>
            </center>
        </div>
    </body>
</html>

<!----------Dialog---------->
    <div id='Booking_List_Dialog' name='dialog_section'><br>
        <center>
            <table width='80%'>
                <tr>
                    <td style='text-align:center;font-size:36px;'>
                        可出車訂單：
                        <!-- <input type='text' id='Fuzzy_Search' style='text-align:center;font-size:36px;font-family:Microsoft JhengHei;' placeholder='關鍵字搜尋'> -->
                    </td>
                </tr>
            </table>
            <form action='../Orders/Order_Details.php' method='POST'>
                <table id='Booking_List_Table' width='90%' style='table-layout: fixed;'>
                </table>
            </form>
        </center>
    </div>

    <div id="Booked_Info_Dialog" name='dialog_section'><br>
        <center>
            <table id='Index_Table' width='90%'>
                <tr>
                    <td rowspan='2'><center><img src='../images/LWithoutN.png' style='width:80px;height:80px;'></center></td>
                    <td style='font-size:32px'>訂單編號：<span id='Dialog_Booking_ID' style='font-size:26px;color:#0091FF'></td>
                </tr>
                <tr>
                    <td style='font-size:32px'>訂車日期：<span id='Dialog_Booking_Date' style='font-size:26px'></span></td>
                </tr>
            </table>
            <br>
            <table id='Customer_Table' width='90%'>
                <tr><td style='font-size:26px'>顧客姓名：<span id='Customer_Name' style='font-size:26px'></span></td></tr>
                <tr><td style='font-size:26px'>顧客性別：<span id='Customer_Sex' style='font-size:26px'></span></td></tr>
                <tr><td style='font-size:26px'>身分證號：<span id='Customer_SSID' style='font-size:26px'></span></td></tr>
                <tr><td style='font-size:26px'>連絡電話：<span id='Customer_Phone' style='font-size:26px'></span></td></tr>
                <tr><td style='font-size:26px'>E-Mail：<span id='Customer_Email' style='font-size:26px'></span></td></tr>

            </table>
            <br>
            <table id='Detail_Table' width='90%'>
                <tr>
                    <td style='font-size:26px' colspan='2'>取車日期：<span id='Estimated_Rent_DateTime' style='font-size:26px'></span></td>
                </tr>
                <tr>
                    <td style='font-size:26px' colspan='2'>還車日期：<span id='Estimated_Return_DateTime' style='font-size:26px'></span></td>
                </tr>
                <tr>
                    <td style='font-size:26px'>車型：<span id='Car_Type' style='font-size:26px'></span></td>
                    <td style='font-size:26px'>取車地點：<span id='Rental_Area' style='font-size:26px'></span></td>
                </tr>
                <tr>
                    <td style='font-size:26px'>車號：<span id='License_Plate' style='font-size:26px'></span></td>
                    <td style='font-size:26px'>還車地點：<span id='Return_Area' style='font-size:26px'></span></td>
                </tr>
                <tr>
                    <td style='font-size:26px'>狀態：<span id='Status' style='font-size:26px'></span></td>
                    <td style='font-size:26px'>總價：<span id='Total_Price' style='font-size:26px'></span></td>
                </tr>
                <tr>
                    <td colspan='2' style='font-size:26px'>備註：<br><span id='Remark' style='font-size:26px'></span></td>
                </tr>
            </table>
            <br>
            <table width='90%' style='bottom: 0;'>
                <tr>
                    <td style='text-align:left'>
                        <form id='To_Detail' style='display:inline' action='../Orders/Order_Details.php' method='POST'>
                            <input type='hidden' id='To_Detail_ID' name='Action'>
                            <input type='submit' id='To_Detail_Button' style='width:130px;height:50px;background-color:#0091FF;font-size:24px;margin:0px 5px;' class='function_btn' value='進入訂單'>
                        </form>
                    </td>
                    <td style='text-align:right'>
                        <form id='Status_Change' style='display:inline' action='service.php' method='POST'>
                            <input type='hidden' name='Feature' value='Status_Change'>
                            <input type='hidden' id='Status_Change_Order_ID' name='Order_ID'>
                            <input type='hidden' id='Status_Change_Status' name='Status'>
                            <button id='Status_Change_Button' style='width:130px;height:50px;background-color:#0091FF;font-size:24px;margin:0px 5px;' class='function_btn'>還車</button>
                        </form>
                        <button type="button" style='width:130px;height:50px;background-color:#0091FF;font-size:24px;margin:0px 5px;' class='function_btn' onClick="close_dialog()">確定</button>
                    </td>
                </tr>
            </table>
        </center>
    </div>
<!-------------------------->

<script>
    var gender = ['女','男','不明','戰鬥直升機'];
    var Weekday = ['日','一','二','三','四','五','六'];
    var Status = ['未取車(未付訂)','未取車(已付訂)','未取車(已付清)','已出車(未付訂)','已出車(已付訂)','已出車(已付清)','已還車','保留中','訂單取消'];
    var Vehicle_Array = [];
    var Booking_List = [];
    var minDate;

    window.onerror = function(errorMsg, url, lineNumber){
        // location.reload();
    }

    var picker = new Lightpick({
        field: document.getElementById('Date'),
        startDate: moment().startOf('day'),
        singleDate: true,
        repick: true,
        onSelect: function(date){
            create_datebar();
            // Day count is disabled at line 719
        }
    });

    $(document).ready(function() {
        var ignoreScrollEvents = false;
        function syncScroll(element1, element2) {
            element1.scroll(function (e) {
                var ignore = ignoreScrollEvents
                ignoreScrollEvents = false
                if (ignore) return

                ignoreScrollEvents = true
                element2.scrollTop(element1.scrollTop())
            })
        }
        syncScroll($("#roomlist_container"), $("#vehicledate_container"));
        syncScroll($("#vehicledate_container"), $("#roomlist_container"));

        $("#Status_Change").submit(function(){
            event.preventDefault();
            $.ajax({
                type: "POST",
                url: "service.php",
                data: $("#Status_Change").serialize(),
                success: function(data) {
                    if(data.Success){
                        create_datebar();
                        close_dialog();
                    }
                },
                error: function(jqXHR) {
                    console.log("error: " + jqXHR.responseText);
                }
            })
        });

        $("#Booking_List_Dialog").dialog({
            width: 970,
            autoResize:true,
            autoOpen: false,
            resizable: false,
            draggable: false,
            modal: true
            // dialogClass: 'ui-dialog-osx',
        });

        $("#Booked_Info_Dialog").dialog({
            // height: 750,
            width: 669,
            autoResize:true,
            autoOpen: false,
            resizable: false,
            draggable: false,
            modal: true
            // dialogClass: 'ui-dialog-osx',
        });
    });

    function close_dialog(){
        var Dialog_Section = document.getElementsByName('dialog_section');
        for(i=0;i<Dialog_Section.length;i++)
            $('#'+Dialog_Section[i].id).dialog( "close" );
    }

    $("body").on("click",".ui-widget-overlay",function() {
        close_dialog();
    });

    function set_Date(){
        $("#Date").val(moment().startOf('day').format('YYYY/MM/DD'));
        create_datebar();
    }

    function create_datebar(){
        var Today_Date = moment().startOf('day');
        var Start_Date = $("#Date").val();
        minDate =  moment(new Date(Start_Date)).subtract(2, 'day').format('YYYY/MM/DD');
        var row='';
        for(i=0;i<15;i++){
            row += "<td class='";
            if(i>9)
                row += "over_size";
            row += "'><center><button type='button' id='" + moment(new Date(minDate)).add(i, 'day').format('YYYY/MM/DD') + "' class='date_btn";
            if(moment(new Date(minDate)).add(i, 'day').format('YYYY/MM/DD')==moment(new Date(Today_Date)).format('YYYY/MM/DD')){
                row += " current_date";
            }
            if(i==2)
                row += " checking' onClick='set_Checking_Date(this.id)'>" + moment(new Date(minDate)).add(i, 'day').format('MM/DD') + "<br>(" + Weekday[moment(new Date(minDate)).add(i, 'day').day()] + ")</button></center></td>";
            else
                row += "' onClick='set_Checking_Date(this.id)'>" + moment(new Date(minDate)).add(i, 'day').format('MM/DD') + "<br>(" + Weekday[moment(new Date(minDate)).add(i, 'day').day()] + ")</button></center></td>";
        }
        $("#datebar_container").html(row);
        create_list();
    }

    function set_Checking_Date(target_value){
        $("#Date").val(target_value);
        create_datebar();
    }

    function set_Month(value){
        var Current_Date = $("#Date").val();
        switch(value){
            case -1:
                $("#Date").val(moment(new Date(Current_Date)).subtract(1, 'month').format('YYYY/MM/DD'));
                create_datebar();
            break;

            case 1:
                $("#Date").val(moment(new Date(Current_Date)).add(1, 'month').format('YYYY/MM/DD'));
                create_datebar();
            break;
        }
    }

    function create_list(){
        $.ajax({
            type: "POST",
            url: "service.php",
            data: {
                Feature: "get_carlist",
            },
            success: function(data) {
                Vehicle_Array = data;
                let vehicle_list = '';
                for(let Car_Capacity in Vehicle_Array){
                    for(let Car_Type in Vehicle_Array[Car_Capacity]){
                        // vehicle_list += "<tr class='CType'><td style='background-color: WHITE;'>" + Car_Type + "</td></tr>";
                        vehicle_list += "<tr><td style='background-color: WHITE;'></td></tr>";
                        for(let info in Vehicle_Array[Car_Capacity][Car_Type]){
                            vehicle_list += "<tr><td class='redips-mark'>" + Vehicle_Array[Car_Capacity][Car_Type][info]['License_Plate'] + "</td></tr>";
                        }
                    }
                }
                $("#roomlist_table").html(vehicle_list);
                create_calendar_table();
            },
            error: function(jqXHR) {
                console.log("error: " + jqXHR.responseText);
            }
        });
    }

    function create_calendar_table(){
        $.ajax({
            type: "POST",
            url: "service.php",
            data: {
                Feature: "get_booked_list",
                minDate: minDate,
            },
            success: function(data) {
                let Calendar = data;
                let Calendar_list = '';
                // console.log(Calendar);
                for(let Car_Capacity in Vehicle_Array){
                    for(let Car_Type in Vehicle_Array[Car_Capacity]){
                        Calendar_list += "<tr><td style='background-color: WHITE;' colspan='15'>" + Car_Type + "</td></tr>";
                        for(let info in Vehicle_Array[Car_Capacity][Car_Type]){
                            Calendar_list += "<tr>";
                            for(i=0;i<15;i++){
                                let flag = true;
                                Calendar_list += "<td ";
                                for(x=0;x<Calendar.length;x++){
                                    if(Vehicle_Array[Car_Capacity][Car_Type][info]['License_Plate'] == Calendar[x].License_Plate && i==Calendar[x].Start_From){
                                        Calendar_list += "colspan='"+Calendar[x].Day_Count+"'";
                                        if(i>9)
                                            Calendar_list += " class='over_size'";
                                        Calendar_list += "><div class='arranged_booking' style='background-image: linear-gradient(90deg, "+Calendar[x].Color1+", "+Calendar[x].Color2+");' title='"+Calendar[x].Order_ID+"' onclick='open_dialog(\""+Calendar[x].Order_ID+"\");'></div></td>";
                                        flag = !flag;
                                        i+=Calendar[x].Day_Count-1;
                                    }
                                }
                                if(flag){
                                    if(i>9)
                                        Calendar_list += " class='over_size'";
                                    Calendar_list += " ></td>";
                                }
                            }
                            Calendar_list += "</tr>";
                        }
                    }
                }
                $("#vehicledate_table").html(Calendar_list);
                // setTimeout('create_calendar_table()', 60000);
            },
            error: function(jqXHR) {
                console.log(jqXHR.responseText);
            }
        })
    }

    function open_dialog(BID){
        ////////if name is set, which means it has booking_id
        $.ajax({
            type: "POST",
            url: "service.php",
            dataType: "json",
            data: {
                Feature: "get_booked_info",
                Booking_ID: BID,
            },
            success: function(data) {
                
                let Estimated_Rent_DateTime = data['Estimated_Rent_DateTime'].replace(/-/g, "/").split(":")[0] + ":" + data['Estimated_Rent_DateTime'].replace(/-/g, "/").split(":")[1];
                let Estimated_Return_DateTime = data['Estimated_Return_DateTime'].replace(/-/g, "/").split(":")[0] + ":" + data['Estimated_Return_DateTime'].replace(/-/g, "/").split(":")[1];

                $("#Dialog_Booking_ID").html(data['Order_ID']);
                $("#Dialog_Booking_Date").html(data['Created_DT'].replace(/-/g, "/"));
                $("#Customer_Name").html(data['Customer_Name']);
                $("#Customer_Sex").html(gender[data['Customer_Sex']]);
                $("#Customer_SSID").html(data['Customer_SSID']);
                $("#Customer_Phone").html(data['Customer_Phone']);
                $("#Customer_Email").html(data['Customer_Email']);
                $("#Estimated_Rent_DateTime").html(Estimated_Rent_DateTime);
                $("#Estimated_Return_DateTime").html(Estimated_Return_DateTime);
                $("#Car_Type").html(data['Car_Type']);
                $("#Rental_Area").html(data['Rental_Area']);
                $("#License_Plate").html(data['License_Plate']);
                $("#Return_Area").html(data['Return_Area']);
                $("#Status").html(Status[data['Status']]);
                $("#Total_Price").html(data['Total_Price']);
                $("#Remark").html(data['Remark']);

                $("#To_Detail_ID").val(data['Order_ID']);

                $("#Status_Change_Order_ID").val(data['Order_ID']);
                $("#Status_Change_Status").val(data['Status']);
                switch(data['Status']){
                    case "0":
                    case "1":
                    case "2":
                        $("#Status_Change_Button").html("出車");
                    break;
                    
                    case "3":
                    case "4":
                    case "5":
                        $("#Status_Change_Button").html("還車");
                    break;

                    case "7":
                        $("#Status_Change_Button").html("你媽死了");
                    break;
                }

                $("#Status_Change_Button").prop('disabled', data['SC_Flag']);

                $( "#Booked_Info_Dialog" ).dialog( "open" );
            },
            error: function(jqXHR) {
                console.log(jqXHR.responseText);
            }
        })
    }

    function available_list(){
        $.ajax({
            type: "POST",
            url: "service.php",
            dataType: "json",
            data: {
                Feature: "show_booking_list",
                Fuzzy_Search: $("#Fuzzy_Search").val()
            },
            success: function(data) {
                // console.log(data);return;
                $("#Booking_List_Table").html('');
                var json_data = data;
                var row = "";
                for(i=0;i<json_data.length;i++){
                    row += "<tr><td><button type='submit' class='info_button_table' value='"+json_data[i]['Order_ID']+"' name='Action'><center><table class='info_table'><tr><td width='50%'>訂單編號：<span style='color:#0091FF;'>"+json_data[i]['Order_ID']+"</span></td><td>旅客姓名："+json_data[i]['Customer_Name']+"</td></tr><tr><td>租車日期："+json_data[i]['Estimated_Rent_DateTime'].replace(/-/g, "/").split(":")[0] + ":" + json_data[i]['Estimated_Rent_DateTime'].replace(/-/g, "/").split(":")[1] +"</td><td>連絡電話："+json_data[i]['Customer_Phone']+"</td></tr><tr><td>退房日期："+json_data[i]['Estimated_Return_DateTime'].replace(/-/g, "/").split(":")[0] + ":" + json_data[i]['Estimated_Return_DateTime'].replace(/-/g, "/").split(":")[1] +"</td><td>E-Mail："+json_data[i]['Customer_Email']+"</td></tr><tr><td>車型：<span style='color:#0091FF;'>"+json_data[i]['Car_Type']+" / "+json_data[i]['License_Plate']+"</span></td><td></td></tr></table></center></button></td></tr>";
                }
                $("#Booking_List_Table").html(row);
                $( "#Booking_List_Dialog" ).dialog( "open" );
            },
            error: function(jqXHR) {
                console.log(jqXHR.responseText);
            }
        })
    }
</script>