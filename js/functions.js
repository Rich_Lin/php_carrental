var decodedCookie = decodeURIComponent(document.cookie);

function includeHTML() {
    // check_log();
    // access_control();
    var z, i, elmnt, file, xhttp;
    /*loop through a collection of all HTML elements:*/
    z = document.getElementsByTagName("*");
    for (i = 0; i < z.length; i++) {
        elmnt = z[i];
        file = elmnt.getAttribute("include-html");
        if (file) {
            xhttp = new XMLHttpRequest();
            xhttp.onreadystatechange = function() {
                if (this.readyState == 4) {
                    if (this.status == 200) { elmnt.innerHTML = this.responseText; }
                    if (this.status == 404) { elmnt.innerHTML = "Page not found."; }
                    elmnt.removeAttribute("include-html");
                    includeHTML();
                }
            }
            xhttp.open("GET", file, true);
            xhttp.send();
            return;
        }
    }
}

function dropdown_toogle(target) {
    if (target.classList.contains('active-dropdown')) {
        target.classList.remove("active-dropdown");
        document.getElementById('arrow-icon').classList.remove("fa-caret-up");
        document.getElementById('arrow-icon').classList.add("fa-caret-down");
        document.getElementById("dropdown-container").style.display = 'none';
    } else {
        target.classList.add("active-dropdown");
        document.getElementById("dropdown-container").style.display = 'block';
        document.getElementById('arrow-icon').classList.add("fa-caret-up");
        document.getElementById('arrow-icon').classList.remove("fa-caret-down");
    }
}

function logout() {
    var logout_confirm = confirm("確定要登出嗎？");
    if (logout_confirm == true) {
        $.ajax({
            type: "POST",
            url: "../logout.php",
            data: {},
            success: function(data) {
                window.location.href = '../';
            },
            error: function(jqXHR) {
                console.log("error: " + jqXHR.responseText);
            }
        })
    }
}

// function check_log() {
//     $.ajax({
//         type: "POST",
//         url: "../login.php",
//         data: {
//             Feature: "check_login",
//         },
//         success: function(data) {
//             if (!data.Status)
//                 window.location.href = '../';
//         },
//         error: function(jqXHR) {
//             console.log("error: " + jqXHR.responseText);
//         }
//     })
// }

function access_control() {
    var new_decodedCookie = decodedCookie.split(';');
    var ID = new_decodedCookie[0];
    var Level = new_decodedCookie[1];
    if (ID.charAt(0) == ' ')
        ID = ID.substring(1);
    ID = ID.split('=');
    ID = ID[1];
    if (Level.charAt(0) == ' ')
        Level = Level.substring(1);
    Level = Level.split('=');
    Level = Level[1];

    var x = document.getElementsByClassName('access_level');
    for (i = 0; i < x.length; i++) {
        if (x[i].classList.contains('AC_' + (Level - 1))) {
            x[i].disabled = true;
            x[i].style.display = 'none';
        } else if (x[i].classList.contains('AC_' + Level) && x[i].classList.contains('self_check') && x[i].value != ID) {
            x[i].disabled = true;
            x[i].style.display = 'none';
        }
    }
    setTimeout('access_control()', 750);
}

// $(function() {
//   // Sidebar toggle behavior
//   $('#sidebarCollapse').on('click', function() {
//     $('#sidebar, #content').toggleClass('active');
//   });
// });