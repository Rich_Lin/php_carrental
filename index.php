<?php
    include_once "session_stat.php";
    // echo '<meta http-equiv=REFRESH CONTENT=0;url=Car_Arrangement\>';
    // '未取車(未付訂)',
    // '未取車(已付訂)',
    // '未取車(已付清)',
    // '已出車(未付訂)',
    // '已出車(已付訂)',
    // '已出車(已付清)',
    // '已還車',
    // '保留中',
    // '訂單取消'
?>
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=utf-8">
        <style>
            * {
                font-family: Microsoft JhengHei;
            }
            #login_form{
                position: absolute;
                left: 50%;
                top: 50%;
                transform: translate(-50%, -50%);
                text-align: center;
            }
            #contain{
                position: relative;
                border: 1px solid #979797;
                border-radius: 15%;
                padding: 50px;
                font-size: 24px;
                text-align: center;
            }
            input{
                margin:10px
            }
            input[type='text'],input[type='password']{
                width: 200px;
                height: 40px;
                font-size: 24px;
                border: 1px solid #979797;
                border-radius: 15px;
            }
            input[type=checkbox], input[type=radio] {
                vertical-align: middle;
                position: relative;
                bottom: 1px;
            }
            input[type=submit]{
                width: 300px;
                height: 80;
                font-size: 24px;
                background-color: #F79B00;
                color: WHITE;
                border-radius: 15px;
            }
        </style>
        
        <script src="https://code.jquery.com/jquery-1.12.4.js"></script>
    </head>
    <body onload="check_login_status();">

        <div id='login_form' style='width:450px;'>
            <p style='font-size:30px'>10Night Studio 租車管理系統</p>
            <div id='contain'>
                <img src='images\LWithoutN.png' style='width:100px;height:100px;'>
                <br>
                <span id='message'></span>
                <form id='login' action='login.php' method='POST'>
                    <input type='hidden' name='Feature' value='login'>
                    帳號：<input type='text' name='Account' placeHolder='' value='a795006017' required>
                    <br>
                    密碼：<input type='password' name='PassWord' id='PassWord' value='111' required>
                    <br>
                    <label><input type='checkbox' style='zoom:2' id='Remember_Me' name='Remember_Me' value='true'>記住我</label>
                    <br>
                    <input type='submit' value='登入'>
                </form>
            </div>
        </div>

    </body>
</html>

<script>
    function check_login_status(){
        $.ajax({
            type: "POST",
            url: "login.php",
            dataType: "json",
            data: {
                Feature: "check_login",
            },
            success: function(data) {
                if(data.Success)
                    window.location.href = 'Car_Arrangement/';
            },
            error: function(jqXHR) {
                console.log(jqXHR.responseText);
            }
        })
    }

    $(document).ready(function() {
        $("#login").submit(function(){
            var checked = false;
            event.preventDefault();
            $.ajax({
                type: "POST",
                url: "login.php",
                data: $("#login").serialize(),
                success: function(data) {
                    console.log(data);
                    if(data.Success){
                        window.location.href = 'Car_Arrangement/';
                    }
                    else{
                        $("#message").html('<font color="#ff0000">帳號或密碼輸入有誤，請重新輸入</font>');
                        $("#PassWord").val('');
                    }
                },
                error: function(jqXHR) {
                    // $("#message").html(jqXHR.responseText);
                    console.log("error: " + jqXHR.responseText);
                }
            })
        });
    });
</script>