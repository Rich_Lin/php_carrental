<?php
// session_start();
/////////////////////////////////////////////////////
//Create Log
date_default_timezone_set('Asia/Taipei');
$current_date = date('Y-m', time());
$current_dt = date('m-d H:i:s', time());
$url  = '../'.$current_date.'_log.txt';
$dir_name = dirname($url);
if(!file_exists($dir_name)) {
    $res = mkdir(iconv("UTF-8","GBK",$dir_name),0777,true);
}
$fp = fopen($url,"a");
fwrite($fp,"\"".$current_dt."\": " . var_export($_POST,true)."\r\n");
fclose($fp);
/////////////////////////////////////////////////////
//資料庫設定
//資料庫位置
$db_server = "localhost";
//資料庫名稱
$db_name = "car_rental";
//資料庫管理者帳號
$db_user = "root";
//資料庫管理者密碼
$db_passwd = "";

//對資料庫連線
$conn = new mysqli($db_server, $db_user, $db_passwd,$db_name) or die("無法對資料庫連線");

//資料庫連線採UTF8
mysqli_query($conn, "SET NAMES utf8");

//選擇資料庫
//if(!@mysqli_select_db($db_name))
//        die("無法使用資料庫");
?>