<?php
    include_once "../session_stat.php";
?>
<html>
    <head>
        <script type="text/javascript" src="../js/functions.js"></script>
        <script type="text/javascript" src="https://cdnjs.cloudflare.com/ajax/libs/moment.js/2.22.2/moment.min.js"></script>

        <link rel="stylesheet" href="//code.jquery.com/ui/1.12.1/themes/base/jquery-ui.css">
        <script src="https://code.jquery.com/jquery-1.12.4.js"></script>
        <script src="https://code.jquery.com/ui/1.12.1/jquery-ui.js"></script>
    
        <script type="text/javascript" src="https://cdnjs.cloudflare.com/ajax/libs/twitter-bootstrap/4.3.1/js/bootstrap.bundle.min.js"></script>
        <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/twitter-bootstrap/4.3.1/css/bootstrap.min.css">

        <script type="text/javascript" src="../js/lightpick.js"></script>
        <link rel="stylesheet" type="text/css" href="../js/lightpick.css">

        <style>
            *{
                font-size: 18px;
            }
            p{
                margin-bottom: unset !important;
            }
            #Branch_Table{
                margin-top: 15px;
            }
            #Branch_Table tr:hover{
                background-color: #ffff99;
            }
            #Branch_Table th,td{
                text-align:center;
                padding: 5px 0px;
            }
            #Branch_Table th{
                background-color: #6236FF;
                color: WHITE;
            }
            #Branch_Table td{
                width:1%;
                white-space:nowrap;
            }
            #Branch_Form_Table td{
                padding: 10px 0px;
            }
            .function_btn{
                margin: 0px 5px;
            }
            .ui-widget.ui-widget-content{
                border-radius: 20px;
                border-width: 20px;
                /* background-color: #DADADA; */
                border: 1px solid #DADADA;
            }
            .ui-widget-content {
                border-radius: 20px;
                border-width: 20px;
            }
            .ui-widget-overlay{
                /* background-color: transparent; */
            }
            .ui-dialog-titlebar{
                display: none
            }
            .BA_Input{
                width: 90%
            }
        </style>

    </head>

    <body onload="includeHTML();get_branches();">
        <div class='navbar-div' include-html="../navbar.php"></div>
        <div class='for_hyper left' include-html="../hyper.php"></div>
        <div class='right'>
            <center>
                <div>
                    <input type='button' class='function_btn' style='width:130px;height:50px;background-color:#F79B00;float:right;margin:15px 30px;' value='新增分店' onClick='Open_Branch_Dialog("New")'>
                </div>
                <div style='width:100%;'>
                    <center>
                        <table id='Branch_Table' width='95%' border='1' style='margin-bottom: 100px;'>
                            <tr>
                                <th>店名</th>
                                <th>地址</th>
                                <th>市話</th>
                                <th>手機</th>
                                <th>顏色</th>
                                <th>備註</th>
                                <th></th>
                            </tr>
                        </table>
                    </center>
                </div>
            </center>
        </div>
    </body>
</html>


<!----------Dialog---------->
    <div id='Branch_Dialog' name='dialog_section'><br>
        <center>
            <h2>新增/修改店家資訊</h2>
            <form id='Branch_Form'>
                <input type="hidden" name='Feature' value='IU_Branch' autofocus="true" />
                <input type="hidden" name='BA_ID' id='BA_ID' class='BA_Input' autofocus="true" />
                <table id='Branch_Form_Table' width='95%'>
                    <tr>
                        <td style='text-align:right;font-size:1.5vw;width:15%'>
                            店名：
                        </td>
                        <td style='font-size:1.5vw;'>
                            <input type='text' class='BA_Input' id='BA_Name' name='BA_Name' placeholder='請輸入店名' required>
                        </td>
                        
                        <td style='text-align:right;font-size:1.5vw;width:20%'>
                            地址：
                        </td>
                        <td style='font-size:1.5vw;'>
                            <input type='text' class='BA_Input' id='BA_Address' name='BA_Address' placeholder='請輸入地址' required>
                        </td>
                    </tr>
                    <tr>
                        <td style='text-align:right;font-size:1.5vw;'>
                        </td>
                        <td style='font-size:1.5vw;'>
                        </td>
                        
                        <td style='text-align:right;font-size:1.5vw;'>
                            地址備註：
                        </td>
                        <td style='font-size:1.5vw;'>
                            <input type='text' class='BA_Input' id='BA_Address_Remark' name='BA_Address_Remark' placeholder='請輸入地址備註'>
                        </td>
                    </tr>
                    <tr>
                        <td style='text-align:right;font-size:1.5vw;'>
                            市話：
                        </td>
                        <td style='font-size:1.5vw;'>
                            <input type='text' class='BA_Input' id='BA_Phone1' name='BA_Phone1' placeholder='請輸入市話' required>
                        </td>
                        
                        <td style='text-align:right;font-size:1.5vw;'>
                            手機：
                        </td>
                        <td style='font-size:1.5vw;'>
                            <input type='text' class='BA_Input' id='BA_Phone2' name='BA_Phone2' placeholder='請輸入手機' required>
                        </td>
                    </tr>
                    <tr>
                        <td style='text-align:right;font-size:1.5vw;'>
                            顏色：
                        </td>
                        <td style='font-size:1.5vw;'>
                            <select name='BA_Color' id='BA_Color' class='BA_Input' onchange='display_color(this.value)' required>
                                <option value=''>請選擇顏色</option>
                                <option value='aliceblue' style='background-color:aliceblue'>aliceblue</option>
                                <option value='antiquewhite' style='background-color:antiquewhite'>antiquewhite</option>
                                <option value='aqua' style='background-color:aqua'>aqua</option>
                                <option value='aquamarine' style='background-color:aquamarine'>aquamarine</option>
                                <option value='azure' style='background-color:azure'>azure</option>
                                <option value='beige' style='background-color:beige'>beige</option>
                                <option value='bisque' style='background-color:bisque'>bisque</option>
                                <option value='black' style='background-color:black'>black</option>
                                <option value='blanchedalmond' style='background-color:blanchedalmond'>blanchedalmond</option>
                                <option value='blue' style='background-color:blue'>blue</option>
                                <option value='blueviolet' style='background-color:blueviolet'>blueviolet</option>
                                <option value='brown' style='background-color:brown'>brown</option>
                                <option value='burlywood' style='background-color:burlywood'>burlywood</option>
                                <option value='cadetblue' style='background-color:cadetblue'>cadetblue</option>
                                <option value='chartreuse' style='background-color:chartreuse'>chartreuse</option>
                                <option value='chocolate' style='background-color:chocolate'>chocolate</option>
                                <option value='coral' style='background-color:coral'>coral</option>
                                <option value='cornflowerblue' style='background-color:cornflowerblue'>cornflowerblue</option>
                                <option value='cornsilk' style='background-color:cornsilk'>cornsilk</option>
                                <option value='crimson' style='background-color:crimson'>crimson</option>
                                <option value='cyan' style='background-color:cyan'>cyan</option>
                                <option value='darkblue' style='background-color:darkblue'>darkblue</option>
                                <option value='darkcyan' style='background-color:darkcyan'>darkcyan</option>
                                <option value='darkgoldenrod' style='background-color:darkgoldenrod'>darkgoldenrod</option>
                                <option value='dark-gray' style='background-color:dark-gray'>dark-gray</option>
                                <option value='darkgreen' style='background-color:darkgreen'>darkgreen</option>
                                <option value='dark-grey' style='background-color:dark-grey'>dark-grey</option>
                                <option value='darkkhaki' style='background-color:darkkhaki'>darkkhaki</option>
                                <option value='darkmagenta' style='background-color:darkmagenta'>darkmagenta</option>
                                <option value='darkolivegreen' style='background-color:darkolivegreen'>darkolivegreen</option>
                                <option value='darkorange' style='background-color:darkorange'>darkorange</option>
                                <option value='darkorchid' style='background-color:darkorchid'>darkorchid</option>
                                <option value='darkred' style='background-color:darkred'>darkred</option>
                                <option value='darksalmon' style='background-color:darksalmon'>darksalmon</option>
                                <option value='darkseagreen' style='background-color:darkseagreen'>darkseagreen</option>
                                <option value='darkslateblue' style='background-color:darkslateblue'>darkslateblue</option>
                                <option value='darkslategray' style='background-color:darkslategray'>darkslategray</option>
                                <option value='darkslategrey' style='background-color:darkslategrey'>darkslategrey</option>
                                <option value='darkturquoise' style='background-color:darkturquoise'>darkturquoise</option>
                                <option value='darkviolet' style='background-color:darkviolet'>darkviolet</option>
                                <option value='deeppink' style='background-color:deeppink'>deeppink</option>
                                <option value='deepskyblue' style='background-color:deepskyblue'>deepskyblue</option>
                                <option value='dimgray' style='background-color:dimgray'>dimgray</option>
                                <option value='dimgrey' style='background-color:dimgrey'>dimgrey</option>
                                <option value='dodgerblue' style='background-color:dodgerblue'>dodgerblue</option>
                                <option value='firebrick' style='background-color:firebrick'>firebrick</option>
                                <option value='floralwhite' style='background-color:floralwhite'>floralwhite</option>
                                <option value='forestgreen' style='background-color:forestgreen'>forestgreen</option>
                                <option value='fuchsia' style='background-color:fuchsia'>fuchsia</option>
                                <option value='gainsboro' style='background-color:gainsboro'>gainsboro</option>
                                <option value='ghostwhite' style='background-color:ghostwhite'>ghostwhite</option>
                                <option value='gold' style='background-color:gold'>gold</option>
                                <option value='goldenrod' style='background-color:goldenrod'>goldenrod</option>
                                <option value='gray' style='background-color:gray'>gray</option>
                                <option value='green' style='background-color:green'>green</option>
                                <option value='greenyellow' style='background-color:greenyellow'>greenyellow</option>
                                <option value='grey' style='background-color:grey'>grey</option>
                                <option value='honeydew' style='background-color:honeydew'>honeydew</option>
                                <option value='hotpink' style='background-color:hotpink'>hotpink</option>
                                <option value='indianred' style='background-color:indianred'>indianred</option>
                                <option value='indigo' style='background-color:indigo'>indigo</option>
                                <option value='ivory' style='background-color:ivory'>ivory</option>
                                <option value='khaki' style='background-color:khaki'>khaki</option>
                                <option value='lavender' style='background-color:lavender'>lavender</option>
                                <option value='lavenderblush' style='background-color:lavenderblush'>lavenderblush</option>
                                <option value='lawngreen' style='background-color:lawngreen'>lawngreen</option>
                                <option value='lemonchiffon' style='background-color:lemonchiffon'>lemonchiffon</option>
                                <option value='lightblue' style='background-color:lightblue'>lightblue</option>
                                <option value='lightcoral' style='background-color:lightcoral'>lightcoral</option>
                                <option value='lightcyan' style='background-color:lightcyan'>lightcyan</option>
                                <option value='lightgoldenrodyellow' style='background-color:lightgoldenrodyellow'>lightgoldenrodyellow</option>
                                <option value='lightgray' style='background-color:lightgray'>lightgray</option>
                                <option value='lightgreen' style='background-color:lightgreen'>lightgreen</option>
                                <option value='lightgrey' style='background-color:lightgrey'>lightgrey</option>
                                <option value='lightpink' style='background-color:lightpink'>lightpink</option>
                                <option value='lightsalmon' style='background-color:lightsalmon'>lightsalmon</option>
                                <option value='lightseagreen' style='background-color:lightseagreen'>lightseagreen</option>
                                <option value='lightskyblue' style='background-color:lightskyblue'>lightskyblue</option>
                                <option value='lightslategray' style='background-color:lightslategray'>lightslategray</option>
                                <option value='lightslategrey' style='background-color:lightslategrey'>lightslategrey</option>
                                <option value='lightsteelblue' style='background-color:lightsteelblue'>lightsteelblue</option>
                                <option value='lightyellow' style='background-color:lightyellow'>lightyellow</option>
                                <option value='lime' style='background-color:lime'>lime</option>
                                <option value='limegreen' style='background-color:limegreen'>limegreen</option>
                                <option value='linen' style='background-color:linen'>linen</option>
                                <option value='magenta' style='background-color:magenta'>magenta</option>
                                <option value='maroon' style='background-color:maroon'>maroon</option>
                                <option value='mediumaquamarine' style='background-color:mediumaquamarine'>mediumaquamarine</option>
                                <option value='mediumblue' style='background-color:mediumblue'>mediumblue</option>
                                <option value='mediumorchid' style='background-color:mediumorchid'>mediumorchid</option>
                                <option value='mediumpurple' style='background-color:mediumpurple'>mediumpurple</option>
                                <option value='mediumseagreen' style='background-color:mediumseagreen'>mediumseagreen</option>
                                <option value='mediumslateblue' style='background-color:mediumslateblue'>mediumslateblue</option>
                                <option value='mediumspringgreen' style='background-color:mediumspringgreen'>mediumspringgreen</option>
                                <option value='mediumturquoise' style='background-color:mediumturquoise'>mediumturquoise</option>
                                <option value='mediumvioletred' style='background-color:mediumvioletred'>mediumvioletred</option>
                                <option value='midnightblue' style='background-color:midnightblue'>midnightblue</option>
                                <option value='mintcream' style='background-color:mintcream'>mintcream</option>
                                <option value='mistyrose' style='background-color:mistyrose'>mistyrose</option>
                                <option value='moccasin' style='background-color:moccasin'>moccasin</option>
                                <option value='navajowhite' style='background-color:navajowhite'>navajowhite</option>
                                <option value='navy' style='background-color:navy'>navy</option>
                                <option value='oldlace' style='background-color:oldlace'>oldlace</option>
                                <option value='olive' style='background-color:olive'>olive</option>
                                <option value='olivedrab' style='background-color:olivedrab'>olivedrab</option>
                                <option value='orange' style='background-color:orange'>orange</option>
                                <option value='orangered' style='background-color:orangered'>orangered</option>
                                <option value='orchid' style='background-color:orchid'>orchid</option>
                                <option value='palegoldenrod' style='background-color:palegoldenrod'>palegoldenrod</option>
                                <option value='palegreen' style='background-color:palegreen'>palegreen</option>
                                <option value='paleturquoise' style='background-color:paleturquoise'>paleturquoise</option>
                                <option value='palevioletred' style='background-color:palevioletred'>palevioletred</option>
                                <option value='papayawhip' style='background-color:papayawhip'>papayawhip</option>
                                <option value='peachpuff' style='background-color:peachpuff'>peachpuff</option>
                                <option value='peru' style='background-color:peru'>peru</option>
                                <option value='pink' style='background-color:pink'>pink</option>
                                <option value='plum' style='background-color:plum'>plum</option>
                                <option value='powderblue' style='background-color:powderblue'>powderblue</option>
                                <option value='purple' style='background-color:purple'>purple</option>
                                <option value='rebeccapurple' style='background-color:rebeccapurple'>rebeccapurple</option>
                                <option value='red' style='background-color:red'>red</option>
                                <option value='rosybrown' style='background-color:rosybrown'>rosybrown</option>
                                <option value='royalblue' style='background-color:royalblue'>royalblue</option>
                                <option value='saddlebrown' style='background-color:saddlebrown'>saddlebrown</option>
                                <option value='salmon' style='background-color:salmon'>salmon</option>
                                <option value='sandybrown' style='background-color:sandybrown'>sandybrown</option>
                                <option value='seagreen' style='background-color:seagreen'>seagreen</option>
                                <option value='seashell' style='background-color:seashell'>seashell</option>
                                <option value='sienna' style='background-color:sienna'>sienna</option>
                                <option value='silver' style='background-color:silver'>silver</option>
                                <option value='skyblue' style='background-color:skyblue'>skyblue</option>
                                <option value='slateblue' style='background-color:slateblue'>slateblue</option>
                                <option value='slategray' style='background-color:slategray'>slategray</option>
                                <option value='slategrey' style='background-color:slategrey'>slategrey</option>
                                <option value='snow' style='background-color:snow'>snow</option>
                                <option value='springgreen' style='background-color:springgreen'>springgreen</option>
                                <option value='steelblue' style='background-color:steelblue'>steelblue</option>
                                <option value='tan' style='background-color:tan'>tan</option>
                                <option value='teal' style='background-color:teal'>teal</option>
                                <option value='thistle' style='background-color:thistle'>thistle</option>
                                <option value='tomato' style='background-color:tomato'>tomato</option>
                                <option value='turquoise' style='background-color:turquoise'>turquoise</option>
                                <option value='violet' style='background-color:violet'>violet</option>
                                <option value='wheat' style='background-color:wheat'>wheat</option>
                                <option value='white' style='background-color:white'>white</option>
                                <option value='whitesmoke' style='background-color:whitesmoke'>whitesmoke</option>
                                <option value='yellow' style='background-color:yellow'>yellow</option>
                                <option value='yellowgreen' style='background-color:yellowgreen'>yellowgreen</option>
                            </select>
                        </td>
                        <td id='display_color'>
                        </td>
                        <td>
                        </td>
                    </tr>
                    <tr>
                        <td style='text-align:right;font-size:1.5vw;vertical-align:top'>
                            備註：
                        </td>
                        <td style='font-size:1.5vw;' colspan='3'>
                            <textarea class='BA_Input' id='BA_Remark' name='BA_Remark' style='width: 90%;height: 200px;'></textarea>
                        </td>
                    </tr>
                </table>
                <input type='submit' class='function_btn' style='background-color:#0091FF;font-size:24px;' value='新增/修改'>
                <input type='button' class='function_btn' style='background-color:tomato;font-size:24px;' value='取消' onClick='Close_Dialog()'>
            </form>
        </center>
    </div>
<!-------------------------->

<script>

    $(document).ready(function() {
        $("#Branch_Dialog").dialog({
            // height: 750,
            width: 850,
            autoResize:true,
            autoOpen: false,
            resizable: false,
            draggable: false,
            modal: true
            // dialogClass: 'ui-dialog-osx',
        });

        $("#Branch_Form").submit(function(){
            event.preventDefault();
            $.ajax({
                type: "POST",
                url: "service.php",
                data: $("#Branch_Form").serialize(),
                success: function(data) {
                    if(data.Success){
                        get_branches();
                        Close_Dialog();
                        console.log('RRRR');
                    }
                },
                error: function(jqXHR) {
                    console.log("error: " + jqXHR.responseText);
                }
            })
        });
    });

    $("body").on("click",".ui-widget-overlay",function() {
        Close_Dialog();
    });

    function get_branches(){
        $.ajax({
            type: "POST",
            url: "service.php",
            dataType: "json",
            data: {
                Feature: "get_branches"
            },
            success: function(data) {
                let rows = "<tr><th>店名</th><th>地址</th><th>市話</th><th>手機</th><th>顏色</th><th>備註</th><th></th></tr>";
                for(let i=0;i<data.length;i++){
                    rows += "<tr><td>"+data[i].BA_Name+"</td><td><p>"+data[i].BA_Address;
                    if(data[i].BA_Address_Remark!="")
                        rows += "<br>("+data[i].BA_Address_Remark+")";
                    rows += "</p></td><td>"+data[i].BA_Phone1+"</td><td>"+data[i].BA_Phone2+"</td><td style='background-color:"+data[i].BA_Color+";'></td><td><span style='text-overflow:ellipsis;overflow:hidden;width:95%;display:inline-block;white-space:unset'>"+data[i].BA_Remark+"</span></td><td><button class='function_btn' style='background-color: #0091FF;' onclick='Open_Branch_Dialog(\"EDIT_"+data[i].BA_ID+"\")'>編輯</button><button class='function_btn' onclick='Open_Branch_Dialog(\"DEL_"+data[i].BA_ID+"\")' style='background-color: RED;'>刪除</button></td></tr>";
                }
                $("#Branch_Table").html(rows);
            },
            error: function(jqXHR) {
                $("#result").html('<font color="#ff0000">發生錯誤：GB</font>');
                console.log(jqXHR.responseText);
            }
        })
    }

    function Open_Branch_Dialog(action){
        if(action == "New"){
            $("#Branch_Dialog").dialog("open");
        }
        else{
            let Order_ID = action.split("_")[1];
            action = action.split("_")[0];
            switch(action){
                case "EDIT":
                    $.ajax({
                        type: "POST",
                        url: "service.php",
                        dataType: "json",
                        data: {
                            Feature: "get_specific_branches",
                            Order_ID: Order_ID
                        },
                        success: function(data) {
                            $("#BA_ID").val(data.BA_ID);
                            $("#BA_Name").val(data.BA_Name);
                            $("#BA_Address").val(data.BA_Address);
                            $("#BA_Address_Remark").val(data.BA_Address_Remark);
                            $("#BA_Phone1").val(data.BA_Phone1);
                            $("#BA_Phone2").val(data.BA_Phone2);
                            $("#BA_Color").val(data.BA_Color);
                            display_color(data.BA_Color);
                            $("#BA_Remark").val(data.BA_Remark);

                            $("#Branch_Dialog").dialog("open");
                        },
                        error: function(jqXHR) {
                            $("#result").html('<font color="#ff0000">發生錯誤：GB</font>');
                            console.log(jqXHR.responseText);
                        }
                    })
                break;
                
                case "DEL":
                    $.ajax({
                        type: "POST",
                        url: "service.php",
                        dataType: "json",
                        data: {
                            Feature: "delete_branch",
                            Order_ID: Order_ID
                        },
                        success: function(data) {
                            get_branches();
                        },
                        error: function(jqXHR) {
                            console.log("error: " + jqXHR.responseText);
                        }
                    })
                break;
            }
        }
    }

    function display_color(color){
        $("#display_color").css("background-color", color);
    }

    function Close_Dialog(){
        let Targets = document.getElementsByName('dialog_section');
        for(i=0;i<Targets.length;i++)
            $('#'+Targets[i].id).dialog( "close" );
        Targets = document.getElementsByClassName('BA_Input');
        for(i=0;i<Targets.length;i++){
            if(Targets[i].tagName == "SELECT")
                Targets[i].selectedIndex = 0;
            else
                Targets[i].value = "";
        }
        $("#display_color").css("background-color", "");
    }
</script>