<?php
    header('Content-Type: application/json; charset=UTF-8');
    include_once "../mysql_connect.inc.php";
    // error_reporting(0);
    if ($_SERVER['REQUEST_METHOD'] == "POST") {
        if(isset($_POST)){
            switch($_POST['Feature']){
                case 'get_branches':
                    get_branches($conn);
                break;

                case 'get_specific_branches':
                    $sql = "SELECT * FROM `branch_allocation` WHERE `BA_ID`='".$_POST['Order_ID']."'";
                    echo json_encode(mysqli_query($conn,$sql)->fetch_assoc(),JSON_UNESCAPED_UNICODE);
                break;

                case 'IU_Branch':
                    if(isset($_POST['BA_ID'])&&!empty($_POST['BA_ID'])){
                        $sql = "UPDATE `branch_allocation` SET `BA_Name`='".$_POST['BA_Name']."',`BA_Address`='".$_POST['BA_Address']."',`BA_Address_Remark`='".$_POST['BA_Address_Remark']."',`BA_Phone1`='".$_POST['BA_Phone1']."',`BA_Phone2`='".$_POST['BA_Phone2']."',`BA_Color`='".$_POST['BA_Color']."',`BA_Remark`='".$_POST['BA_Remark']."' WHERE `BA_ID`='".$_POST['BA_ID']."'";
                        if(!mysqli_query($conn,$sql)){
                            echo "SQL Error: BM_IU_EDIT";
                            die;
                        }
                    }
                    else{
                        $sql = "INSERT INTO `branch_allocation`(`BA_Name`, `BA_Address`, `BA_Address_Remark`, `BA_Phone1`, `BA_Phone2`, `BA_Color`, `BA_Remark`) VALUES ('".$_POST['BA_Name']."', '".$_POST['BA_Address']."', '".$_POST['BA_Address_Remark']."', '".$_POST['BA_Phone1']."', '".$_POST['BA_Phone2']."', '".$_POST['BA_Color']."', '".$_POST['BA_Remark']."')";
                        if(!mysqli_query($conn,$sql)){
                            echo "SQL Error: BM_IU_NEW";
                            die;
                        }
                    }
                    echo json_encode(array('Success' => true),JSON_UNESCAPED_UNICODE);
                break;

                case 'delete_branch':
                    $sql = "DELETE FROM `branch_allocation` WHERE `BA_ID`='".$_POST['Order_ID']."'";
                    if(!mysqli_query($conn,$sql)){
                        echo "SQL Error: BM_IU_DEL";
                        die;
                    }
                    echo json_encode(array('Success' => true),JSON_UNESCAPED_UNICODE);
                break;
            }
        }
    }

    function get_branches($conn){
        $branch_array = array();
        $sql = "SELECT * FROM `branch_allocation` WHERE 1";
        $result = mysqli_query($conn,$sql);
        while($row = $result->fetch_assoc()){
            $branch_array[] = $row;
        }
        echo json_encode($branch_array,JSON_UNESCAPED_UNICODE);
    }
?>