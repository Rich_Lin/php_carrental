-- phpMyAdmin SQL Dump
-- version 4.8.3
-- https://www.phpmyadmin.net/
--
-- 主機: 127.0.0.1
-- 產生時間： 2020 年 11 朁E24 日 07:55
-- 伺服器版本: 10.1.37-MariaDB
-- PHP 版本： 7.2.12

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET AUTOCOMMIT = 0;
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- 資料庫： `car_rental`
--

-- --------------------------------------------------------

--
-- 資料表結構 `branch_allocation`
--

CREATE TABLE `branch_allocation` (
  `BA_ID` int(11) NOT NULL,
  `BA_Name` varchar(20) NOT NULL,
  `BA_Address` varchar(255) NOT NULL,
  `BA_Address_Remark` varchar(100) NOT NULL,
  `BA_Phone1` varchar(20) NOT NULL,
  `BA_Phone2` varchar(20) NOT NULL,
  `BA_Color` varchar(30) NOT NULL,
  `BA_Remark` varchar(255) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- 資料表的匯出資料 `branch_allocation`
--

INSERT INTO `branch_allocation` (`BA_ID`, `BA_Name`, `BA_Address`, `BA_Address_Remark`, `BA_Phone1`, `BA_Phone2`, `BA_Color`, `BA_Remark`) VALUES
(1, '桃園春日', '桃園市桃園區春日路1200號', '', '03-378-2223', '0973-228-956', 'forestgreen', ''),
(2, '桃園車站', '桃園市桃園區吉林路38號', '桃園後火車站旁', '03-378-2223', '0973-228-956', 'crimson', ''),
(3, '蘆竹南崁', '桃園市蘆竹區中正北路399號', '風車餐廳對面，車欣汽車廣場內', '03-378-2223', '0973-228-956', 'deepskyblue', ''),
(4, '桃園平鎮', '桃園市平鎮區延平路三段258號', '', '03-491-6323', '0973-228-956', 'yellowgreen', ''),
(5, '林口龜山', '桃園市龜山區文化一路 86-27號', '華亞科技園區', '03-318-0757', '0973-228-956', 'yellow', ''),
(6, '桃園內壢', '桃園市中壢區吉林二路79巷30號', '', '03-378-2223', '0973-228-956', 'sandybrown', ''),
(7, '新北蘆洲', '新北市蘆洲區民族路550號', '', '02-2285-6001', '0973-228-956', 'peru', ''),
(8, '八德', '桃園市八德區建國路247號', '', '03-378-2223', '0973-228-956', 'palevioletred', ''),
(9, '八德忠勇', '桃園市八德區忠勇一街92號', '', '03-378-2223', '0973-228-956', 'fuchsia', '');

-- --------------------------------------------------------

--
-- 資料表結構 `car_list`
--

CREATE TABLE `car_list` (
  `Company_Name` varchar(255) NOT NULL,
  `Vehicle_Inspection` varchar(255) NOT NULL,
  `License_Plate` varchar(10) NOT NULL,
  `Allocation` int(11) NOT NULL,
  `Car_Type` varchar(255) NOT NULL,
  `Displacement` int(11) NOT NULL,
  `Manufacturing_Date` varchar(7) NOT NULL,
  `Color` varchar(20) NOT NULL,
  `ISOFIX` tinyint(1) NOT NULL,
  `Equipment` varchar(255) NOT NULL,
  `Car_Style` varchar(255) NOT NULL,
  `Model` varchar(255) NOT NULL,
  `Engine_Number` varchar(255) NOT NULL,
  `Body_Number` varchar(255) NOT NULL,
  `Registration` int(11) NOT NULL,
  `Current_Location` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- 資料表的匯出資料 `car_list`
--

INSERT INTO `car_list` (`Company_Name`, `Vehicle_Inspection`, `License_Plate`, `Allocation`, `Car_Type`, `Displacement`, `Manufacturing_Date`, `Color`, `ISOFIX`, `Equipment`, `Car_Style`, `Model`, `Engine_Number`, `Body_Number`, `Registration`, `Current_Location`) VALUES
('華誼租賃有限公司', '8月19日', 'RAE-7856', 2, 'STAREX', 2497, '2013.07', '灰', 0, '', '廂式', 'STAREX', 'D4CBD324512', 'KMHWG81KBDU585202', 0, 4),
('華誼租賃有限公司', '2月3日', 'RAE-8971', 2, 'VERNA 16', 1591, '2016.01', '白', 0, '', '轎式', 'PBG-A', 'G4FCFU669129', 'RFHCS41DBGS000051', 0, 1),
('華誼租賃有限公司', '2月3日', 'RAE-8977', 2, 'FIT', 1497, '2016.01', '白', 0, '藍芽、倒車雷達、AUX、USB、12V', '轎式', 'FIT 1.5 VTi', 'L15Z42601457', 'BKTGK5850GF101455', 0, 1),
('華誼優盟租賃有限公司', '2月27日', 'RAM-7080', 2, 'STAREX', 2497, '2014.05', '黑', 0, '', '廂式', 'STAREX', 'D4CBE510549', 'KMHWG81KBEU652336', 0, 5),
('華誼優盟租賃有限公司', '3月2日', 'RAM-7090', 2, 'STAREX', 2497, '2014.04', '黑', 0, '', '廂式', 'STAREX', 'D4CBE507204', 'KMHWG81KBEU650280', 0, 6),
('行通租賃有限公司', '', 'RAM-7196', 2, 'U6 16', 1798, '2014.11', '白', 0, '', '旅行式', '無', '無', '無', 0, 9),
('華誼租賃有限公司', '', 'RAM-7315', 2, 'FIENTA', 1591, '', '白', 0, '', '轎式 5D', '', '', '', 0, 9),
('華誼優盟租賃有限公司', '3月31日', 'RAM-7521', 2, 'T5', 1968, '2015.01', '黑', 0, '', '廂式', 'CARAVELLE L 2.0 TDI', '無', 'WV2ZZZ7HZFH098293', 0, 7),
('行通租賃有限公司', '', 'RAM-8667', 2, 'M7', 2198, '2016.01', '白', 0, '倒車顯影、AUX、USB、110V、12V、AV端子', '廂式', '無', '無', '無', 0, 7),
('華誼優盟租賃有限公司', '12月23日', 'RAM-9857', 2, 'WISH', 1987, '2016.06', '白', 0, '', '轎式', 'ZGE21L-JPXJKR', '3ZRX581895', 'ZGE21~6107654', 0, 4),
('華誼租賃有限公司', '12月23日', 'RAM-9860', 2, 'WISH', 1987, '2016.06', '白', 0, '', '轎式', 'ZGE21L-JPXJKR', '3ZRX581954', 'ZGE21~6107647', 0, 5),
('華誼租賃有限公司', '', 'RAM-9861', 2, 'WISH', 1987, '2016.06', '白', 0, '', '轎式', 'ZGE21L-JPXJKR', '3ZRX582018', 'ZGE21~6107709', 0, 6),
('華誼優盟租賃有限公司', '12月23日', 'RAM-9862', 2, 'WISH', 1987, '2016.06', '白', 0, '', '轎式', 'ZGE21L-JPXJKR', '3ZRX582006', 'ZGE21~6107679', 0, 7),
('華誼租賃有限公司', '10/28 下次4/28', 'RAR-5315', 2, 'PREVIA', 2362, '2014.07', '黑', 0, '', '旅行式 HID頭燈', 'PREVIA', '無', 'JTEGD54M20A045492', 0, 9),
('華誼優質租賃有限公司', '9月21日', 'RAS-8101', 2, 'FIT', 1497, '2016.09', '白', 0, '藍芽、倒車雷達、AUX、USB、12V', '轎式', 'FIT 1.5 VTi', 'L15Z43601869', 'PKTGK5850HF001867', 0, 2),
('華誼優質租賃有限公司', '7月30日', 'RBU-1290', 2, 'NEW VIOS', 1496, '2019.05', '白', 0, '藍芽、倒車雷達、AUX、USB、12V', '轎式', 'NSP151L-FEXDKR', '2NRX457158', 'NSP151~4072731', 0, 9),
('華誼優質租賃有限公司', '7月30日', 'RBU-1291', 2, 'NEW YARIS', 1496, '2019.07', '白', 0, '藍芽、倒車雷達、12V', '轎式', 'NSP151L-FHXGKR', '2NRX464023', 'NSP151~4074592', 0, 6),
('華誼優質租賃有限公司', '7月30日', 'RBU-1292', 2, 'NEW ALTIS', 1798, '2019.07', '黑', 0, '', '轎式', 'ZRE211L-GEXEKR', '2ZRY577171', 'ZRE211~0011337', 0, 5),
('華誼優質租賃有限公司', '7月30日', 'RBU-1293', 2, 'NEW ALTIS', 1798, '2019.07', '白', 0, '', '轎式', 'ZRE211L-GEXEKR', '2ZRY577379', 'ZRE211~0011360', 0, 6),
('華誼優質租賃有限公司', '7月30日', 'RBU-1295', 2, 'NEW VIOS', 1496, '2019.07', '白', 0, '藍芽、倒車雷達、AUX、USB、12V', '轎式', 'NSP151L-FEXDKR', '2NRX469203', 'NSP151~4075636', 0, 1),
('華誼優質租賃有限公司', '7月30日', 'RBU-1296', 2, 'NEW YARIS', 1496, '2019.07', '白', 0, '藍芽、倒車雷達、12V', '轎式', 'NSP151L-FHXRKR', '2NRX467152', 'NSP151~4075321', 0, 7),
('華誼優質租賃有限公司', '7月30日', 'RBU-1297', 2, 'MAZDA3', 1998, '2019.06', '深紅', 0, '倒車顯影、藍芽、CarPlay、Android Auto、AUX、USB', '轎式 LED頭燈 4D', 'Mazda3', '無', 'JM7BP2S7A01101368', 0, 9),
('華誼優質租賃有限公司', '7月30日', 'RBU-1298', 2, 'MAZDA3 尊榮版', 1998, '2019.06', '4D灰', 0, '倒車顯影、藍芽、CarPlay、Android Auto、AUX、USB', '轎式 LED頭燈 4D', 'Mazda3', '無', 'JM7BP2S7A01102398', 0, 1),
('華誼優質租賃有限公司', '7月30日', 'RBU-1300', 2, 'NEW ELANTRA', 1591, '2019.07', '白', 0, '倒車雷達、USB(不支援蘋果撥放)、AUX', '轎式', 'ELANTRA ADG-1', 'G4FGKU138484', 'RFHD141CBLS000011', 0, 7),
('華誼優質租賃有限公司', '7月30日', 'RBU-1301', 2, 'KICKS', 1498, '2019.07', '灰', 0, '倒車顯影、藍芽、CarPlay、Android Auto、AUX、USB', '旅行式', 'KICKS P15 FVA', 'HR15011693T', 'P15FVA011555', 0, 5),
('華誼優盟租賃有限公司', '7月28日', 'RBU-6203', 2, 'SIENTA', 1798, '2017.03', '白', 0, '藍芽、倒車顯影、AUX、USB、右側電動滑門', '旅行式 LED光型', 'ZSP170L-MWXQPR', '2ZR1931872', 'ZSP170~0008346', 0, 2),
('華誼優盟租賃有限公司', '7月28日', 'RBU-6206', 2, 'SIENTA', 1798, '2017.03', '白', 0, '藍芽、倒車顯影、AUX、USB、右側電動滑門', '旅行式 LED光型', 'ZSP170L-MWXQPR', '2ZR1931237', 'ZSP170~0008270', 0, 3),
('華誼租賃有限公司', '9月7日', 'RBU-6309', 2, 'VIOS', 1496, '2016.06', '灰', 0, '', '轎式', 'NSP151L-FEXRKR', '2NRX018759', 'NSP151~4005114', 0, 2),
('華誼租賃有限公司', '9月30日', 'RBU-6395', 2, 'ELANTRA', 1591, '2017.08', '白', 0, '', '轎式', 'ELANTRA ADG-A', 'G4FGHU713535', 'RFHDA41CBHS000936', 0, 7),
('華誼國際租賃有限公司', '9月30日', 'RBU-6397', 2, 'VIOS', 1496, '2017.07', '黑', 0, '', '轎式', 'NSP151L-BEXDKR', '2NRX187055', 'NSP151~4031484', 0, 3),
('華誼優質租賃有限公司', '9月30日', 'RBU-6502', 2, 'YARIS', 1496, '2017.08', '白', 0, '藍芽、倒車雷達、12V、盲點偵測', '轎式', 'NSP151L-AHXGKR', 'NSP151~4032193', '2NRX191010', 0, 9),
('華誼優質租賃有限公司', '9月30日', 'RBU-6505', 2, 'ELANTRA', 1591, '2017.07', '黑', 0, '', '轎式', 'ELANTRA ADG-A', 'G4FGHU713604', 'RFHDA41CBHS000687', 0, 8),
('華誼優質租賃有限公司', '9月30日', 'RBU-6506', 2, 'MAZDA3 豪華進化版4D', 1998, '2017.08', '4D白', 0, '藍芽、USB、AUX、', '轎式 4D', 'Mazda3', '無', 'JM7BN327X01183574', 0, 2),
('華誼優質租賃有限公司', '9月30日', 'RBU-6508', 2, 'MAZDA3 豪華進化版5D', 1998, '2017.08', '5D灰', 0, '藍芽、USB、AUX、', '轎式 5D', 'Mazda3', '無', 'JM7BN347201181766', 0, 3),
('華誼租賃有限公司', '9月30日', 'RBU-6509', 2, 'HR-V', 1799, '2017.09', '白', 0, '藍芽、倒車雷達、AUX、USB、12V', '轎式', 'HR-V 1.8 VTi', 'R18ZJ1612470', 'RKTRU5830HF012471', 0, 4),
('華誼租賃有限公司', '1月25日', 'RBU-6960', 2, 'S3', 1556, '2016.05', '黑', 0, '藍芽、倒車雷達、AUX、USB、12V、環景攝影', '轎式', 'S61FPA', 'A16NAAF*G0074308*', 'S61FPAA00110Y', 0, 1),
('華誼租賃有限公司', '8月2日', 'RBU-7920', 2, 'WISH 頂級', 1987, '2016.06', '灰', 0, '藍芽、倒車雷達、AUX、USB、12V、導航、抬頭顯示器', '轎式', 'ZGE21L-JPXEKR', '3ZRX583204', 'ZCE21~6108216', 0, 8),
('華誼優盟租賃有限公司', '8月10日', 'RBU-8026', 2, 'MAZDA3 豪華進化版5D', 1998, '2018.04', '5D灰', 0, '藍芽、USB、AUX、', '轎式 4D', 'Mazda3', '無', 'JM7BN347101221982', 0, 4),
('華誼租賃有限公司', '3月17日', 'RBU-9638', 2, 'CITY 15', 1497, '2015.05', '白', 0, '', '轎式', 'CITY 1.5 VTI', 'L15Z11600903', 'RKTGM6660FF000910', 0, 5),
('華誼租賃有限公司', '9月4日', 'RCE-0087', 2, 'ALTIS', 1798, '2019.01', '白', 0, 'AUX、USB、12V、倒車雷達', '轎式', 'ZRE172L-GEXEKR', '2ZRY543774', 'ZRE172~5711491', 0, 1),
('華誼優盟租賃有限公司', '2月13日', 'RCE-0725', 2, 'CARENS', 1999, '2018.01', '深藍', 0, '藍芽、AUX、USB、12V、倒車顯影、導航', '旅行式', 'RPG20-B20', 'G4NAHH301482', 'RFHHK512BIS000006', 0, 4),
('華誼優質租賃有限公司', '2月20日', 'RCE-0733', 2, 'CR-V', 1997, '2015.08', '灰', 0, '', '旅行式 LED光型', 'CR-V 2.0 VTi', 'R20A59280721', 'RTRM1830GF100712', 0, 6),
('華誼租賃有限公司', '3月30日', 'RCE-0855', 2, 'NEW ELANTRA', 1591, '2020.03', '白', 0, '倒車雷達、USB(不支援蘋果撥放)、AUX', '轎式', 'ELANTRA ADG-1', 'G4FGKU001172', 'RFHD141CBLS000208', 0, 8),
('華誼租賃有限公司', '6月17日', 'RCE-1070', 2, 'LIVINA', 1598, '2019.01', '灰', 0, '', '轎式', 'LIVINA L11 GM', 'HR16748561C', 'L11GMA041423', 0, 6),
('華誼租賃有限公司', '6月23日', 'RCE-1093', 2, 'NEW VIOS', 1496, '2019.02', '白', 0, '藍芽、倒車雷達、AUX、USB、12V', '轎式', 'NSP151L-FEXDKR', '2NRX423210', 'NSP151~4067020', 0, 2),
('華誼優盟租賃有限公司', '8月10日', 'RCE-6271', 2, 'M7', 2198, '2018.08', '黑', 0, '倒車顯影、AUX、USB、110V、12V、AV端子', '廂式', 'L92SDCA', 'G22TGA0008746', 'L92SDCA004436', 0, 8),
('華誼優盟租賃有限公司', '8月10日', 'RCE-6272', 2, 'ALTIS', 1798, '2018.06', '白', 0, '', '轎式', 'ZRE172-GEXDKR', '2ZRY492110', 'ZRE172~5699330', 0, 2),
('華誼優盟租賃有限公司', '8月10日', 'RCE-6273', 2, 'NEW VIOS', 1496, '2018.08', '白', 0, '藍芽、倒車雷達、AUX、USB、12V', '轎式', 'NSP151L-FEXDKR', '2NRX347486', 'NSP151~4054159', 0, 3),
('華誼優盟租賃有限公司', '8月10日', 'RCE-6278', 2, 'FIT 頂級', 1497, '2018.08', '白', 0, '', '轎式', 'FIT 1.5 S', 'L15Z44605135', 'BKTGK5880JF005165', 0, 3),
('華誼租賃有限公司', '3月5日', 'RCE-8011', 2, 'U5', 1556, '2018.02', '白', 0, '環景攝影、USB、藍芽、手機鏡像、盲點偵測', '轎式', 'H61FPA', 'UA16NAAF*H0241591*', 'H61FLAA003745', 0, 8),
('華誼聯合租賃有限公司', '9月28日', 'RCH-9152', 2, 'NEW YARIS', 1496, '2018.08', '白', 0, '藍芽、倒車雷達、12V', '轎式', 'NSP151L-FHXRKR', '2NRX354151', 'NSP151~4055527', 0, 8),
('華誼聯合租賃有限公司', '9月28日', 'RCH-9153', 2, 'NEW VIOS', 1496, '2018.09', '黑', 0, '藍芽、倒車雷達、AUX、USB、12V', '轎式', 'NSP151L-FEXDKR', '2NRX363331', 'NSP151~4056278', 0, 4),
('', '', 'RCH-9156', 2, 'NEW VIOS', 0, '', '白', 0, '藍芽、倒車雷達、AUX、USB、12V', '轎式', '', '', '', 0, 5),
('華誼優盟租賃有限公司', '6月4日', 'RCK-0308', 2, 'ALTIS 12', 1798, '2012.07', '白', 0, '', '轎式', 'ZRE142L-GEXEKR', '2ZRX205164', 'ZRE142~5225511', 0, 3);

-- --------------------------------------------------------

--
-- 資料表結構 `car_type`
--

CREATE TABLE `car_type` (
  `Car_ID` int(11) NOT NULL,
  `Car_Type` varchar(255) NOT NULL,
  `Car_Capacity` int(11) NOT NULL DEFAULT '4',
  `Weekday_Price` int(11) NOT NULL,
  `Weekend_Price` int(11) NOT NULL,
  `Halfday_Price` int(11) NOT NULL,
  `Weekly` int(11) NOT NULL,
  `Monthly` int(11) NOT NULL,
  `Remark` varchar(255) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- 資料表的匯出資料 `car_type`
--

INSERT INTO `car_type` (`Car_ID`, `Car_Type`, `Car_Capacity`, `Weekday_Price`, `Weekend_Price`, `Halfday_Price`, `Weekly`, `Monthly`, `Remark`) VALUES
(1, 'U6 16', 2, 2200, 2500, 1800, 1900, 1500, ''),
(2, 'M7', 4, 2800, 3300, 2300, 2300, 0, ''),
(3, 'VIOS', 4, 1400, 1700, 1000, 1100, 700, ''),
(4, 'FIENTA', 4, 1500, 1800, 1300, 1400, 1000, ''),
(5, 'NEW VIOS', 4, 1500, 1800, 1100, 1200, 800, ''),
(6, 'CITY 15', 4, 1600, 1900, 1200, 1300, 900, ''),
(7, 'FIT', 4, 1600, 1900, 1200, 1300, 900, ''),
(8, 'LIVINA', 4, 1600, 1900, 1200, 1300, 900, ''),
(9, 'S3', 4, 1600, 1900, 1200, 1300, 900, ''),
(10, 'VERNA 16', 4, 1600, 1900, 1200, 1300, 900, ''),
(11, 'ALTIS', 4, 1700, 2000, 1300, 1400, 1000, ''),
(12, 'ELANTRA', 4, 1800, 2100, 1500, 1500, 1100, ''),
(13, 'NEW ELANTRA', 4, 1900, 2200, 1600, 1600, 1200, ''),
(14, 'HR-V', 6, 2000, 2300, 1600, 1600, 1300, ''),
(15, 'U5', 4, 2000, 2300, 1600, 1600, 1300, ''),
(16, 'WISH', 4, 2200, 2500, 1800, 1900, 1500, ''),
(17, 'WISH 頂級', 44, 2300, 2600, 1900, 2000, 1600, ''),
(18, 'PREVIA', 4, 2800, 3300, 2300, 2300, 0, ''),
(19, 'STAREX', 4, 3000, 3500, 2500, 2500, 0, ''),
(20, 'ALTIS 12', 4, 1500, 1800, 1100, 1200, 800, ''),
(21, 'FIT 頂級', 4, 1700, 2000, 1300, 1400, 1000, ''),
(22, 'MAZDA3 豪華進化版5D', 4, 2000, 2300, 1600, 1700, 1300, ''),
(23, 'SIENTA', 4, 2000, 2300, 1600, 1600, 1300, ''),
(24, 'CARENS', 4, 2300, 2600, 1900, 2000, 1600, ''),
(25, 'T5', 4, 3500, 4000, 2800, 2800, 0, ''),
(26, 'YARIS', 4, 1500, 1800, 1100, 1200, 800, ''),
(27, 'NEW YARIS', 4, 1600, 1900, 1200, 1300, 900, ''),
(28, 'NEW ALTIS', 4, 1900, 2200, 1500, 1600, 1200, ''),
(29, 'MAZDA3 豪華進化版4D', 4, 1900, 2200, 1500, 1600, 1200, ''),
(30, 'CR-V', 4, 2200, 2500, 1800, 1900, 1500, ''),
(31, 'KICKS', 4, 2200, 2500, 1800, 1900, 1500, ''),
(32, 'MAZDA3 尊榮版', 4, 2200, 2500, 1800, 1900, 1500, ''),
(33, 'MAZDA3', 4, 0, 0, 0, 0, 0, '');

-- --------------------------------------------------------

--
-- 資料表結構 `company_list`
--

CREATE TABLE `company_list` (
  `Company_Name` varchar(255) NOT NULL,
  `Uniform_Number` varchar(10) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- 資料表的匯出資料 `company_list`
--

INSERT INTO `company_list` (`Company_Name`, `Uniform_Number`) VALUES
('華誼優盟租賃有限公司', '42963941'),
('華誼優質租賃有限公司', '82923376'),
('華誼國際租賃有限公司', '69682607'),
('華誼租賃有限公司', '54533616'),
('華誼聯合租賃有限公司', '50866062'),
('行通租賃有限公司', '24621893');

-- --------------------------------------------------------

--
-- 資料表結構 `customer_list`
--

CREATE TABLE `customer_list` (
  `Customer_ID` int(11) NOT NULL,
  `Customer_Name` varchar(25) NOT NULL,
  `Customer_SSID` varchar(15) NOT NULL,
  `Customer_Sex` tinyint(1) NOT NULL,
  `Customer_BDay` date NOT NULL,
  `Customer_Phone` varchar(20) NOT NULL,
  `Customer_Email` varchar(255) NOT NULL,
  `Customer_Address` varchar(500) NOT NULL,
  `Customer_Status` tinyint(1) NOT NULL DEFAULT '0',
  `Customer_Remark` varchar(255) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- 資料表的匯出資料 `customer_list`
--

INSERT INTO `customer_list` (`Customer_ID`, `Customer_Name`, `Customer_SSID`, `Customer_Sex`, `Customer_BDay`, `Customer_Phone`, `Customer_Email`, `Customer_Address`, `Customer_Status`, `Customer_Remark`) VALUES
(1, '游承恩', 'A123456789', 1, '1989-07-10', '0987877778', 'gginin@gmail.com', '然後他就死掉了', 1, '整天機機歪歪叫\n媽的畜生'),
(2, '帥哥嵐', 'A111111111', 1, '1994-06-08', '0963412092', '10night@gmail.com', '關你屁事', 0, ''),
(4, '狗狗', 'A159875342', 2, '2000-11-16', '01135867865', 'adlkja@gmail.com', 'RRRAAADWA', 0, ''),
(5, '蟲蟲', 'A223456789', 0, '1990-11-15', '015987365', 'mushi@yahoo.com.tw', '台北市士林區某個裡900鄰成德路某段9999-9999號999樓9999號房', 1, '呵呵蟲蟲是女的\n但是有屌\n所以蟲蟲是被%到雌墮\nㄏㄏㄏㄏㄏㄏㄏㄏ');

-- --------------------------------------------------------

--
-- 資料表結構 `employee_list`
--

CREATE TABLE `employee_list` (
  `Employee_ID` int(11) NOT NULL,
  `Employee_Name` varchar(255) NOT NULL,
  `L_Name` varchar(20) NOT NULL,
  `L_PWord` varchar(20) NOT NULL,
  `Employee_Sex` int(11) NOT NULL,
  `Employee_Phone` varchar(20) NOT NULL,
  `Employee_SSID` varchar(20) NOT NULL,
  `Employee_Address` varchar(500) NOT NULL,
  `Employee_Allocation` int(11) NOT NULL,
  `Employee_Type` int(11) NOT NULL,
  `Employee_Salary` int(11) NOT NULL,
  `Employee_Level` int(11) NOT NULL,
  `Employee_Remark` varchar(255) NOT NULL,
  `Enable` tinyint(1) NOT NULL DEFAULT '1'
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- 資料表的匯出資料 `employee_list`
--

INSERT INTO `employee_list` (`Employee_ID`, `Employee_Name`, `L_Name`, `L_PWord`, `Employee_Sex`, `Employee_Phone`, `Employee_SSID`, `Employee_Address`, `Employee_Allocation`, `Employee_Type`, `Employee_Salary`, `Employee_Level`, `Employee_Remark`, `Enable`) VALUES
(1, '游承恩', 'a795006017', '111', 1, '0987887778', 'A123456789', '地獄二戰市猶太區黃泉路4段987號', 2, 0, 30000, 1, '就是個他媽蛋蛋會痛的婊子', 1);

-- --------------------------------------------------------

--
-- 資料表結構 `location_list`
--

CREATE TABLE `location_list` (
  `Location_ID` int(11) NOT NULL,
  `Location_Name` varchar(20) NOT NULL,
  `Location_Address` varchar(100) NOT NULL,
  `Location_Coloe_Code` varchar(6) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- 資料表的匯出資料 `location_list`
--

INSERT INTO `location_list` (`Location_ID`, `Location_Name`, `Location_Address`, `Location_Coloe_Code`) VALUES
(1, '桃園', '桃園市吉林路38號(桃園車站店)', 'dd7e6b');

-- --------------------------------------------------------

--
-- 資料表結構 `log_of_all`
--

CREATE TABLE `log_of_all` (
  `PKey` bigint(20) NOT NULL,
  `By_Who` int(11) NOT NULL,
  `When_Did` int(11) NOT NULL,
  `Content` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- 資料表結構 `order_list`
--

CREATE TABLE `order_list` (
  `Order_ID` varchar(12) NOT NULL,
  `Created_DT` datetime NOT NULL,
  `Customer_ID` int(11) NOT NULL,
  `Guarantor_ID` int(11) DEFAULT NULL,
  `Employee_ID` int(11) NOT NULL,
  `Emergency_Name` varchar(50) NOT NULL,
  `Emergency_Phone` varchar(20) NOT NULL,
  `Car_Type` varchar(255) NOT NULL,
  `License_Plate` varchar(10) NOT NULL,
  `Rental_Area` int(11) NOT NULL,
  `Return_Area` int(11) NOT NULL,
  `Estimated_Rent_DateTime` datetime NOT NULL,
  `Estimated_Return_DateTime` datetime NOT NULL,
  `Actual_Rent_DateTime` datetime NOT NULL,
  `Actual_Return_DateTime` datetime NOT NULL,
  `Day_Count` int(11) NOT NULL,
  `Total_Price` int(11) NOT NULL,
  `Status` int(1) NOT NULL,
  `Remark` varchar(255) NOT NULL,
  `Log` longblob NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- 資料表的匯出資料 `order_list`
--

INSERT INTO `order_list` (`Order_ID`, `Created_DT`, `Customer_ID`, `Guarantor_ID`, `Employee_ID`, `Emergency_Name`, `Emergency_Phone`, `Car_Type`, `License_Plate`, `Rental_Area`, `Return_Area`, `Estimated_Rent_DateTime`, `Estimated_Return_DateTime`, `Actual_Rent_DateTime`, `Actual_Return_DateTime`, `Day_Count`, `Total_Price`, `Status`, `Remark`, `Log`) VALUES
('202011060001', '2020-11-06 23:20:03', 2, 0, 1, '你媽超胖', '0126549879', 'U6 16', 'RAM-7196', 1, 3, '2020-11-24 10:00:00', '2020-11-30 12:00:00', '0000-00-00 00:00:00', '0000-00-00 00:00:00', 7, 0, 0, '', 0x5b7b2246656174757265223a224372656174655f4f72646572222c22456d706c6f7965655f4944223a2231222c22437573746f6d65725f4944223a2232222c2247756172616e746f725f4944223a22222c2253746172745f44617465223a22323032302f31312f3036222c2253746172745f446174655f54696d65223a2231303a3030222c22456e645f44617465223a22323032302f31312f3133222c22456e645f446174655f54696d65223a2231323a3030222c224461795f436f756e74223a2237222c2252656e745f506c616365223a22e6a183e59c92222c2273656c65637465645f6f7074696f6e223a225536203136222c22546f74616c5f5072696365223a22243135323030222c2252656d61726b223a22222c22437573746f6d65725f4e616d65223a22e5b8a5e593a5e5b590222c22437573746f6d65725f50686f6e65223a2230393633343132303932222c22437573746f6d65725f456d61696c223a2231306e6967687440676d61696c2e636f6d222c22437573746f6d65725f41646472657373223a22e9979ce4bda0e5b181e4ba8b222c22437573746f6d65725f42446179223a22313939342f30362f3038222c2247756172616e746f725f5265717569726d656e74223a2266616c7365222c22437573746f6d65725f536578223a2231222c22437573746f6d65725f53534944223a2241313131313131313131222c22456d657267656e63795f4e616d65223a22e4bda0e5aabde8b685e88396222c22456d657267656e63795f50686f6e65223a2230313236353439383739222c2247756172616e746f725f4e616d65223a22222c2247756172616e746f725f50686f6e65223a22222c2247756172616e746f725f456d61696c223a22222c2247756172616e746f725f41646472657373223a22222c2247756172616e746f725f42446179223a22222c2247756172616e746f725f536578223a2230222c2247756172616e746f725f53534944223a22227d5d),
('202011060002', '2020-11-06 23:23:03', 2, 0, 1, '奚隹 奚隹 石更 石更 白勺', '3234235', 'STAREX', 'RAE-7856', 4, 5, '2020-11-18 10:00:00', '2020-11-23 12:00:00', '2020-11-19 17:30:47', '0000-00-00 00:00:00', 4, 0, 3, '', 0x5b7b2246656174757265223a224372656174655f4f72646572222c22456d706c6f7965655f4944223a2231222c22437573746f6d65725f4944223a2232222c2247756172616e746f725f4944223a22222c2253746172745f44617465223a22323032302f31312f3130222c2253746172745f446174655f54696d65223a2231303a3030222c22456e645f44617465223a22323032302f31312f3134222c22456e645f446174655f54696d65223a2231323a3030222c224461795f436f756e74223a2234222c2252656e745f506c616365223a22e6a183e59c92222c2273656c65637465645f6f7074696f6e223a22535441524558222c22546f74616c5f5072696365223a22243131373030222c2252656d61726b223a22222c22437573746f6d65725f4e616d65223a22e5b8a5e593a5e5b590222c22437573746f6d65725f50686f6e65223a2230393633343132303932222c22437573746f6d65725f456d61696c223a2231306e6967687440676d61696c2e636f6d222c22437573746f6d65725f41646472657373223a22e9979ce4bda0e5b181e4ba8b222c22437573746f6d65725f42446179223a22313939342f30362f3038222c2247756172616e746f725f5265717569726d656e74223a2266616c7365222c22437573746f6d65725f536578223a2231222c22437573746f6d65725f53534944223a2241313131313131313131222c22456d657267656e63795f4e616d65223a22e5a59ae99ab920e5a59ae99ab920e79fb3e69bb420e79fb3e69bb420e799bde58bba222c22456d657267656e63795f50686f6e65223a2233323334323335222c2247756172616e746f725f4e616d65223a22222c2247756172616e746f725f50686f6e65223a22222c2247756172616e746f725f456d61696c223a22222c2247756172616e746f725f41646472657373223a22222c2247756172616e746f725f42446179223a22222c2247756172616e746f725f536578223a2230222c2247756172616e746f725f53534944223a22227d5d),
('202011160001', '2020-11-16 18:06:04', 2, 0, 1, '1135', '64651328', 'NEW ELANTRA', 'RBU-1300', 6, 2, '2020-11-22 00:00:00', '2020-12-10 12:00:00', '0000-00-00 00:00:00', '0000-00-00 00:00:00', 2, 0, 0, '幹你娘問題怎麼那麼多\r\n操他媽的機掰咧\r\n幹', 0x5b7b2246656174757265223a224372656174655f4f72646572222c22456d706c6f7965655f4944223a2231222c22437573746f6d65725f4944223a2232222c2247756172616e746f725f4944223a22222c2253746172745f44617465223a22323032302f31312f3136222c2253746172745f446174655f54696d65223a2231303a3030222c22456e645f44617465223a22323032302f31312f3138222c22456e645f446174655f54696d65223a2231323a3030222c224461795f436f756e74223a2232222c2252656e745f506c616365223a2232222c2273656c65637465645f6f7074696f6e223a224e455720454c414e545241222c22546f74616c5f5072696365223a222433383830222c2252656d61726b223a22e5b9b9e4bda0e5a898e5958fe9a18ce6808ee9babce982a3e9babce5a49a0d0ae6938de4bb96e5aabde79a84e6a99fe68eb0e592a70d0ae5b9b9222c22437573746f6d65725f4e616d65223a22e5b8a5e593a5e5b590222c22437573746f6d65725f50686f6e65223a2230393633343132303932222c22437573746f6d65725f456d61696c223a2231306e6967687440676d61696c2e636f6d222c22437573746f6d65725f41646472657373223a22e9979ce4bda0e5b181e4ba8b222c22437573746f6d65725f42446179223a22313939342f30362f3038222c2247756172616e746f725f5265717569726d656e74223a2266616c7365222c22437573746f6d65725f536578223a2231222c22437573746f6d65725f53534944223a2241313131313131313131222c22456d657267656e63795f4e616d65223a2231313335222c22456d657267656e63795f50686f6e65223a223634363531333238222c2247756172616e746f725f4e616d65223a22222c2247756172616e746f725f50686f6e65223a22222c2247756172616e746f725f456d61696c223a22222c2247756172616e746f725f41646472657373223a22222c2247756172616e746f725f42446179223a22222c2247756172616e746f725f536578223a2230222c2247756172616e746f725f53534944223a22227d5d);

-- --------------------------------------------------------

--
-- 資料表結構 `scheduling_list`
--

CREATE TABLE `scheduling_list` (
  `Scheduling_ID` bigint(20) NOT NULL,
  `Request_Create_DT` datetime NOT NULL,
  `Request_StaffID` int(11) NOT NULL,
  `Request_Location` int(11) NOT NULL,
  `Expect_DT` datetime NOT NULL,
  `Response_StaffID` int(11) NOT NULL,
  `Response_Location` int(11) NOT NULL,
  `Response_DT` datetime NOT NULL,
  `Actual_Arrival_DT` datetime NOT NULL,
  `License_Plate` varchar(10) NOT NULL,
  `Status` int(1) NOT NULL DEFAULT '0',
  `Remark` varchar(255) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- 資料表的匯出資料 `scheduling_list`
--

INSERT INTO `scheduling_list` (`Scheduling_ID`, `Request_Create_DT`, `Request_StaffID`, `Request_Location`, `Expect_DT`, `Response_StaffID`, `Response_Location`, `Response_DT`, `Actual_Arrival_DT`, `License_Plate`, `Status`, `Remark`) VALUES
(1, '2020-10-18 12:14:09', 1, 7, '0000-00-00 00:00:00', 1, 2, '0000-00-00 00:00:00', '0000-00-00 00:00:00', 'RAM-7196', 0, '');

--
-- 已匯出資料表的索引
--

--
-- 資料表索引 `branch_allocation`
--
ALTER TABLE `branch_allocation`
  ADD PRIMARY KEY (`BA_ID`);

--
-- 資料表索引 `car_list`
--
ALTER TABLE `car_list`
  ADD PRIMARY KEY (`License_Plate`),
  ADD KEY `Company_Name` (`Company_Name`),
  ADD KEY `Car_Type` (`Car_Type`);

--
-- 資料表索引 `car_type`
--
ALTER TABLE `car_type`
  ADD PRIMARY KEY (`Car_ID`),
  ADD UNIQUE KEY `Car_Type` (`Car_Type`);

--
-- 資料表索引 `company_list`
--
ALTER TABLE `company_list`
  ADD PRIMARY KEY (`Company_Name`);

--
-- 資料表索引 `customer_list`
--
ALTER TABLE `customer_list`
  ADD PRIMARY KEY (`Customer_ID`),
  ADD UNIQUE KEY `Customer_SSID` (`Customer_SSID`);

--
-- 資料表索引 `employee_list`
--
ALTER TABLE `employee_list`
  ADD PRIMARY KEY (`Employee_ID`);

--
-- 資料表索引 `location_list`
--
ALTER TABLE `location_list`
  ADD PRIMARY KEY (`Location_ID`);

--
-- 資料表索引 `log_of_all`
--
ALTER TABLE `log_of_all`
  ADD PRIMARY KEY (`PKey`);

--
-- 資料表索引 `order_list`
--
ALTER TABLE `order_list`
  ADD PRIMARY KEY (`Order_ID`);

--
-- 資料表索引 `scheduling_list`
--
ALTER TABLE `scheduling_list`
  ADD PRIMARY KEY (`Scheduling_ID`);

--
-- 在匯出的資料表使用 AUTO_INCREMENT
--

--
-- 使用資料表 AUTO_INCREMENT `branch_allocation`
--
ALTER TABLE `branch_allocation`
  MODIFY `BA_ID` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=12;

--
-- 使用資料表 AUTO_INCREMENT `car_type`
--
ALTER TABLE `car_type`
  MODIFY `Car_ID` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=34;

--
-- 使用資料表 AUTO_INCREMENT `customer_list`
--
ALTER TABLE `customer_list`
  MODIFY `Customer_ID` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=6;

--
-- 使用資料表 AUTO_INCREMENT `employee_list`
--
ALTER TABLE `employee_list`
  MODIFY `Employee_ID` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=4;

--
-- 使用資料表 AUTO_INCREMENT `location_list`
--
ALTER TABLE `location_list`
  MODIFY `Location_ID` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;

--
-- 使用資料表 AUTO_INCREMENT `log_of_all`
--
ALTER TABLE `log_of_all`
  MODIFY `PKey` bigint(20) NOT NULL AUTO_INCREMENT;

--
-- 使用資料表 AUTO_INCREMENT `scheduling_list`
--
ALTER TABLE `scheduling_list`
  MODIFY `Scheduling_ID` bigint(20) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
